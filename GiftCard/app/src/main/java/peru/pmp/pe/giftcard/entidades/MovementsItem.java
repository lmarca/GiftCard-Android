package peru.pmp.pe.giftcard.entidades;

/**
 * Created by Admin on 21/11/17.
 */

public class MovementsItem {

    private String movFechaTxn;
    private String movDesTxn;
    private String movMonOriginalTxn;
    private String movMontoTxn;
    private String movSigMontoTxn;
    private String movTipoTarjeta;
    private String movFiller;

    public MovementsItem() {
        super();
        this.movFechaTxn = "";
        this.movDesTxn = "";
        this.movMonOriginalTxn = "";
        this.movMontoTxn = "";
        this.movSigMontoTxn = "";
        this.movTipoTarjeta = "";
        this.movFiller = "";
    }

    public MovementsItem(String movFechaTxn, String movDesTxn, String movMonOriginalTxn, String movMontoTxn, String movSigMontoTxn, String movTipoTarjeta, String movFiller) {
        this.movFechaTxn = movFechaTxn;
        this.movDesTxn = movDesTxn;
        this.movMonOriginalTxn = movMonOriginalTxn;
        this.movMontoTxn = movMontoTxn;
        this.movSigMontoTxn = movSigMontoTxn;
        this.movTipoTarjeta = movTipoTarjeta;
        this.movFiller = movFiller;
    }

    public String getMovFechaTxn() {
        return movFechaTxn;
    }

    public void setMovFechaTxn(String movFechaTxn) {
        this.movFechaTxn = movFechaTxn;
    }

    public String getMovDesTxn() {
        return movDesTxn;
    }

    public void setMovDesTxn(String movDesTxn) {
        this.movDesTxn = movDesTxn;
    }

    public String getMovMonOriginalTxn() {
        return movMonOriginalTxn;
    }

    public void setMovMonOriginalTxn(String movMonOriginalTxn) {
        this.movMonOriginalTxn = movMonOriginalTxn;
    }

    public String getMovMontoTxn() {
        return movMontoTxn;
    }

    public void setMovMontoTxn(String movMontoTxn) {
        this.movMontoTxn = movMontoTxn;
    }

    public String getMovSigMontoTxn() {
        return movSigMontoTxn;
    }

    public void setMovSigMontoTxn(String movSigMontoTxn) {
        this.movSigMontoTxn = movSigMontoTxn;
    }

    public String getMovTipoTarjeta() {
        return movTipoTarjeta;
    }

    public void setMovTipoTarjeta(String movTipoTarjeta) {
        this.movTipoTarjeta = movTipoTarjeta;
    }

    public String getMovFiller() {
        return movFiller;
    }

    public void setMovFiller(String movFiller) {
        this.movFiller = movFiller;
    }

}
