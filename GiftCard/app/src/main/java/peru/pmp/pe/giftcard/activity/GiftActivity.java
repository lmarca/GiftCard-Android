package peru.pmp.pe.giftcard.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;


import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;

/**
 * Created by apple on 9/11/17.
 */

public class GiftActivity extends MyActivity {

    @Bind(R.id.edit_mensaje)
    TextView edit_mensaje;

    @Bind(R.id.btnAddCard)
    Button btnAddCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift);
        DesignUtil.customStatusBarColor(getWindow(), this, R.color.colorPrimaryDark);

        ButterKnife.bind(this);
        /*** INICIALIZACION DE VARIABLES ***/

        configurationActivity();

    }

    private void configurationActivity() {

        Typeface face=Typeface.createFromAsset(getAssets(),"fonts/poppins-regular.ttf");
        edit_mensaje.setTypeface(face);
        btnAddCard.setTypeface(face);
    }

    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_ENTER) {
			return true;
		}
		return false;
	}

    @OnClick(R.id.btnAddCard)
    public void getKind() { show(KindCardActivity.class);}

    @OnClick(R.id.btnLogout)
    public void logout() { finishSession();}

}
