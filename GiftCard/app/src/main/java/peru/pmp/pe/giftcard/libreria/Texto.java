package peru.pmp.pe.giftcard.libreria;

import peru.pmp.pe.giftcard.utilitarios.Soporte;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <p>
 * Empresa: MOVILOGIC PERU
 * </p>
 * 
 * @author Billy Vera Castellanos
 * @version 1.0
 */
public class Texto {

	public final static String ENTER = "\r\n";

	public Texto() {
	}

	public static boolean isNullOrEmpty(String valor) {
		return valor == null || (valor != null && valor.trim().length() == 0);
	}

	public static boolean contains(String valor1, String valor2) {
		// /Soporte.dump("valor1='"+valor1+"'=='"+valor2+"' resultado " +
		// valor1.indexOf(valor2) );
		return valor1 != null && valor2 != null
				&& (valor1.indexOf(valor2) != -1);
	}
	
	public static boolean isRegexValido(String regex, String texto){
		Pattern p;
		Matcher m;
		boolean valid = false;
		try {
			p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE
					| Pattern.MULTILINE);
			m = p.matcher(texto);
			valid = m.find();

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			m = null;
			p = null;
		}
		return valid;
	}

	public static String regexToString(String regex, String texto) {
		String retorno = null;
		Pattern p = null;
		Matcher m;
		try {
			p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE
					| Pattern.MULTILINE);
			m = p.matcher(texto);
			if (m.find()) {
				if(m.groupCount()>0)
					retorno = m.group(1);
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			m = null;
			p = null;
		}
		return retorno;
	}

	public static String[] regexToArray(String regex, String texto) {
		String[] grupos = null;
		Pattern p = null;
		Matcher m;
		Vector<String> vector = new Vector<String>();
		try {
			p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE
					| Pattern.MULTILINE);
			m = p.matcher(texto);
			while (m.find()) {
				for (int z = 1; z <= m.groupCount(); z++)
					vector.add(m.group(z));
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			m = null;
			p = null;
			if (vector.size() > 0)
				grupos = vectorToArray(vector);
			vector = null;
		}
		return grupos;
	}

	public static String[] vectorToArray(Vector<String> vector) {
		String[] grupos = new String[0];
		if (vector != null) {
			int size = vector.size();
			grupos = new String[size];
			for (int u = 0; u < size; u++)
				grupos[u] = vector.elementAt(u);
		}
		return grupos;
	}

	public static String removerLineas(String texto, String empiecencon) {
		// Tenemos todo en memoria. Ahora tenemos que pasarlo al vector.
		BufferedReader input = new BufferedReader(new StringReader(texto));
		String lin = "";
		String newtexto = "";
		try {
			while ((lin = input.readLine()) != null) {
				if (lin.startsWith(empiecencon))
					continue;
				if (newtexto.length() > 0)
					newtexto += "\r\n";
				newtexto += lin;
			}
		} catch (IOException e) {

		}
		return newtexto;
	}

	public static String obtenerLinea(String texto, String empiecencon) {
		// Tenemos todo en memoria. Ahora tenemos que pasarlo al vector.
		BufferedReader input = new BufferedReader(new StringReader(texto));
		String lin = "";
		String newtexto = "";
		try {
			while ((lin = input.readLine()) != null) {
				if (lin.startsWith(empiecencon)) {
					if (newtexto.length() > 0)
						newtexto += "\r\n";
					if (lin.length() > empiecencon.length())
						newtexto += lin.substring(empiecencon.length());
					else
						newtexto += "";
				}
			}
		} catch (IOException e) {

		}
		return newtexto;
	}

	/***********************************************************************
	 * FUNCION: PadRight DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: la misma cadena rellenado con el caracter especificado REVISION:
	 * -
	 ***********************************************************************/
	public static void padRight(StringBuffer buffer, int repe, String carac) {
		if (buffer == null)
			buffer = new StringBuffer();

		if (buffer.length() > repe)
			buffer.delete(repe, buffer.length());
		int resto = repe - buffer.length();
		for (int i = 0; i < resto; i++)
			buffer.append(carac);
	}

	public static String padRight(String buffer, int repe, String carac) {
		if (buffer == null)
			buffer = "";
		StringBuffer retorno = new StringBuffer(buffer);
		padRight(retorno, repe, carac);
		return retorno.toString();
	}

	/***********************************************************************
	 * FUNCION: PadLeft DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: la misma cadena rellenado con el caracter especificado REVISION:
	 * -
	 ***********************************************************************/
	public static void padLeft(StringBuffer buffer, int repe, String carac) {
		if (buffer == null)
			buffer = new StringBuffer();

		if (buffer.length() > repe)
			buffer.delete(0, buffer.length() - repe);
		int resto = repe - buffer.length();
		for (int i = 0; i < resto; i++)
			buffer.insert(0, carac);
	}

	public static String padLeft(String buffer, int repe, String carac) {
		if (buffer == null)
			buffer = "";
		StringBuffer retorno = new StringBuffer(buffer);
		padLeft(retorno, repe, carac);
		return retorno.toString();
	}

	/***********************************************************************
	 * FUNCION: PadLeft DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: la misma cadena rellenado con el caracter especificado REVISION:
	 * -
	 ***********************************************************************/
	public static void repetir(StringBuffer buffer, int repe, String carac) {
		for (int i = 0; i < repe; i++)
			buffer.append(carac);
	}

	public static void trim(StringBuffer buffer) {
		if (buffer == null)
			buffer = new StringBuffer();

		int gato = 0;
		while (gato < buffer.length()) {
			if (buffer.charAt(gato) == ' ') {
				buffer.deleteCharAt(gato);
			} else {
				break;
			}
		}
		gato = buffer.length();
		while (gato > 0) {
			if (buffer.charAt(gato - 1) == ' ') {
				buffer.deleteCharAt(gato - 1);
				gato--;
			} else {
				break;
			}
		}
	}

	/***********************************************************************
	 * FUNCION: Right DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: REVISION: -
	 ***********************************************************************/
	public static String right(String buffer, int len) {
		if (buffer == null)
			buffer = "";
		int lenText = buffer.length();
		if (buffer.length() < len)
			return buffer;
		else
			return buffer.substring(lenText - len);
	}

	/***********************************************************************
	 * FUNCION: Left DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: REVISION: -
	 ***********************************************************************/
	public static String left(String buffer, int len) {
		if (buffer == null)
			buffer = "";
		if (buffer.length() < len)
			return buffer;
		else
			return buffer.substring(0, len);
	}

	/***********************************************************************
	 * FUNCION: Stuff DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: Reemplaza el contenido de una cadena con la especificada
	 * REVISION: -
	 ***********************************************************************/
	public static String stuff(String texto, String aReempl, String xReempl) {
		if (texto == null) {
			texto = "";
		}
		String newTexto = "";
		int letra = 0;
		int ini = -1;
		for (int i = 0; i < texto.length(); i++) {
			if (texto.charAt(i) == aReempl.charAt(letra)) {
				if (ini == -1) {
					ini = i;
				}
				letra++;
				if ((letra) == aReempl.length()) {
					newTexto = newTexto + xReempl;
					ini = -1;
					letra = 0;
				}
			} else {
				if (!(ini == -1)) {
					newTexto = newTexto + texto.substring(ini, ini + letra + 1);
					ini = -1;
					letra = 0;
				} else {
					newTexto = newTexto + texto.charAt(i);
				}
			}
		}
		return (newTexto);
	}

	public static String stuff(String texto, int posIni, String xReempl) {
		if (Texto.isNullOrEmpty(texto) || Texto.isNullOrEmpty(xReempl)
				|| posIni + xReempl.length() > texto.length()) {
			return texto;
		}
		String newTexto = "";
		newTexto = texto.substring(0, posIni) + xReempl
				+ texto.substring(posIni + xReempl.length());
		return (newTexto);
	}

	/***********************************************************************
	 * FUNCION: Encriptar DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: - REVISION: -
	 ***********************************************************************/
	public static byte[] Encriptar(String Campo, int inicio, int cantidad) {
		int largo;
		int i = 0;
		int pos = 0;
		byte valorint;
		int tmp;
		byte txt[];
		largo = Campo.length();
		txt = new byte[largo];
		if (largo >= (inicio + cantidad)) {
			for (i = inicio; i < (inicio + cantidad); i++) {
				valorint = (byte) Campo.charAt(i);
				tmp = (int) valorint + 100;
				txt[pos++] = (byte) tmp;
			}
		}
		return (txt);
	}

	/***********************************************************************
	 * FUNCION: DesEncriptar DESCRIPCION:
	 * 
	 * PARAMETROS: -
	 * 
	 * RETORNO: - REVISION: -
	 ***********************************************************************/
	public static void DesEncriptar(String Campo, int inicio, int cantidad,
                                    byte txt[]) {
		int largo;
		int i = 0;
		int pos = 0;
		int valorint;
		largo = Campo.length();
		if (largo >= (inicio + cantidad)) {
			for (i = inicio; i < (inicio + cantidad); i++) {
				valorint = (int) (byte) Campo.charAt(i);
				valorint = valorint - 100;
				txt[pos++] = (byte) valorint;
			}
		}
	}

	public static Vector<String> Bytes2Vector(byte[] bites) {
		Vector<String> vector = new Vector<String>();
		String line = "";
		if (bites != null) {
			BufferedReader bread = new BufferedReader(new StringReader(
					new String(bites)));
			do {
				try {
					line = (String) bread.readLine();
				} catch (IOException e) {
				}
				;
				if (line != null) {
					vector.add(line);
				}
			} while (line != null);
		}
		return vector;
	}

	public static Vector<String> StringBuffer2Vector(StringBuffer stbuff) {
		return StringBuffer2Vector(stbuff.toString());
	}
	public static Vector<String> StringBuffer2Vector(String stbuff) {
		Vector<String> vector = new Vector<String>();
		String line = "";
		if (stbuff != null) {
			BufferedReader bread = new BufferedReader(new StringReader(
					stbuff));
			///Soporte.dump("lineas VOUCHER");
			do {
				try {
					line = bread.readLine();
					///Soporte.dump(line);
				} catch (IOException e) {
				}
				;
				if (line != null) {
					vector.add(line);
				}
			} while (line != null);
		}
		return vector;
	}

	public static String ByteArrayToStr(byte[] bites) {
		return ByteArrayToStr(bites, 0, bites.length);
	}

	public static String ByteArrayToStr(byte[] bites, int offset, int len) {
		return new String(bites, offset, len);
	}

	public static boolean tieneCaracteresValidos(String txt) {
		boolean bAlfa = true;
		if (txt != null) {
			char car;
			for (int u = 0; u < txt.length(); u++) {
				car = txt.charAt(u);
				if (car != ' ' && car != '.' && !Character.isLetterOrDigit(car)) {
					bAlfa = false;
					break;
				}
			}
		}
		return bAlfa;
	}

	// Inicialmente eliminara retornos de carro y tabulaciones.-
	public static String eliminarCaracteresNoLegibles(String valor) {
		StringBuffer buff = new StringBuffer();
		if (valor != null) {
			char[] textobase = valor.toCharArray();
			for (char car : textobase) {
				if (Character.isDigit(car) || Character.isLetter(car)
						|| Character.isSpaceChar(car)
						|| Character.isUnicodeIdentifierPart(car))
					buff.append(car);
				else
					buff.append(' ');
			}
		}
		return valor.toString();
	}

	public static void eliminarCaracteresEspeciales(StringBuffer buff) {
		if (buff != null) {
			char[] textobase = buff.toString().toCharArray();
			int posCurrent = 0;
			for (char car : textobase) {
				if (car == '\n' || car == '\r' || car == '\t') {
					buff.deleteCharAt(posCurrent);
					buff.insert(posCurrent, ' ');
				}
				posCurrent++;
			}
		}
	}

	public static void normalizaCaracteresEspeciales(StringBuffer buff) {
		if (buff != null) {
			char[] textobase = buff.toString().toCharArray();
			int posCurrent = 0;
			for (int u = 0; u < textobase.length; u++) {
				char car = textobase[u];
				if (car == '\n' || car == '\r') {
					buff.deleteCharAt(posCurrent);
					buff.insert(posCurrent, Archivo.LABEL_ENTER);
					posCurrent += Archivo.LABEL_ENTER.length() - 1;
				} else if (car == '\t') {
					buff.deleteCharAt(posCurrent);
					buff.insert(posCurrent, Archivo.LABEL_TAB);
					posCurrent += Archivo.LABEL_TAB.length() - 1;
				}
				posCurrent++;
			}
		}
	}

	// Inicialmente eliminara retornos de carro y tabulaciones.-
	public static String[] split(String txt) {
		return split(txt, ",");
	}

	public static String[] split(String txt, String delim) {
		String[] tokens;
		ArrayList<String> vector = split2Array(txt, delim);
		tokens = new String[vector.size()];
		for (int u = 0; u < vector.size(); u++)
			tokens[u] = vector.get(u);
		vector = null;
		return tokens;
	}
	
	public static String[] split(String txt, int ancho) {
		String[] tokens;
		int qbloques = txt.length()/ancho + ((txt.length()%ancho)>0?1:0);
		tokens = new String[qbloques];
		int curPos = 0;
		for(int curbloque=0; curbloque<qbloques; curbloque++){
			try{
				tokens[curbloque] = txt.substring(curPos, curPos + ancho);
			}catch(Exception e){
				tokens[curbloque] = txt.substring(curPos, txt.length());
			}
			curPos += ancho;
		}
		return tokens;
	}
	public static ArrayList<String> split2Array(String txt) {
		return split2Array(txt, ",");
	}

	public static ArrayList<String> split2Array(String txt, String delim) {
		ArrayList<String> vector = new ArrayList<String>();
		if (txt != null) {
			int index = 0;
			int curPos = 0;
			while (true) {
				index = txt.indexOf(delim, curPos);
				// /Soporte.dump("Texto.split: delim=" +
				// delim+" curPos="+curPos+" resultado index="+index);
				if (index == -1) {
					if (curPos < txt.length())
						vector.add(txt.substring(curPos));
					break;
				}
				// /Soporte.dump("Texto.split: add=" + txt.substring(curPos,
				// index));
				vector.add(txt.substring(curPos, index));
				curPos = index + delim.length();
			}
			if (txt.endsWith(delim))
				vector.add("");
		}
		return vector;
	}

	public static void deleteRowBuffer(StringBuffer stbuff, int rowid) {
		if (stbuff != null) {
			BufferedReader bread = new BufferedReader(new StringReader(
					stbuff.toString()));
			int curRow = 0;
			String line = null;
			long curPos = 0;
			do {
				try {
					line = (String) bread.readLine();
				} catch (IOException e) {
				}
				if (line != null) {
					curPos += line.length() + 1;
					// Eliminar los caracteres hasta el ENTER
					if (rowid == curRow) {
						char car;
						while (curPos < stbuff.length()) {
							car = stbuff.charAt((int) curPos);
							stbuff.deleteCharAt((int) curPos);
							if (car == '\n' || car == '\r')
								break;
						}
						break;
					}
				}
				curRow++;
			} while (line != null);
		}
	}

	/************************************************************************
	 * @funciones numericas
	 */
	public static int toInt(String valor) {
		return toInt(valor, 10);
	}

	public static int toInt(String valor, int radian) {
		int intValue = 0;
		try {
			intValue = Integer.parseInt(valor, radian);
		} catch (Exception e) {
		}
		return intValue;
	}

	public static short toShort(String valor) {
		short intValue = 0;
		try {
			intValue = Short.parseShort(valor);
		} catch (Exception e) {
		}
		return intValue;
	}

	public static double toDouble(String valor) {
		double retorno = 0;
		try {
			if (!Soporte.isNullOrE(valor))
				retorno = Double.parseDouble(valor);
		} catch (Exception e) {
		}
		return retorno;
	}

	public static double toDouble(String valor, int decimales) {
		double retorno = 0;
		try {
			if (!Soporte.isNullOrE(valor)) {
				retorno = Double.parseDouble(valor);
				int auxInt = (int) (retorno * 100);
				retorno = auxInt / 100;
			}
		} catch (Exception e) {
		}
		return retorno;
	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;
		/*
		 * Email format: A valid email address will have following format:
		 * [\\w\\.-]+: Begins with word characters, (may include periods and
		 * hypens).
		 * 
		 * @: It must have a '@' symbol after initial characters.
		 * ([\\w\\-]+\\.)+: '@' must follow by more alphanumeric characters (may
		 * include hypens.). This part must also have a "." to separate domain
		 * and subdomain names. [A-Z]{2,4}$ : Must end with two to four
		 * alaphabets. (This will allow domain names with 2, 3 and 4 characters
		 * e.g pa, com, net, wxyz)
		 * 
		 * Examples: Following email addresses will pass validation abc@xyz.net;
		 * ab.c@tx.gov
		 */

		// Initialize reg ex for email.
		String expression = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}((;\\s?){1}[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4})*$";
		////= "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;
		// Make the comparison case-insensitive.
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public static boolean in(String valor, String lista) {
		return in(valor, lista, ",");
	}

	public static boolean in(String valor, String lista_general,
                             String separador) {
		boolean es = false;
		String[] comandos = Texto.split(lista_general, separador);
		for (int u = 0; u < comandos.length; u++) {
			if (valor.equalsIgnoreCase(comandos[u])) {
				es = true;
				break;
			}
		}
		return es;
	}

	/************************************************************************
	 * @funciones texto
	 */
	public static int comparaValores(String txtBD, String txtUser) {
		int compare = 0;
		String master = new String(txtBD);
		if (master.length() > txtUser.length()) {
			master = master.substring(0, txtUser.length());
		}
		compare = master.compareTo(txtUser);
		master = null;
		return compare;
	}

	// Inicialmente eliminara retornos de carro y tabulaciones.-
	public static String eliminarEspeciales(String txt) {
		String sNuevoTXT = "";
		if (txt != null) {
			char[] caracteres = txt.toCharArray();
			char car;
			for (int u = 0; u < caracteres.length; u++) {
				car = caracteres[u];
				if (car == '\n' || car == '\r' || car == '\t') {
					car = ' ';
				}
				sNuevoTXT += car;
			}
		}
		return sNuevoTXT;
	}

	public static String codeDecode(String cadenaTexto) {

		// Vemos si el formato entrante es ASCII o UTF8
		// /CharsetEncoder isoEncoder =
		// Charset.forName("ISO-8859-1").newEncoder();
		CharsetEncoder utf8Encoder = Charset.forName("UTF-8").newEncoder();
		// /Boolean isISO = isoEncoder.canEncode(cadenaTexto);
		Boolean isUTF8 = utf8Encoder.canEncode(cadenaTexto);
		String datos = cadenaTexto;
		if (isUTF8) {
			// Convertir de UTF-8 a ISO-8859-1
			Charset utf8charset = Charset.forName("UTF-8");
			Charset iso88591charset = Charset.forName("ISO-8859-1");

			// Decode UTF-8
			ByteBuffer bb = ByteBuffer.wrap(cadenaTexto.getBytes());
			CharBuffer data = utf8charset.decode(bb);

			// Encode ISO-8559-1
			ByteBuffer outputBuffer = iso88591charset.encode(data);
			byte[] outputData = outputBuffer.array();

			datos = new String(outputData);
		}
		return datos;
	}

	public static String nvl(String texto) {
		return nvl(texto, "");
	}

	public static String nvl(String texto, String defecto) {
		if (texto == null)
			texto = defecto;
		return texto;
	}

	public static String nve(String texto, String defecto) {
		if (Soporte.isNullOrE(texto))
			texto = defecto;
		return texto;
	}

	public static String elipsis(String texto, int count) {
		texto = nvl(texto);
		if (texto.length() > count) {
			texto = Texto.padRight(texto.substring(0, count - 3), count, ".");
		}
		return texto;
	}

	private static DecimalFormatSymbols formatSymbols;

	public static DecimalFormatSymbols getFormatoDecimal() {
		if (formatSymbols == null) {
			formatSymbols = new DecimalFormatSymbols();
			formatSymbols.setDecimalSeparator('.');
			formatSymbols.setGroupingSeparator(',');
		}
		return formatSymbols;
	}

	public static String numberFormat(double valor) {
		NumberFormat formatter = new DecimalFormat("####,###,##0.00",
				getFormatoDecimal());
		return formatter.format(valor);
	}

	public static byte[] StrToByteArray(String str) {
		return str.getBytes(Charset.forName("ISO-8859-1"));
	}

	public static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	public static String replaceCharacterSpecial(String texto){
		/*return texto
				.replace("ñ", "n")
				.replace("á", "a")
				.replace("é", "e")
				.replace("í", "i")
				.replace("ó", "o")
				.replace("ú", "u")
				.replace("Á", "A")
				.replace("É", "E")
				.replace("Í", "I")
				.replace("Ó", "O")
				.replace("Ú", "U")
				.replace('Ñ', 'N');
				*/
		return "";
	}
	
	public static boolean validaTelefono(String numero) {
		// Compiles the given regular expression into a pattern.
		int valor = Texto.toInt(numero);
		if(valor>100000000 && valor<=999999999)
			return true;
		else
			return false;
	}

    public static final String unescapeHtml3(final String input) {
        StringWriter writer = null;
        int len = input.length();
        int i = 1;
        int st = 0;
        while (true) {
            // look for '&'
            while (i < len && input.charAt(i-1) != '&')
                i++;
            if (i >= len)
                break;

            // found '&', look for ';'
            int j = i;
            while (j < len && j < i + MAX_ESCAPE + 1 && input.charAt(j) != ';')
                j++;
            if (j == len || j < i + MIN_ESCAPE || j == i + MAX_ESCAPE + 1) {
                i++;
                continue;
            }

            // found escape 
            if (input.charAt(i) == '#') {
                // numeric escape
                int k = i + 1;
                int radix = 10;

                final char firstChar = input.charAt(k);
                if (firstChar == 'x' || firstChar == 'X') {
                    k++;
                    radix = 16;
                }

                try {
                    int entityValue = Integer.parseInt(input.substring(k, j), radix);

                    if (writer == null) 
                        writer = new StringWriter(input.length());
                    writer.append(input.substring(st, i - 1));

                    if (entityValue > 0xFFFF) {
                        final char[] chrs = Character.toChars(entityValue);
                        writer.write(chrs[0]);
                        writer.write(chrs[1]);
                    } else {
                        writer.write(entityValue);
                    }

                } catch (NumberFormatException ex) {
                    i++;
                    continue;
                }
            }
            else {
                // named escape
                CharSequence value = lookupMap.get(input.substring(i, j));
                if (value == null) {
                    i++;
                    continue;
                }

                if (writer == null) 
                    writer = new StringWriter(input.length());
                writer.append(input.substring(st, i - 1));

                writer.append(value);
            }

            // skip escape
            st = j + 1;
            i = st;
        }

        if (writer != null) {
            writer.append(input.substring(st, len));
            return writer.toString();
        }
        return input;
    }

    private static final String[][] ESCAPES = {
        {"\"",     "quot"}, // " - double-quote
        {"&",      "amp"}, // & - ampersand
        {"<",      "lt"}, // < - less-than
        {">",      "gt"}, // > - greater-than

        // Mapping to escape ISO-8859-1 characters to their named HTML 3.x equivalents.
        {"\u00A0", "nbsp"}, // non-breaking space
        {"\u00A1", "iexcl"}, // inverted exclamation mark
        {"\u00A2", "cent"}, // cent sign
        {"\u00A3", "pound"}, // pound sign
        {"\u00A4", "curren"}, // currency sign
        {"\u00A5", "yen"}, // yen sign = yuan sign
        {"\u00A6", "brvbar"}, // broken bar = broken vertical bar
        {"\u00A7", "sect"}, // section sign
        {"\u00A8", "uml"}, // diaeresis = spacing diaeresis
        {"\u00A9", "copy"}, // © - copyright sign
        {"\u00AA", "ordf"}, // feminine ordinal indicator
        {"\u00AB", "laquo"}, // left-pointing double angle quotation mark = left pointing guillemet
        {"\u00AC", "not"}, // not sign
        {"\u00AD", "shy"}, // soft hyphen = discretionary hyphen
        {"\u00AE", "reg"}, // ® - registered trademark sign
        {"\u00AF", "macr"}, // macron = spacing macron = overline = APL overbar
        {"\u00B0", "deg"}, // degree sign
        {"\u00B1", "plusmn"}, // plus-minus sign = plus-or-minus sign
        {"\u00B2", "sup2"}, // superscript two = superscript digit two = squared
        {"\u00B3", "sup3"}, // superscript three = superscript digit three = cubed
        {"\u00B4", "acute"}, // acute accent = spacing acute
        {"\u00B5", "micro"}, // micro sign
        {"\u00B6", "para"}, // pilcrow sign = paragraph sign
        {"\u00B7", "middot"}, // middle dot = Georgian comma = Greek middle dot
        {"\u00B8", "cedil"}, // cedilla = spacing cedilla
        {"\u00B9", "sup1"}, // superscript one = superscript digit one
        {"\u00BA", "ordm"}, // masculine ordinal indicator
        {"\u00BB", "raquo"}, // right-pointing double angle quotation mark = right pointing guillemet
        {"\u00BC", "frac14"}, // vulgar fraction one quarter = fraction one quarter
        {"\u00BD", "frac12"}, // vulgar fraction one half = fraction one half
        {"\u00BE", "frac34"}, // vulgar fraction three quarters = fraction three quarters
        {"\u00BF", "iquest"}, // inverted question mark = turned question mark
        {"\u00C0", "Agrave"}, // А - uppercase A, grave accent
        {"\u00C1", "Aacute"}, // Б - uppercase A, acute accent
        {"\u00C2", "Acirc"}, // В - uppercase A, circumflex accent
        {"\u00C3", "Atilde"}, // Г - uppercase A, tilde
        {"\u00C4", "Auml"}, // Д - uppercase A, umlaut
        {"\u00C5", "Aring"}, // Е - uppercase A, ring
        {"\u00C6", "AElig"}, // Ж - uppercase AE
        {"\u00C7", "Ccedil"}, // З - uppercase C, cedilla
        {"\u00C8", "Egrave"}, // И - uppercase E, grave accent
        {"\u00C9", "Eacute"}, // Й - uppercase E, acute accent
        {"\u00CA", "Ecirc"}, // К - uppercase E, circumflex accent
        {"\u00CB", "Euml"}, // Л - uppercase E, umlaut
        {"\u00CC", "Igrave"}, // М - uppercase I, grave accent
        {"\u00CD", "Iacute"}, // Н - uppercase I, acute accent
        {"\u00CE", "Icirc"}, // О - uppercase I, circumflex accent
        {"\u00CF", "Iuml"}, // П - uppercase I, umlaut
        {"\u00D0", "ETH"}, // Р - uppercase Eth, Icelandic
        {"\u00D1", "Ntilde"}, // С - uppercase N, tilde
        {"\u00D2", "Ograve"}, // Т - uppercase O, grave accent
        {"\u00D3", "Oacute"}, // У - uppercase O, acute accent
        {"\u00D4", "Ocirc"}, // Ф - uppercase O, circumflex accent
        {"\u00D5", "Otilde"}, // Х - uppercase O, tilde
        {"\u00D6", "Ouml"}, // Ц - uppercase O, umlaut
        {"\u00D7", "times"}, // multiplication sign
        {"\u00D8", "Oslash"}, // Ш - uppercase O, slash
        {"\u00D9", "Ugrave"}, // Щ - uppercase U, grave accent
        {"\u00DA", "Uacute"}, // Ъ - uppercase U, acute accent
        {"\u00DB", "Ucirc"}, // Ы - uppercase U, circumflex accent
        {"\u00DC", "Uuml"}, // Ь - uppercase U, umlaut
        {"\u00DD", "Yacute"}, // Э - uppercase Y, acute accent
        {"\u00DE", "THORN"}, // Ю - uppercase THORN, Icelandic
        {"\u00DF", "szlig"}, // Я - lowercase sharps, German
        {"\u00E0", "agrave"}, // а - lowercase a, grave accent
        {"\u00E1", "aacute"}, // б - lowercase a, acute accent
        {"\u00E2", "acirc"}, // в - lowercase a, circumflex accent
        {"\u00E3", "atilde"}, // г - lowercase a, tilde
        {"\u00E4", "auml"}, // д - lowercase a, umlaut
        {"\u00E5", "aring"}, // е - lowercase a, ring
        {"\u00E6", "aelig"}, // ж - lowercase ae
        {"\u00E7", "ccedil"}, // з - lowercase c, cedilla
        {"\u00E8", "egrave"}, // и - lowercase e, grave accent
        {"\u00E9", "eacute"}, // й - lowercase e, acute accent
        {"\u00EA", "ecirc"}, // к - lowercase e, circumflex accent
        {"\u00EB", "euml"}, // л - lowercase e, umlaut
        {"\u00EC", "igrave"}, // м - lowercase i, grave accent
        {"\u00ED", "iacute"}, // н - lowercase i, acute accent
        {"\u00EE", "icirc"}, // о - lowercase i, circumflex accent
        {"\u00EF", "iuml"}, // п - lowercase i, umlaut
        {"\u00F0", "eth"}, // р - lowercase eth, Icelandic
        {"\u00F1", "ntilde"}, // с - lowercase n, tilde
        {"\u00F2", "ograve"}, // т - lowercase o, grave accent
        {"\u00F3", "oacute"}, // у - lowercase o, acute accent
        {"\u00F4", "ocirc"}, // ф - lowercase o, circumflex accent
        {"\u00F5", "otilde"}, // х - lowercase o, tilde
        {"\u00F6", "ouml"}, // ц - lowercase o, umlaut
        {"\u00F7", "divide"}, // division sign
        {"\u00F8", "oslash"}, // ш - lowercase o, slash
        {"\u00F9", "ugrave"}, // щ - lowercase u, grave accent
        {"\u00FA", "uacute"}, // ъ - lowercase u, acute accent
        {"\u00FB", "ucirc"}, // ы - lowercase u, circumflex accent
        {"\u00FC", "uuml"}, // ь - lowercase u, umlaut
        {"\u00FD", "yacute"}, // э - lowercase y, acute accent
        {"\u00FE", "thorn"}, // ю - lowercase thorn, Icelandic
        {"\u00FF", "yuml"}, // я - lowercase y, umlaut
    };

	    private static final int MIN_ESCAPE = 2;
	    private static final int MAX_ESCAPE = 6;

	    private static final HashMap<String, CharSequence> lookupMap;
	    static {
	        lookupMap = new HashMap<String, CharSequence>();
	        for (final CharSequence[] seq : ESCAPES)
	            lookupMap.put(seq[1].toString(), seq[0]);
	    }

}
