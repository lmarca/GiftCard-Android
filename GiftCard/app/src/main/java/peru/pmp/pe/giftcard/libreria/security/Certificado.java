package peru.pmp.pe.giftcard.libreria.security;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;

import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.utilitarios.BCD;
import peru.pmp.pe.giftcard.utilitarios.Soporte;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.security.auth.x500.X500Principal;

@SuppressWarnings("deprecation")
public class Certificado {

	protected KeyStore keyStore;
	protected TrustManagerFactory trustManager;
	protected MyTrustManager trustOnFlyManager;
	protected SSLSocketFactory socketFactory;
		
	/******** Crea un certificado digital y lo almacena en el keystore local *************/
	@SuppressLint("TrulyRandom")
	@TargetApi(Build.VERSION_CODES.KITKAT)
	public static KeyPair generateKeyPair(Context context, String alias)
	        throws GeneralSecurityException {
	    final Calendar start = new GregorianCalendar();
	    final Calendar end = new GregorianCalendar();
	    end.add(Calendar.YEAR, 25);
	    final KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
	            .setAlias(alias)
	            .setKeyType("RSA")
            	.setKeySize(2048)
            	.setSubject(new X500Principal("CN=" + alias + ", OU=procesos mc, O=procesos mc, C=PE"))
	            .setSerialNumber(BigInteger.ONE)
	            .setStartDate(start.getTime())
	            .setEndDate(end.getTime())
	            .build();
	    final KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA", KeyStore.getDefaultType());
	    gen.initialize(spec);
	    return gen.generateKeyPair();
	}
	
	public static boolean saveCertificado(KeyPair keyPair, String pathPrivate, String pathPublic){
		FileOutputStream fos;
		BufferedOutputStream buffer;
		try{
			/// Llave privada
			fos = Memoria.getContext().openFileOutput(pathPrivate, Context.MODE_PRIVATE);
			buffer = new BufferedOutputStream(fos);
			buffer.write(keyPair.getPrivate().getEncoded());
			buffer.flush();
			buffer.close();
			fos.close();
			/// Llave pública
			fos = Memoria.getContext().openFileOutput(pathPublic, Context.MODE_PRIVATE);
			buffer = new BufferedOutputStream(fos);
			buffer.write(keyPair.getPrivate().getEncoded());
			buffer.flush();
			buffer.close();
			fos.close();
			return true;
		}catch(Exception e){
			d("Certificado.saveCertificado:"+e.getMessage());
		}finally{
			fos = null;
			buffer = null;
		}
		return false;
	}
	
	/// Cargar el certificado del sistema de archivos
	private Certificate loadCertificate(String filename) throws Exception {
		// Load CAs from an InputStream 
		// (could be from a resource or ByteArrayInputStream or ...)
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		// From https://www.washington.edu/itconnect/security/ca/load-der.crt
		Certificate certificado = null;
		InputStream caInput = new BufferedInputStream(
				this.getClass().getClassLoader().getResourceAsStream(filename));
		
		try {
			certificado = cf.generateCertificate(caInput);
			
			showCertificate(new X509Certificate[]{(X509Certificate)certificado});
			
		} finally {
		    caInput.close();
		    caInput = null;
		}
		return certificado;
	}
	
	public static void showCertificate(X509Certificate[] chain) throws Exception {
		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		for (int i = 0; i < chain.length; i++) {
			X509Certificate cert = chain[i];
			d(" " + (i + 1) + " Subject " + cert.getSubjectDN());
			d("   Issuer  " + cert.getIssuerDN());
			sha1.update(cert.getEncoded());
			d("   sha1    " + BCD.byteArrayToStringHex(sha1.digest()));
			md5.update(cert.getEncoded());
			d("   md5     " + BCD.byteArrayToStringHex(md5.digest()));
			d("");
		}
	}	
	/*** Instala un certificado desde un servidor web ****/
	public static boolean installCertificate(String host, int port, String protocolo)
			throws Exception {
		
		Certificado me = new Certificado();
		
		/// 1. Se abre el keystore del sistema operativo (por confirmar si es del OS)
		me.loadKeyStore();
	    
		/// 2. Se crea el contexto SSL
	    if(me.preloadSSLContext("TLS")){
	    	
	    	/*Socket tunnel = null;
			if(PROXY){
				tunnel = new Socket(tunnelHost, tunnelPort);
				doTunnelHandshake(tunnel, host, port);
			}*/					

			SSLSocket socket;
			
			socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket();

			try{
				socket.connect(new InetSocketAddress(host, port), 10000);
				
				d("Starting SSL handshake...");
				socket.startHandshake();
				
				/// SSLSocket no realiza verificacion de hostname
				MyHostnameVerifier hv = new MyHostnameVerifier();
				SSLSession s = socket.getSession();
				if (!hv.verify(host, s)) {
				    throw new SSLHandshakeException("Host "+host+" no ha sido encontrado. (" + s.getPeerHost()+ ")");
				}
				hv = null;

				d("No hay errores, certificados son de confianza!");
				
			}catch(IOException v){
				if(v instanceof SSLHandshakeException){
					d("Error de negociacion de Certificado | " + v.getMessage());
					showCertificate(me.trustOnFlyManager.chain);
			    	for(int k = 0; k<me.trustOnFlyManager.chain.length; k++)
			    	{
			        	X509Certificate cert = me.trustOnFlyManager.chain[k];
			        	String alias = host + "-" + (k + 1);
			        	me.keyStore.setCertificateEntry(alias, cert);
			        	d("Adicionando certificado to keystore using alias '" + alias + "'");
			    	}					
				}else{
					d("loadSocketSecure:"+v.getMessage());
					return false;
				}
				///e.printStackTrace();
			}catch(Exception e){
				d("loadSocketSecure:"+e.getMessage());
				return false;
			}finally{
				if(socket!=null)
					socket.close();
			}	    	
	    }
	    
	    me = null;

	    return true;
	}
	
	public boolean loadKeyStore() throws Exception {
		String keyStoreType = KeyStore.getDefaultType();
	    keyStore = KeyStore.getInstance(keyStoreType); ///"AndroidKeyStore");
	    keyStore.load(null);
	    Certificate cert = loadCertificate("images/procesos.png");
	    keyStore.setCertificateEntry("testws.punto-web.com", cert);
		return true;
	}
	
	public boolean preloadSSLContext(String protocolo) throws Exception {
		
		createAdminConfianza(keyStore);
		
		if(trustManager!=null){
			byte[] bites = new byte[48];
			/** Se recomienda invocar 'nextBytes' despues de instanciar SecureRandom ***/
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG"); ///, "SUN");
			secureRandom.nextBytes(bites);
			// Create an SSLContext that uses our TrustManager
			SSLContext contexto = SSLContext.getInstance(protocolo);
			if(trustOnFlyManager!=null)
				contexto.init(new KeyManager[]{}, new TrustManager[]{trustOnFlyManager}, secureRandom);
			else
				contexto.init(null, trustManager.getTrustManagers(), null);
			bites = null;
			
			socketFactory = contexto.getSocketFactory();
			
			return true;
		}
		
		return false;
	}	
	
	// Create a TrustManager that trusts the CAs in our KeyStore PERSONALIZED
	private boolean createAdminConfianza(KeyStore keyStore)
			throws Exception {
		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		trustManager = TrustManagerFactory.getInstance(tmfAlgorithm);
		trustManager.init(keyStore);
		/**codigo para utilizar el trustmanager personalizado || si carga al vuelo el certificado se inicializa 'myTrustManager' **/
		/*X509TrustManager defaultTM = null;
		int len = trustManager.getTrustManagers().length;
		for(int z=0; z<len; z++){
			defaultTM = (X509TrustManager)trustManager.getTrustManagers()[z];
			trustOnFlyManager = new MyTrustManager(defaultTM);
		}*/
		return true;
	}
	
	public SSLSocketFactory getSSLSocketFactory(){
		return socketFactory;
	}
	
	private static void d(String texto){
		Soporte.dump(texto);
	}
}
