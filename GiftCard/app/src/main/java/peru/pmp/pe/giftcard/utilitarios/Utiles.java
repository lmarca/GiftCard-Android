package peru.pmp.pe.giftcard.utilitarios;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;

import peru.pmp.pe.giftcard.baseclass.Constantes;
import peru.pmp.pe.giftcard.libreria.Texto;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Utiles {


	public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

		Date parsed = null;
		String outputDate = "";

		SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
		SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

		try {
			parsed = df_input.parse(inputDate);
			outputDate = df_output.format(parsed);

		} catch (ParseException e) {
			Soporte.dump("ParseException - dateFormat", e.getLocalizedMessage());
		}

		return outputDate;

	}

	public static byte[] calculoCabecera(int longitudTrama) {
		byte[] bites = new byte[2];
		int calculo1 = (longitudTrama / 256);
		int calculo2 = (longitudTrama % 256);
		// /Soporte.dump("calculoCabecera: "+ calculo1+ " "+ calculo2);
		bites[0] = (byte) calculo1;
		bites[1] = (byte) calculo2;
		return bites;
	}

	public static byte[] calculoCabeceraTrunc(long longitudTrama) {
		byte[] bites;
		int calculo1 = 0;
		int calculo2 = 0;
		if (longitudTrama > 256) {
			bites = new byte[2];
			calculo1 = ((int) longitudTrama / 256);
			calculo2 = ((int) longitudTrama % 256);
			bites[0] = (byte) calculo1;
			bites[1] = (byte) calculo2;
		} else {
			bites = new byte[1];
			calculo1 = (int) longitudTrama;
			bites[0] = (byte) calculo1;
		}
		// /Soporte.dump("calculoCabecera: "+ calculo1+ " "+ calculo2);
		return bites;
	}

	public static String devolverNumTransacConFormato(int numTransacInicial) {
		String num = Texto.padLeft(numTransacInicial + "",
				Constantes.LEN_MAX_NUMTRANSACTION, "0");
		return num;
	}

	public static int getTalla(byte[] talla) {
		return getTalla(talla, 0);
	}

	public static int getTalla(byte[] talla, int offset) {
		if (talla == null || talla.length == 0)
			return 0;
		int cociente = talla[offset];
		int residuo = talla[offset + 1];
		int tallaCalculada = 256 * cociente + residuo;
		return tallaCalculada;
	}

	public static boolean checkSW1(byte[] paramArrayOfByte, int paramInt) {
		boolean bool = true;
		if ((paramArrayOfByte == null) || (paramArrayOfByte.length < 1))
			bool = false;
		if ((0xFF & paramArrayOfByte[0]) == paramInt)
			return bool;
		return false;
	}

	public static boolean checkSW1SW2(byte[] paramArrayOfByte) {
		boolean bool = true;
		if ((paramArrayOfByte == null) || (paramArrayOfByte.length < 2))
			bool = false;
		if (((0xFF & paramArrayOfByte[0]) == 144)
				&& ((0xFF & paramArrayOfByte[1]) == 0))
			return bool;
		return false;
	}

	public static boolean checkSW2(byte[] paramArrayOfByte, int paramInt) {
		boolean bool = true;
		if ((paramArrayOfByte == null) || (paramArrayOfByte.length < 2))
			bool = false;
		while ((0xFF & paramArrayOfByte[1]) == paramInt)
			return bool;
		return false;
	}

	/*- (NSData *) CalculoCabeceraRapida : (NSString *) longitudTrama {
	    
	    NSInteger b = (char *)[longitudTrama integerValue];
	 
	    int calculo1 = (b / 256);
	    int calculo2 = (b % 256);
	    NSMutableData *dataCabecera = [[NSMutableData alloc] initWithCapacity:2];
	    // OJO: importante el cast a (char *) - si no se agrega mÃ¡s de 1 byte a pesar del valor de length:
	    [dataCabecera appendBytes:(char*)&calculo1 length:1];
	    [dataCabecera appendBytes:(char*)&calculo2 length:1];
	    
	    return dataCabecera;
	    
	}
	
	-(NSString *) calculobyte: (NSString *)byte{
	    NSInteger b = (char )[byte integerValue];
	    
	    NSInteger calculo2 = (b % 256);
	    NSMutableData *dataCabecera = [[NSMutableData alloc] initWithCapacity:2];
	    [dataCabecera appendBytes:(char*)&calculo2 length:1];
	    
	    NSString *texto = [NSString stringWithFormat:@"%@",dataCabecera];
	    texto = [texto stringByReplacingOccurrencesOfString:@">" withString:@""];
	    texto = [texto stringByReplacingOccurrencesOfString:@"<" withString:@""];
	    return texto;
	    
	}
	
	+(NSData *)dataForHexString:(NSString*)hexString{
	    
	    if (hexString == nil) {
	        return nil;
	    }
	    
	    
	    const char* ch = [[hexString lowercaseString] cStringUsingEncoding:NSUTF8StringEncoding];
	    NSMutableData* data = [NSMutableData data];
	    while (*ch) {
	        if (*ch == ' ') {
	            continue;
	        }
	        
	        char byte = 0;
	        if ('0' <= *ch && *ch <= '9') {
	            byte = *ch - '0';
	        }
	        
	        else if ('a' <= *ch && *ch <= 'f') {
	            byte = *ch - 'a' + 10;
	        }
	        
	        else if ('A' <= *ch && *ch <= 'F') {
	            byte = *ch - 'A' + 10;
	        }
	        
	        ch++;
	        
	        byte = byte << 4;
	        
	        if (*ch) {
	            if ('0' <= *ch && *ch <= '9') {
	                byte += *ch - '0';
	            } else if ('a' <= *ch && *ch <= 'f') {
	                byte += *ch - 'a' + 10;
	            }
	            else if('A' <= *ch && *ch <= 'F')
	            {
	                byte += *ch - 'A' + 10;
	            }
	            ch++;
	        }
	        
	        [data appendBytes:&byte length:1];
	        
	    }
	    
	    return data;
	    
	}
	
	
	-(NSString *)Obtener_Hora_Actual{
	    NSDateFormatter *hora = [[NSDateFormatter alloc] init];
	    [hora setDateFormat:@"HHmmss"];
	    NSDate *horaActual = [NSDate date];
	    NSString *strHoraActual = [hora stringFromDate:horaActual];
	    return  strHoraActual;
	}
	
	-(NSString *)Obtener_Fecha_Actual{
	    NSDateFormatter *fecha = [[NSDateFormatter alloc] init];
	    [fecha setDateFormat:@"YYYYMMdd"];
	    NSDate *fechaActual = [NSDate date];
	    NSString *strFechaActual = [fecha stringFromDate:fechaActual];
	    return strFechaActual;
	}
	
	- (NSString *)ReplaceXporCant:(NSString *)texto{
	    int cont = 0;
	    NSString *cantidad = @"";
	    
	    for ( int i = 0; i < [texto length]; i++)
	        if ( [texto characterAtIndex:i] == '*'){
	            cont++;
	        }
	    
	    if (cont < 10) {
	        cantidad = [NSString stringWithFormat:@"0%i",cont];
	    }else{
	        cantidad = [NSString stringWithFormat:@"%i",cont];
	    }
	    
	    NSRange range = NSMakeRange(6, cont);
	    NSString *cortar = [NSString stringWithFormat:@"%@",[texto substringWithRange:range]];
	    texto = [texto stringByReplacingOccurrencesOfString:cortar withString:cantidad];
	    return texto;
	}
	
	-(NSString *)obtenerMonto_Formateado:(NSString *)monto{
	    
	    NSString *monto_para_chip = @"";
	    NSInteger cantidad = monto.length;
	    NSLog(@"len %ld",(long)cantidad);
	    
	    if (cantidad < 4) {
	        monto_para_chip = [NSString stringWithFormat:@"000000000%@",monto];
	    }else if ([monto length] < 5){
	        monto_para_chip = [NSString stringWithFormat:@"00000000%@",monto];
	    }else if ([monto length] < 6){
	        monto_para_chip = [NSString stringWithFormat:@"0000000%@",monto];
	    }else if ([monto length] < 7){
	        monto_para_chip = [NSString stringWithFormat:@"000000%@",monto];
	    }else if ([monto length] < 8){
	        monto_para_chip = [NSString stringWithFormat:@"00000%@",monto];
	    }else if ([monto length] < 9){
	        monto_para_chip = [NSString stringWithFormat:@"0000%@",monto];
	    }else if ([monto length] < 10){
	        monto_para_chip = [NSString stringWithFormat:@"000%@",monto];
	    }
	    
	    return monto_para_chip;
	}*/
	@SuppressLint("DefaultLocale")
	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (manufacturer.toLowerCase().contains("unknow"))
			manufacturer = new String(model);
		if (model.startsWith(manufacturer)) {
			return Texto.capitalize(model);
		} else {
			return Texto.capitalize(manufacturer) + " " + model;
		}
	}

	private static String HOST = "190.102.141.102";
	private static int PORT = 8309;
	
	public static void testTLS(Context contexto){
		try{
			
			// Load CAs from an InputStream
			// (could be from a resource or ByteArrayInputStream or ...)
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			// From https://www.washington.edu/itconnect/security/ca/load-der.crt
			InputStream caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));
			Certificate ca;
			try {
			    ca = cf.generateCertificate(caInput);
			    Soporte.dump("ca=" + ((X509Certificate) ca).getSubjectDN());
			} finally {
			    caInput.close();
			}
			/*
			KeyStore keyStore = KeyStore.getInstance( "PKCS12" );
			InputStream inputStreamStore = contexto.getResources().openRawResource(R.raw.server);
			String pkcs12 = UserSession.getCertificate( context );
			InputStream sslInputStream = new ByteArrayInputStream( MyBase64Decoder.decode( pkcs12.getBytes() ) );
			keyStore.load( sslInputStream, "password".toCharArray() );
			
			/*
            //set necessary keystore properties - using a p12 file
            System.setProperty("javax.net.ssl.keyStore","D:/PGM/PRUEBA/Clienttls2/build/classes/clienttls2/client.p12");
            System.setProperty("javax.net.ssl.keyStorePassword","12345678");
            System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");    
   
            //set necessary truststore properties - using JKS
            System.setProperty("javax.net.ssl.trustStore","D:/PGM/PRUEBA/Clienttls2/build/classes/clienttls2/Truststore.jks");
            System.setProperty("javax.net.ssl.trustStorePassword","12345678");
            
            System.setProperty("javax.net.debug", "all");
            // register a https protocol handler  - this may be required for previous JDK versions
            //System.setProperty("java.protocol.handler.pkgs","com.sun.net.ssl.internal.www.protocol");

			InstallCert installcert = new InstallCert(HOST, PORT);  
	        KeyStore trustStore = installcert.getUpdatedTrustStore();  
	        if(trustStore == null) {  
	            return;  
	        }  
	        String serverResponse = "Cert";  
	        ///DefaultHttpClient httpclient = new DefaultHttpClient();  
	        ///KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());  
	        //here loading custom client certificate  
	        InputStream instream = contexto.getResources().openRawResource(R.raw.keystore);  
	        try {  
	        	keyStore.load(instream, "12345678".toCharArray());  
	        } finally {  
	            instream.close();  
	        }
	        //creating SSLSocketFactory using custom keystore and updated truststore  
	        SSLSocketFactory socketFactory = new SSLSocketFactory(keyStore, "12345678", trustStore);  

	        ///SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();            
            ///SSLSocket sslSock = (SSLSocket) factory.createSocket("server.mc.com.pe",8887);

	        SSLSocket sslSock = (SSLSocket) socketFactory.createSocket(null, "190.102.141.102", 8309, false);

            sslSock.setEnabledCipherSuites(new String[] {
            "SSL_RSA_WITH_RC4_128_MD5",
            "SSL_RSA_WITH_RC4_128_SHA",
            "TLS_RSA_WITH_AES_128_CBC_SHA",
            "TLS_DHE_RSA_WITH_AES_128_CBC_SHA",
            "TLS_DHE_DSS_WITH_AES_128_CBC_SHA",
            "SSL_RSA_WITH_3DES_EDE_CBC_SHA",
            "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA",
            "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA",
            "SSL_RSA_WITH_DES_CBC_SHA",
            "SSL_DHE_RSA_WITH_DES_CBC_SHA",
            "SSL_DHE_DSS_WITH_DES_CBC_SHA",
            "SSL_RSA_EXPORT_WITH_RC4_40_MD5",
            "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA",
            "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA",
            "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA",
            "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"});

            sslSock.startHandshake();
                       
            //send HTTP get request
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(sslSock.getOutputStream(), "UTF8"));            
            wr.write("hola");
            wr.flush();
             
            // read response
            BufferedReader rd = new BufferedReader(new InputStreamReader(sslSock.getInputStream()));           
            String string = null;

            while ((string = rd.readLine()) != null) {
                System.out.println(string);
                System.out.flush();
            }
           
            rd.close();
            wr.close();
            // Close connection.
            sslSock.close();
           
            */
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
		
	}
	

}
