package peru.pmp.pe.giftcard.entidades;

import peru.pmp.pe.giftcard.baseclass.Entidad;

/**
 * Created by Admin on 28/08/17.
 */

public class ConsultaSaldosE extends Entidad{

    private String codEmisor;
    private String codUsuario;
    private String numTerminal;
    private String numTarjetaMonedero;
    private String numReferencia;
    private String fechaExpiracion;
    private String comercio;
    private String moneda;
    private String fechaTxnTerminal;
    private String horaTxnTerminal;
    private String reservado;
    private String idTransaccion;
    private String CodRespuesta;
    private String DescRespuesta;
    private String CodAutorizacion;
    private String SaldoMoneda;
    private String SaldoCodBloqueo;
    private String SaldoDescBloqueo;
    private String SaldoNombreTarjeta;
    private String SaldoFechaExpiracion;
    private String SaldoLineaCredito;
    private String SaldoDispConsumo;
    private String SaldoSigDispConsumo;
    private String SaldoEfectivo;
    private String SaldoDispEfectivo;
    private String SaldoSigDispEfectivo;
    private String SaldoPagoMinimo;
    private String SaldoPagoTotal;
    private String SaldoPagoFacturado;
    private String SaldoCuentaPrincipal;
    private String SaldoDispActual;
    private String SaldoSigDispActual;
    private String SaldoFechaUltPago;
    private String SaldoFechaUltFact;
    private String SaldoFechaApertura;
    private String SaldoFormaPago;
    private String SaldoPuntosPlata;
    private String SaldoSigPuntosPlata;
    private String SaldoPuntosPlataCons;
    private String SaldoSigPuntosPlataCons;
    private String SaldoPagoMinimito;
    private String SaldoContable;
    private String SaldoSigContable;
    private String SaldoDiasMora;
    private String SaldoImporteMora;
    private String SaldoCalificacionCliente;
    private String SaldoIndCambioPin;
    private String SaldoNumSeguimiento;
    private String SaldoCodBonus;
    private String SaldoIndNominada;

    public ConsultaSaldosE() {
        super();
        codEmisor = "";
        codUsuario = "";
        numTerminal = "";
        numTarjetaMonedero = "";
        numReferencia = "";
        fechaExpiracion = "";
        comercio = "";
        moneda = "";
        fechaTxnTerminal = "";
        horaTxnTerminal = "";
        reservado = "";
        idTransaccion = "";
        CodRespuesta = "";
        DescRespuesta = "";
        CodAutorizacion = "";
        SaldoMoneda = "";
        SaldoCodBloqueo = "";
        SaldoDescBloqueo = "";
        SaldoNombreTarjeta = "";
        SaldoFechaExpiracion = "";
        SaldoLineaCredito = "";
        SaldoDispConsumo = "";
        SaldoSigDispConsumo = "";
        SaldoEfectivo = "";
        SaldoDispEfectivo = "";
        SaldoSigDispEfectivo = "";
        SaldoPagoMinimo = "";
        SaldoPagoTotal = "";
        SaldoPagoFacturado = "";
        SaldoCuentaPrincipal = "";
        SaldoDispActual = "";
        SaldoSigDispActual = "";
        SaldoFechaUltPago = "";
        SaldoFechaUltFact = "";
        SaldoFechaApertura = "";
        SaldoFormaPago = "";
        SaldoPuntosPlata = "";
        SaldoSigPuntosPlata = "";
        SaldoPuntosPlataCons = "";
        SaldoSigPuntosPlataCons = "";
        SaldoPagoMinimito = "";
        SaldoContable = "";
        SaldoSigContable = "";
        SaldoDiasMora = "";
        SaldoImporteMora = "";
        SaldoCalificacionCliente = "";
        SaldoIndCambioPin = "";
        SaldoNumSeguimiento = "";
        SaldoCodBonus = "";
        SaldoIndNominada = "";
    }

    public String getCodEmisor() {
        return codEmisor;
    }

    public void setCodEmisor(String codEmisor) {
        this.codEmisor = codEmisor;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getNumTerminal() {
        return numTerminal;
    }

    public void setNumTerminal(String numTerminal) {
        this.numTerminal = numTerminal;
    }

    public String getNumTarjetaMonedero() {
        return numTarjetaMonedero;
    }

    public void setNumTarjetaMonedero(String numTarjetaMonedero) {
        this.numTarjetaMonedero = numTarjetaMonedero;
    }

    public String getNumReferencia() {
        return numReferencia;
    }

    public void setNumReferencia(String numReferencia) {
        this.numReferencia = numReferencia;
    }

    public String getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(String fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getComercio() {
        return comercio;
    }

    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getFechaTxnTerminal() {
        return fechaTxnTerminal;
    }

    public void setFechaTxnTerminal(String fechaTxnTerminal) {
        this.fechaTxnTerminal = fechaTxnTerminal;
    }

    public String getHoraTxnTerminal() {
        return horaTxnTerminal;
    }

    public void setHoraTxnTerminal(String horaTxnTerminal) {
        this.horaTxnTerminal = horaTxnTerminal;
    }

    public String getReservado() {
        return reservado;
    }

    public void setReservado(String reservado) {
        this.reservado = reservado;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getCodRespuesta() {
        return CodRespuesta;
    }

    public void setCodRespuesta(String codRespuesta) {
        CodRespuesta = codRespuesta;
    }

    public String getDescRespuesta() {
        return DescRespuesta;
    }

    public void setDescRespuesta(String descRespuesta) {
        DescRespuesta = descRespuesta;
    }

    public String getCodAutorizacion() {
        return CodAutorizacion;
    }

    public void setCodAutorizacion(String codAutorizacion) {
        CodAutorizacion = codAutorizacion;
    }

    public String getSaldoMoneda() {
        return SaldoMoneda;
    }

    public void setSaldoMoneda(String saldoMoneda) {
        SaldoMoneda = saldoMoneda;
    }

    public String getSaldoCodBloqueo() {
        return SaldoCodBloqueo;
    }

    public void setSaldoCodBloqueo(String saldoCodBloqueo) {
        SaldoCodBloqueo = saldoCodBloqueo;
    }

    public String getSaldoDescBloqueo() {
        return SaldoDescBloqueo;
    }

    public void setSaldoDescBloqueo(String saldoDescBloqueo) {
        SaldoDescBloqueo = saldoDescBloqueo;
    }

    public String getSaldoNombreTarjeta() {
        return SaldoNombreTarjeta;
    }

    public void setSaldoNombreTarjeta(String saldoNombreTarjeta) {
        SaldoNombreTarjeta = saldoNombreTarjeta;
    }

    public String getSaldoFechaExpiracion() {
        return SaldoFechaExpiracion;
    }

    public void setSaldoFechaExpiracion(String saldoFechaExpiracion) {
        SaldoFechaExpiracion = saldoFechaExpiracion;
    }

    public String getSaldoLineaCredito() {
        return SaldoLineaCredito;
    }

    public void setSaldoLineaCredito(String saldoLineaCredito) {
        SaldoLineaCredito = saldoLineaCredito;
    }

    public String getSaldoDispConsumo() {
        return SaldoDispConsumo;
    }

    public void setSaldoDispConsumo(String saldoDispConsumo) {
        SaldoDispConsumo = saldoDispConsumo;
    }

    public String getSaldoSigDispConsumo() {
        return SaldoSigDispConsumo;
    }

    public void setSaldoSigDispConsumo(String saldoSigDispConsumo) {
        SaldoSigDispConsumo = saldoSigDispConsumo;
    }

    public String getSaldoEfectivo() {
        return SaldoEfectivo;
    }

    public void setSaldoEfectivo(String saldoEfectivo) {
        SaldoEfectivo = saldoEfectivo;
    }

    public String getSaldoDispEfectivo() {
        return SaldoDispEfectivo;
    }

    public void setSaldoDispEfectivo(String saldoDispEfectivo) {
        SaldoDispEfectivo = saldoDispEfectivo;
    }

    public String getSaldoSigDispEfectivo() {
        return SaldoSigDispEfectivo;
    }

    public void setSaldoSigDispEfectivo(String saldoSigDispEfectivo) {
        SaldoSigDispEfectivo = saldoSigDispEfectivo;
    }

    public String getSaldoPagoMinimo() {
        return SaldoPagoMinimo;
    }

    public void setSaldoPagoMinimo(String saldoPagoMinimo) {
        SaldoPagoMinimo = saldoPagoMinimo;
    }

    public String getSaldoPagoTotal() {
        return SaldoPagoTotal;
    }

    public void setSaldoPagoTotal(String saldoPagoTotal) {
        SaldoPagoTotal = saldoPagoTotal;
    }

    public String getSaldoPagoFacturado() {
        return SaldoPagoFacturado;
    }

    public void setSaldoPagoFacturado(String saldoPagoFacturado) {
        SaldoPagoFacturado = saldoPagoFacturado;
    }

    public String getSaldoCuentaPrincipal() {
        return SaldoCuentaPrincipal;
    }

    public void setSaldoCuentaPrincipal(String saldoCuentaPrincipal) {
        SaldoCuentaPrincipal = saldoCuentaPrincipal;
    }

    public String getSaldoDispActual() {
        return SaldoDispActual;
    }

    public void setSaldoDispActual(String saldoDispActual) {
        SaldoDispActual = saldoDispActual;
    }

    public String getSaldoSigDispActual() {
        return SaldoSigDispActual;
    }

    public void setSaldoSigDispActual(String saldoSigDispActual) {
        SaldoSigDispActual = saldoSigDispActual;
    }

    public String getSaldoFechaUltPago() {
        return SaldoFechaUltPago;
    }

    public void setSaldoFechaUltPago(String saldoFechaUltPago) {
        SaldoFechaUltPago = saldoFechaUltPago;
    }

    public String getSaldoFechaUltFact() {
        return SaldoFechaUltFact;
    }

    public void setSaldoFechaUltFact(String saldoFechaUltFact) {
        SaldoFechaUltFact = saldoFechaUltFact;
    }

    public String getSaldoFechaApertura() {
        return SaldoFechaApertura;
    }

    public void setSaldoFechaApertura(String saldoFechaApertura) {
        SaldoFechaApertura = saldoFechaApertura;
    }

    public String getSaldoFormaPago() {
        return SaldoFormaPago;
    }

    public void setSaldoFormaPago(String saldoFormaPago) {
        SaldoFormaPago = saldoFormaPago;
    }

    public String getSaldoPuntosPlata() {
        return SaldoPuntosPlata;
    }

    public void setSaldoPuntosPlata(String saldoPuntosPlata) {
        SaldoPuntosPlata = saldoPuntosPlata;
    }

    public String getSaldoSigPuntosPlata() {
        return SaldoSigPuntosPlata;
    }

    public void setSaldoSigPuntosPlata(String saldoSigPuntosPlata) {
        SaldoSigPuntosPlata = saldoSigPuntosPlata;
    }

    public String getSaldoPuntosPlataCons() {
        return SaldoPuntosPlataCons;
    }

    public void setSaldoPuntosPlataCons(String saldoPuntosPlataCons) {
        SaldoPuntosPlataCons = saldoPuntosPlataCons;
    }

    public String getSaldoSigPuntosPlataCons() {
        return SaldoSigPuntosPlataCons;
    }

    public void setSaldoSigPuntosPlataCons(String saldoSigPuntosPlataCons) {
        SaldoSigPuntosPlataCons = saldoSigPuntosPlataCons;
    }

    public String getSaldoPagoMinimito() {
        return SaldoPagoMinimito;
    }

    public void setSaldoPagoMinimito(String saldoPagoMinimito) {
        SaldoPagoMinimito = saldoPagoMinimito;
    }

    public String getSaldoContable() {
        return SaldoContable;
    }

    public void setSaldoContable(String saldoContable) {
        SaldoContable = saldoContable;
    }

    public String getSaldoSigContable() {
        return SaldoSigContable;
    }

    public void setSaldoSigContable(String saldoSigContable) {
        SaldoSigContable = saldoSigContable;
    }

    public String getSaldoDiasMora() {
        return SaldoDiasMora;
    }

    public void setSaldoDiasMora(String saldoDiasMora) {
        SaldoDiasMora = saldoDiasMora;
    }

    public String getSaldoImporteMora() {
        return SaldoImporteMora;
    }

    public void setSaldoImporteMora(String saldoImporteMora) {
        SaldoImporteMora = saldoImporteMora;
    }

    public String getSaldoCalificacionCliente() {
        return SaldoCalificacionCliente;
    }

    public void setSaldoCalificacionCliente(String saldoCalificacionCliente) {
        SaldoCalificacionCliente = saldoCalificacionCliente;
    }

    public String getSaldoIndCambioPin() {
        return SaldoIndCambioPin;
    }

    public void setSaldoIndCambioPin(String saldoIndCambioPin) {
        SaldoIndCambioPin = saldoIndCambioPin;
    }

    public String getSaldoNumSeguimiento() {
        return SaldoNumSeguimiento;
    }

    public void setSaldoNumSeguimiento(String saldoNumSeguimiento) {
        SaldoNumSeguimiento = saldoNumSeguimiento;
    }

    public String getSaldoCodBonus() {
        return SaldoCodBonus;
    }

    public void setSaldoCodBonus(String saldoCodBonus) {
        SaldoCodBonus = saldoCodBonus;
    }

    public String getSaldoIndNominada() {
        return SaldoIndNominada;
    }

    public void setSaldoIndNominada(String saldoIndNominada) {
        SaldoIndNominada = saldoIndNominada;
    }

}
