package peru.pmp.pe.giftcard.baseclass;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.activity.MainActivity;
import peru.pmp.pe.giftcard.libreria.ProgressBarHandler;
import peru.pmp.pe.giftcard.libreria.Texto;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.utilitarios.Soporte;

public class MyActivity extends AppCompatActivity implements Constantes,
        OnClickListener, OnItemClickListener, OnKeyListener,
        OnCheckedChangeListener, OnItemSelectedListener {

	Intent _intent = new Intent();

	private static Class<?> _backToClass = null;

	protected TextView lblMensaje;
	private Thread _timer;
	private static Toast _toast;
	private boolean _doneTimer, _done, _pause;
	private int _timeTimer;
	private Dialog dialog;
	private String _className;
	private int _height, _width;
	//protected ProgressBarHandler _progressBar;

    private ProgressDialog progressDialog;

	// /private ImageView imagenSettings;

	private Semaphore semaforo;

	protected RelativeLayout mainLayout;

	/******
	 * Metodo invocado automaticamente cuando es instanciada la clase
	 * 
	 * @param Bundle
	 *            Objeto relacionado con el formulario
	 * @return none
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//setTheme(android.R.style.Theme_InputMethod);
		// /setTheme(android.R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth);

		super.onCreate(savedInstanceState);
		setTitle("GiftCard");

		// /setTitleColor(Color.WHITE);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);

		mainLayout = buildMainLayoutRelative(this);

		_className = getLocalClassName();
		_backToClass = null;

		View v = findViewById(android.R.id.title);
		v.setClickable(true);
		v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openOptionsMenu();
				// msg("Works!");
			}
		});

		//_progressBar = new ProgressBarHandler(this);
        progressDialog = new ProgressDialog(this);
		_done = false;
		_pause = false;

		popupOnOff = false; // // Nuevo 25/09/2015

		semaforo = null;

	}

	public void onPostCreated() {
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Memoria.setContext(this);

		/*
		 * if(Memoria.isProcesando()) /// || !Memoria.isCredenciales())
		 * transparenciaOn(); else transparenciaOff();
		 */

		cancelToast();

		_pause = false;

		showStatus();

	}

	@Override
	protected void onStop() {
		super.onStop();
		_pause = true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		timerOff();
		dismissDialog();
		_done = true;
		_pause = true;
		// /overridePendingTransition(R.anim.slide_in_right,
		// R.anim.slide_out_left);
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		Soporte.dump("onAttachedToWindow");
		if (mainLayout.getLayoutParams() instanceof MarginLayoutParams) {
			// if getLayoutParams() returns null, the if condition will be false
			MarginLayoutParams layoutParams = (MarginLayoutParams) mainLayout
					.getLayoutParams();
			layoutParams.width = android.widget.RelativeLayout.LayoutParams.MATCH_PARENT;
			///layoutParams.height = android.widget.RelativeLayout.LayoutParams.MATCH_PARENT;
			layoutParams.setMargins(dipToPixels(3), dipToPixels(10), dipToPixels(5), dipToPixels(20));
			mainLayout.setPadding(dipToPixels(1), dipToPixels(0), dipToPixels(1), dipToPixels(1));
			setCustomView(mainLayout, 0, Colomural.GRAY34.color(), Colomural.GRAY34.color());

			mainLayout.requestLayout();
		}
	}

	/******
	 * Metodo utilizado para enviar parametros al siguiente formulario
	 *
	 * @param String
	 *            name Nombre del parametro
	 * @param String
	 *            value Valor del parametro
	 * @return none
	 */
	public void putExtra(String name, String value) {
		_intent.putExtra(name, value);
	}

	/******
	 * Metodo utilizado para enviar parametros al siguiente formulario
	 *
	 * @param String
	 *            name Nombre del parametro
	 * @param Entidad
	 *            value Valor del parametro
	 * @return none
	 */
	public void putExtra(String name, Entidad value) {
		_intent.putExtra(name, value);
	}

	public void removeExtra(String name) {
		_intent.removeExtra(name);
	}

	/******
	 * Metodo utilizado para obtener parametros del actual formulario
	 *
	 * @param String
	 *            name Nombre del parametro
	 * @return Entidad value Valor del parametro
	 */
	public Entidad getExtra(String name) {
		Entidad value = (Entidad) _intent.getSerializableExtra(name);
		return value;
	}

	public String getStringExtra(String name) {
		if (getIntent().getExtras() != null)
			return getIntent().getExtras().getString(name);
		else
			return null;
	}

	/******
	 * Metodo utilizado para invocar un formulario
	 *
	 * @param Class
	 *            Clase referencial del formulario a invocar
	 * @return none
	 */
	public void show(Class<?> clase) {
		_intent.setClass(this, clase);
		Soporte.dump(getClass().getName() + " llamando a " + clase.getName());
		hideProgressBar();
		startActivity(_intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	public void backTo(Class<?> clase) {
		_backToClass = clase;
		/*
		 * if (this.getClass() == _backToClass || this.getClass() ==
		 * Mpos10inicio.class)
		 */
		if (this.getClass() == _backToClass) {
			return;
		} else
			finish();
	}

    public void toast(String mensaje) {

        final AlertDialog alertDialog = new AlertDialog.Builder(Memoria.getContext()).create();
        alertDialog.setTitle("Gift Card");
        alertDialog.setMessage(mensaje);
        alertDialog.show();

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                alertDialog.dismiss();
                timer.cancel(); //this will cancel the timer of the system
            }
        }, 4000); // the timer will count 5 seconds....

    }

    public void finishSession() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder .setTitle("Gift Card")
                .setMessage("¿Estás seguro? Vas a cerrar sesión?")
                .setCancelable(false)
                .setPositiveButton("SI", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                        Memoria.deleteUser();
                        show(MainActivity.class);
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

	/******
	 * Metodo utilizado para invocar mostrar un mensaje emergente de duracion
	 * corta en el formulario
	 *
	 * @param String
	 *            Texto del mensaje
	 * @return none
	 */
	public void msg(String texto) {
		msg(texto, Toast.LENGTH_SHORT);
	}

	/******
	 * Metodo utilizado para invocar mostrar un mensaje emergente de duracion
	 * controlada en el formulario
	 *
	 * @param String
	 *            Texto del mensaje
	 * @param int Tiempo de duracion del mensaje. Puede ser Toast.LENGTH_SHORT
	 *        ó Toast.LENGTH_LONG
	 * @return none
	 */
	public void msg(final String texto, final int tiempo) {

		//Memoria.getOperacion().sendBackPaymentHandler("Mensaje:" + texto);

		runOnUiThread(new Runnable() {
			public void run() {
				if (_toast != null)
					_toast.cancel();
				SpannableStringBuilder biggerText = new SpannableStringBuilder(
						texto);
				biggerText.setSpan(new RelativeSizeSpan(1.3f), 0,
						texto.length(), 0);
				_toast = Toast.makeText(Memoria.getContext(), biggerText,
						tiempo);
				_toast.setGravity(Gravity.CENTER, _toast.getXOffset() / 2,
						_toast.getYOffset() / 2);
				_toast.show();

			}
		});
	}

	public void msgNormal(final String texto) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (_toast != null)
					_toast.cancel();
				_toast = Toast.makeText(Memoria.getContext(), texto,
						Toast.LENGTH_LONG);
				_toast.show();
			}
		});
	}

	public void cancelToast() {
		if (_toast != null)
			_toast.cancel();
		_toast = null;
	}

	/******
	 * Metodo invocado automaticamente cuando se realiza la seleccion de un
	 * elemento del formulario
	 *
	 * @return none
	 */
	public void onClick(View v) {
	}

	/******
	 * Metodo invocado automaticamente cuando se selecciona un elemento de una
	 * lista
	 *
	 * @param View
	 *            Lista relacionada
	 * @param int Posicion del elemento seleccionado
	 * @return none
	 */
	public void onItemClick(AdapterView<?> adapter, View v, int position,
                            long id) {
	}

	/******
	 * Metodo invocado automaticamente cuando se presiona una tecla
	 *
	 * @param View
	 *            Lista relacionada
	 * @param int Codigo de tecla
	 * @return none
	 */
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_ENTER) {
//			return true;
//		}
//		return false;
//	}

	/******
	 * Metodo invocado automaticamente cuando se selecciona un spinner
	 *
	 * @param View
	 *            Spinner relacionado
	 * @param int Posicion del elemento seleccionado
	 * @return none
	 */
	@Override
	public void onItemSelected(AdapterView<?> adapter, View v, int position,
                               long id) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	/******
	 * Metodo utilizado para que contenga la validacion de datos en los
	 * formularios
	 *
	 * @return boolean True o false depende del resultado
	 */
	public boolean validaDatos() {
		// /throw new
		// UnsupportedOperationException("validaDatos Not supported yet.");
		Soporte.dump(getClass().getName() + ":validaDatos not supported yet");
		return false;
	}

	/******
	 * Metodo utilizado para obtener los datos ingresados en los formularios
	 *
	 * @return none
	 */
	public void obtenerValores() {
		// /throw new
		// UnsupportedOperationException("obtenerValores Not supported yet.");
		Soporte.dump(getClass().getName() + ":obtenerValores not supported yet");
	}

	/******
	 * Metodo utilizado para asignar los valores en los campos de los
	 * formularios
	 *
	 * @return none
	 */
	public void asignarValores() {
		// /throw new
		// UnsupportedOperationException("asignarValores Not supported yet.");
		Soporte.dump(getClass().getName() + ":asignarValores not supported yet");
	}

	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		// TODO Auto-generated method stub

	}

	/******
	 * Metodo que permite enviar un texto desde otro procesos al current form
	 */
	public void msgNotify(final String texto) {
		msg(texto);
	}

	/******
	 * Metodo que permite enviar un texto desde otro procesos al current form
	 */
	public void textNotify(final String texto) {
		showProgressBar(texto);
	}

	/******
	 * Metodo que permite enviar un bit desde otro procesos al current form
	 */
	public void bitNotify(boolean valor) {

	}

	/******
	 * Metodo que permite notificar el final de un proceso Aqui no invocamos los
	 * dialogos de newpass o actualizar pwd, porque se crearia un ciclo
	 * recursivo.
	 */
	public void finishNotify() {
	}

	public void timerOff() {
		_doneTimer = true;
	}

	public void setDone() {
		_done = true;
	}

	public void setTimer(final int segundos) {
		if(_timer!=null){
			timerOff();
			_timer.interrupt();
		}
		_timer = new Thread(new Runnable() {

			public void run() {
				Soporte.dump("Payment - init timer");
				int seg = 0;
				boolean reset = false;
				Soporte.dump("setTimer = " + segundos + " (_doneTimer=="
						+ _doneTimer + ")");
				while (!_doneTimer) {
					Soporte.sleep(1000);
					seg++;
					if (seg >= segundos) {
						reset = true;
						break;
					}
				}
				Soporte.dump("Payment - end timer");
				if (reset) {
					Soporte.dump("TimerOff = SI RESET ");
		    		//Memoria.getOperacion().setMensajeRpta(CodigoRetorno.TIMEOUT_OPERACION);
					//resetOperacion();
				} else
					Soporte.dump("TimerOff = NO RESET ");
			}
		});
		_timer.start();
	}

	public void alert(final String msg) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (lblMensaje == null)
					return;
				lblMensaje.setBackgroundColor(Color.RED);
				lblMensaje.setText(msg);
			}
		});
	}

	public void warning(final String msg) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (lblMensaje == null)
					return;
				lblMensaje.setBackgroundColor(Color.CYAN);
				lblMensaje.setText(msg);
			}
		});
	}

	public void success(final String msg) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (lblMensaje == null)
					return;
				lblMensaje.setBackgroundColor(Color.GREEN);
				lblMensaje.setText(msg);
			}
		});
	}

	public void showMessage(String texto) {
		showMessage(texto, 10);
	}

	public void showMessage(final String texto, final int timer) {
		runOnUiThread(new Runnable() {
			public void run() {
				/*
				 * Mpos20mensaje.MENSAJE = texto; Mpos20mensaje.TIMER = timer;
				 * show(Mpos20mensaje.class);
				 */
			}
		});
	}

	@SuppressLint("NewApi")
	public void transparenciaOn() {
		runOnUiThread(new Runnable() {
			public void run() {
				RelativeLayout ll = (RelativeLayout) mainLayout;
				if (ll != null) {
					ll.setAlpha(0.98f);
					onOffChildsOnTouch(ll, false);
					ll = null;
				}
			}
		});
	}

	public void onOffChildsOnTouch(ViewGroup viewGroup, boolean valor) {
		int cnt = viewGroup.getChildCount();
		for (int i = 0; i < cnt; i++) {
			View v = viewGroup.getChildAt(i);
			if (v instanceof ViewGroup) {
				onOffChildsOnTouch((ViewGroup) v, valor);
			} else {
				v.setEnabled(valor);
			}
		}
	}

	@SuppressLint("NewApi")
	public void transparenciaOff() {
		runOnUiThread(new Runnable() {
			public void run() {
				RelativeLayout ll = (RelativeLayout) mainLayout;
				if (ll != null) {
					ll.setAlpha(1);
					onOffChildsOnTouch(ll, true);
					ll = null;
				}
			}
		});
	}

	public static StatusGeneral CURRENT_STATUS = StatusGeneral.NO_INICIADO;

	public void setStatus(StatusGeneral status) {
		CURRENT_STATUS = status;
		showStatus();
	}

	public void showStatus() {
		runOnUiThread(new Runnable() {
			public void run() {

			}
		});
	}

	public void dismissDialog() {
		if (dialog != null) {
			dialog.dismiss();
			dialog = null;
		}
		if (progressDialog != null)
			progressDialog.dismiss();

		popupOnOff = false;
	}

	private static boolean popupOnOff = false;

	/************************
	 * Pasos: 1. Activar el chip Bluetooh 2. Leer los parametros del ultimo
	 * dispositivo conectado 3. Realizar una conexion con el ultimo dispositivo
	 * conectado 4. Si no hay info o no se pudo conectar con el ultimo
	 * dispositivo conectado: Mostrar una lista seleccionable con todos los
	 * dispositivos emparentados y con los dispositivos BT4 disponibles 5.
	 * Conectarse con el dispositivo seleccionado en la modalidad BT2 ó BT4 6.
	 * Si es exitoso registrar la modalidad y el nombre del dispositivo
	 * conectado
	 */
	public boolean isPopupON() {
		return popupOnOff;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// /float eventX = event.getX();
		// /float eventY = event.getY();

		Soporte.dump("onTouchEvent " + event.getAction());
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (Memoria.isProcesando()) {
				msg("UN MOMENTO POR FAVOR");
				return false;
			}
			return true;

		case MotionEvent.ACTION_MOVE:

		case MotionEvent.ACTION_UP:

			break;

		default:
			Soporte.dump("Ignored touch event: " + event.toString());
			return false;
		}

		return true;
	}

	public boolean validaStatus() {
		if (!Memoria.hayInternet()) {
			showMessage("NO HAY RED", 3);
			return false;
		}
		if (!Memoria.isCredenciales()) {
			if (CURRENT_STATUS == StatusGeneral.TERMINAL_INVALIDO)
				showMessage("TERMINAL INVALIDO", 3);
			else
				showMessage("HOST NO DISPONIBLE", 3);
			return false;
		}
		return true;
	}

	public void dumpScreenSize() {
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		_height = metrics.heightPixels;
		_width = metrics.widthPixels;
		Soporte.dump("Screen size: height=" + _height + " width=" + _width);
		metrics = null;
	}

	public String getScreenSize() {
		dumpScreenSize();
		return _width + "x" + _height;
	}

	public String getDensityScreen() {
		String valor = "none";
		double density = 0, dp = 0;
		try {
			density = getResources().getDisplayMetrics().density;
			dp = _width / density;
		} catch (Exception e) {
			Soporte.dump("getDensityScreen:" + e.getMessage());
			dp = 0;
		} finally {
			if (density > 0 && density < 1)
				valor = "ldpi";
			else if (density == 1)
				valor = "mdpi";
			else if (density == 1.5)
				valor = "hdpi";
			else if (density == 2)
				valor = "xhdpi";
			else if (density == 3)
				valor = "xxhdpi";
			else if (density == 4)
				valor = "xxxhdpi";
		}
		return valor + (int) dp;
	}

	public int getDpiScreen() {
		double density = 0, dp = 0;
		try {
			density = getResources().getDisplayMetrics().density;
			dp = _width / density;
		} catch (Exception e) {
			Soporte.dump("getDensityScreen:" + e.getMessage());
			dp = 0;
		} finally {
		}
		return (int) dp;
	}

	public boolean isLargeScreen() {
		dumpScreenSize();
		return _height >= 900 && _height <= 1300;
	}

	public boolean isMediumScreen() {
		dumpScreenSize();
		return _height >= 480
				&& (_height < 900 || (_height <= 960 && _width <= 540));
	}

	public int getWidthScreen() {
		return _width;
	}

	public boolean isSmallScreen() {
		dumpScreenSize();
		return getWidthScreen() <= 480 && getDpiScreen() <= 320;
	}

	public void setEditTextMaxLength(final EditText editText, int length) {
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(length);
		editText.setFilters(FilterArray);
	}

	public boolean isSemaforoON() {
		return semaforo != null;
	}

	public void semaforoVERDE() {
		if (isSemaforoON()) {
			Soporte.dump("Semaforo en Verde");
			semaforo.release();
		}
	}

	public void semaforoROJO() {
		semaforoROJO(0);
	}

	public void semaforoROJO(int timer) {
		Soporte.dump("Semaforo en Rojo");
		semaforo = new Semaphore(0);
		try {
			if (timer == 0)
				semaforo.acquire();
			else
				semaforo.tryAcquire(timer, TimeUnit.SECONDS);
		} catch (Exception e) {
			Soporte.dump("Error semaforo en rojo");
		}
	}

	public void semaforoOFF() {
		semaforo = null;
	}

	// / android.Manifest.permission.WRITE_EXTERNAL_STORAGE
	public boolean checkPermiso(String nombrePermiso) {
		PackageManager pm = this.getPackageManager();
		int hasPerm = pm.checkPermission(nombrePermiso, this.getPackageName());
		return (hasPerm == PackageManager.PERMISSION_GRANTED);
	}

	public void showProgressBar() {
        if (progressDialog != null) {
			progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }
	}

	public void showProgressBar(String mensaje) {
		if (progressDialog != null) {
            progressDialog.setCancelable(false);
			progressDialog.setMessage(mensaje);
			progressDialog.setIndeterminate(true);
            progressDialog.show();
        }
	}

	public void hideProgressBar() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}

	public void vibrar() {
		vibrar(200);
	}

	public void vibrar(int tiempo) {
		((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(tiempo);
	}

	public boolean isDone() {
		return _done;
	}

	public boolean isPause() {
		return _pause;
	}

	@SuppressLint("NewApi")
	public void cerrarAplicacion() {
		Soporte.dump("*** CERRANDO APLICACION ***");
		if (Memoria.getCallbackApp() != 0)
			setResult(Memoria.getCallbackResultado(),
					Memoria.getCallbackIntent());
		this.finish();
		// /this.finishAffinity(); /// Fuerza la finalizacion de cualquier otra
		// actividad
	}

	public void startApplication(String packageName) {
		startApplication(packageName, null);
	}

	public void startApplication(String packageName, String parametros) {
		try {
			Intent intent = new Intent("android.intent.action.MAIN");
			intent.addCategory("android.intent.category.LAUNCHER");

			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			List<ResolveInfo> resolveInfoList = getPackageManager()
					.queryIntentActivities(intent, 0);

			for (ResolveInfo info : resolveInfoList)
				if (info.activityInfo.packageName.equalsIgnoreCase(packageName)) {
					launchComponent(info.activityInfo.packageName,
							info.activityInfo.name, parametros);
					return;
				}

			// No match, so application is not installed
			showInMarket(packageName);
		} catch (Exception e) {
			showInMarket(packageName);
		}
	}

	private void launchComponent(String packageName, String name,
                                 String parametros) {
		Intent intent = new Intent("android.intent.action.MAIN");
		intent.addCategory("android.intent.category.LAUNCHER");
		intent.setAction(Intent.ACTION_SEND);
		intent.setComponent(new ComponentName(packageName, name));
		// / Asignacion de parametros
		if (!Soporte.isNullOrEmpty(parametros)) {
			String[] tokens = Texto.split(parametros, ";");
			String[] subtoken;
			for (String elemento : tokens) {
				subtoken = Texto.split(elemento, "=");
				if (subtoken.length >= 2)
					intent.putExtra(subtoken[0], subtoken[1]);
			}
			subtoken = null;
			tokens = null;
		}
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(intent);
	}

	private void showInMarket(String packageName) {
		Intent intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("market://details?id=" + packageName));
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public static enum Colomural {
		PANTONE, NAVY, FONDO, BLUE_PASTEL, GRAY34, LIGHTGRAY, ANOTHER;
		public String hex() {
			switch (this) {
			case PANTONE:
				return "#002080";
			case NAVY:
				return "#000033";
			case FONDO:
				return "#FAFAFA";
			case BLUE_PASTEL:
				return "#102540";
			case GRAY34:
				return "#222222";
			case LIGHTGRAY:
				return "#D3D3D3";
			case ANOTHER:
				return "#F5ECCE";
			default:
				return "#FFFFFF";
			}
		}

		public int color() {
			return Color.parseColor(this.hex());
		}
	}

	protected Drawable loadImagen(String filename) {
		InputStream stream;
		stream = this.getClass().getClassLoader()
				.getResourceAsStream("images/" + filename);
		Bitmap bitmap = BitmapFactory.decodeStream(stream);
		Drawable d = new BitmapDrawable(getResources(), bitmap);
		bitmap = null;
		stream = null;
		return d;
	}

	protected LinearLayout buildTitulo(Context contexto, String titulo,
                                       int font_size) {
		return buildTitulo(contexto, titulo, font_size,
				ViewGroup.LayoutParams.WRAP_CONTENT, -1, -1);
	}

	protected LinearLayout buildTitulo(Context contexto, String titulo,
                                       int font_size, int height_fila) {
		return buildTitulo(contexto, titulo, font_size, height_fila, -1, -1);
	}

	protected LinearLayout buildTitulo(Context contexto, String titulo,
                                       int font_size, int height_fila, int textcolor) {
		return buildTitulo(contexto, titulo, font_size, height_fila, textcolor,
				-1);
	}

	protected LinearLayout buildTitulo(Context contexto, String titulo,
                                       int font_size, int height_fila, int textcolor, int backcolor) {
		LinearLayout layout = buildLayout(contexto);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.getLayoutParams().height = height_fila;
		if (backcolor != -1)
			layout.setBackgroundColor(backcolor);
		layout.setGravity(Gravity.CENTER);
		TextView txttitulo = buildTextView(this, titulo, font_size,
				Typeface.BOLD);
		txttitulo.setGravity(Gravity.CENTER_VERTICAL);
		if (textcolor != -1)
			txttitulo.setTextColor(textcolor);
		layout.addView(txttitulo);
		return layout;
	}

	protected static LinearLayout buildLayout(Context contexto) {
		return buildLayout(contexto, ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
	}

	protected static LinearLayout buildLayoutH(Context contexto) {
		LinearLayout layout = buildLayout(contexto,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		return layout;
	}

	protected static LinearLayout buildLayout(Context contexto, int ancho,
                                              int alto) {
		LinearLayout layout = new LinearLayout(contexto);
		layout.setPadding(0, 0, 0, 0);
		layout.setLayoutParams(buildParams(ancho, alto));
		layout.setOrientation(LinearLayout.VERTICAL);
		return layout;
	}

	protected static LinearLayout buildLayoutH(Context contexto, int ancho,
                                               int alto) {
		LinearLayout layout = new LinearLayout(contexto);
		layout.setPadding(0, 0, 0, 0);
		layout.setLayoutParams(buildParams(ancho, alto));
		layout.setOrientation(LinearLayout.HORIZONTAL);
		return layout;
	}

	protected RelativeLayout buildMainLayoutRelative(Context contexto) {
		RelativeLayout layout = new RelativeLayout(contexto);
		layout.setBackgroundColor(Color.WHITE);
		layout.setPadding(0, 0, 0, 30);
		return layout;
	}

	protected LinearLayout buildLinea(Context contexto, int height) {
		LinearLayout layout = buildLayoutH(contexto,
				ViewGroup.LayoutParams.MATCH_PARENT, dipToPixels(height));
		layout.setBackgroundColor(Color.GRAY);
		return layout;
	}

	protected static LayoutParams buildParamsWrapContent() {
		return buildParams(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	protected static LayoutParams buildParams(int ancho, int alto) {
		return new ViewGroup.LayoutParams(ancho, alto);
	}

	protected static LayoutParams buildParamsMatchParent() {
		return new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
	}

	protected static LayoutParams buildParamsMatchWrap() {
		return new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	protected static LayoutParams buildMarginParamsMatchWrap() {
		return new ViewGroup.MarginLayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	protected static LinearLayout.LayoutParams buildParamsFitToParent() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		params.weight = 1.0f;
		params.width = 0;
		return params;
	}

	protected static LinearLayout.LayoutParams buildParamsFitToParent(
			float weight) {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		params.weight = weight;
		params.width = 0;
		return params;
	}

	protected static RelativeLayout.LayoutParams buildRelativeLayoutParams() {
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		return lp;
	}

	protected static RelativeLayout.LayoutParams buildRelativeLayoutParamsMatch() {
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		return lp;
	}

	protected static RelativeLayout.LayoutParams buildRelativeLayoutMargen(
			int left, int top, int right, int bottom) {
		RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		llp.setMargins(left, top, right, bottom);
		return llp;
	}

	protected static LayoutParams buildLayoutMargen(int left, int top,
                                                    int right, int bottom) {
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		llp.setMargins(left, top, right, bottom);
		return llp;
	}

	protected static TextView buildTextView(Context contexto, String texto,
                                            int size) {
		return buildTextView(contexto, texto, size, Typeface.NORMAL,
				Color.WHITE);
	}

	protected static TextView buildTextView(Context contexto, String texto,
                                            int size, int estilo) {
		return buildTextView(contexto, texto, size, estilo, Color.WHITE);
	}

	protected static TextView buildTextView(Context contexto, String texto,
                                            int size, int estilo, int color) {
		return buildTextView(contexto, texto, size, estilo, color, null);
	}

	protected static TextView buildTextView(Context contexto, String texto,
                                            int size, int estilo, int color, OnClickListener listener) {
		TextView obj = new TextView(contexto);
		obj.setText(texto);
		obj.setTextColor(color);
		obj.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
		obj.setTypeface(null, estilo);
		obj.setLayoutParams(buildParamsWrapContent());
		if (listener != null) {
			obj.setClickable(true);
			obj.setOnClickListener(listener);
		}
		return obj;
	}

	protected static Button buildButton(Context contexto, String texto, int size) {
		Button obj = new Button(contexto);
		obj.setText(texto);
		obj.setTextColor(Color.WHITE);
		obj.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
		// obj.setTypeface(null, estilo);
		return obj;
	}

	protected ImageView buildImageView(Context contexto, Drawable drawable) {
		return buildImageView(contexto, drawable, -1, -1);
	}

	protected ImageView buildImageView(Context contexto, Drawable drawable,
                                       int ancho, int alto) {
		ImageView obj = new ImageView(contexto);
		obj.setLayoutParams(buildParamsWrapContent());
		if (alto != -1)
			obj.getLayoutParams().height = dipToPixels(alto);
		if (ancho != -1)
			obj.getLayoutParams().width = dipToPixels(ancho);
		obj.setImageDrawable(drawable);
		return obj;
	}

	public static LinearLayout buildLayoutProgressBar(Context contexto,
                                                      String texto, int textSize) {
		LinearLayout layout = buildLayout(contexto);
		layout.setLayoutParams(buildParamsWrapContent());
		layout.setBackgroundColor(Colomural.BLUE_PASTEL.color());
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.setPadding(20, 20, 20, 20);
		layout.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

		ProgressBar progressBar = new ProgressBar(contexto, null,
				android.R.attr.progressBarStyleLarge);
		progressBar.setLayoutParams(buildParamsWrapContent());

		TextView textView = buildTextView(contexto, texto, textSize);
		textView.setLayoutParams(buildLayoutMargen(5, 0, 0, 0));
		//textView.setId(1000);

		layout.addView(progressBar);
		layout.addView(textView);

		return layout;
	}

	public void setMargin(View view, int left, int top, int right, int bottom) {

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(dipToPixels(left), dipToPixels(top),
				dipToPixels(right), dipToPixels(bottom));

		view.setLayoutParams(params);
	}

	public void setCustomView(View v, int radius, int backgroundColor, int borderColor)
	{
	    GradientDrawable shape = new GradientDrawable();
	    shape.setShape(GradientDrawable.RECTANGLE);
	    shape.setCornerRadius(dipToPixels(radius));///(new float[] { 8, 8, 8, 8, 0, 0, 0, 0 });
	    shape.setColor(backgroundColor);
	    shape.setStroke(3, borderColor);
	    v.setBackground(shape);
	}	
	
	public int dipToPixels(int dps) {
		final float scale = getResources().getDisplayMetrics().density;
		int pixels = (int) (dps * scale + 0.5f);
		return pixels;
	}

}
