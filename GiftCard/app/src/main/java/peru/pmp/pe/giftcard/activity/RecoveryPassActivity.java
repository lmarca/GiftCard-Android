package peru.pmp.pe.giftcard.activity;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.TramaE;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.networking.GoogleApi;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.ReportUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 9/11/17.
 */

public class RecoveryPassActivity extends MyActivity {

    TramaE tramaE;
    Timer timer;
    int currentPage = 0;

    @Bind(R.id.logo)
    ImageView logo;

    @Bind(R.id.edit_email)
    EditText edit_email;

    @Bind(R.id.edit_title)
    TextView edit_title;

    @Bind(R.id.edit_ingrese)
    TextView edit_ingrese;

    @Bind(R.id.btnRecovery)
    Button btnRecovery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery_pass);
        DesignUtil.customStatusBar(getWindow(), this);

        ButterKnife.bind(this);
        configurationStarted();
    }

    private void configurationStarted() {

        timer = new Timer();
        feedImages();

        Typeface face=Typeface.createFromAsset(getAssets(),"fonts/poppins-regular.ttf");
        edit_ingrese.setTypeface(face);
        btnRecovery.setTypeface(face);
        edit_email.setTypeface(face);
        edit_title.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/poppins-medium.ttf"));
        edit_title.setPaintFlags(edit_title.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }

    public void feedImages() {

        final Drawable[] array = new Drawable[4];
        array[0] = getResources().getDrawable(R.drawable.img1);
        array[1] = getResources().getDrawable(R.drawable.img2);
        array[2] = getResources().getDrawable(R.drawable.img3);
        array[3] = getResources().getDrawable(R.drawable.img4);

        //Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                try {
                    runOnUiThread(new  Runnable() {

                        public void run() {

                            if (currentPage < array.length - 1)
                                currentPage = currentPage  + 1;
                            else
                                currentPage = 0;

                            logo.setImageDrawable(array[currentPage]);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 0, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer = null;
    }

    @OnClick(R.id.btnRecovery)
    public void getRecovery() {
        if (!Memoria.hayInternet()) {
            toast(ReportUtil.MENSAJE_CONEXION_INTERNET);
        }else {
            if (Soporte.getValidateMail(edit_email)) {
                Memoria.setEmailUser(edit_email.getText().toString());
                apiJson();
            }else {
                toast(ReportUtil.MENSAJE_CORREO);
            }
        }
    }

    private void apiJson() {

        showProgressBar("Procesando");

        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetOlvidoPassword(tramaE.buildTramaOlvidoPassword()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


                tramaE = null;
                hideProgressBar();
                edit_email.setText("");

                if (response.isSuccessful()) {

                    try {

                        String user = response.body().get("Olvido_Password").toString();

                        JSONObject obj = new JSONObject(user);
                        toast(obj.getString("response_message"));

                    } catch (Throwable tx) {}
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();
            }
        });

    }
}
