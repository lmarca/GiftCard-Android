package peru.pmp.pe.giftcard.entidades;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import peru.pmp.pe.giftcard.baseclass.Entidad;
import peru.pmp.pe.giftcard.libreria.Texto;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.utilitarios.Soporte;


@SuppressLint("DefaultLocale")
public class OperacionE extends Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4880705059375916602L;
	String nombres;
    String password;
    String tipoDoc;
    String numberDoc;
    String codeActivacion;
    String numberTarj;
    String dateTarj;
    String aliasTarj;


    public OperacionE(){
		super();
        nombres = "";
        password = "";
        tipoDoc = "";
        numberDoc = "";
        codeActivacion = "";
        numberTarj = "";
        dateTarj = "";
        aliasTarj = "";

    }

    public String getNumberTarj() {
        return numberTarj;
    }

    public void setNumberTarj(String numberTarj) {
        this.numberTarj = numberTarj;
    }

    public String getDateTarj() {
        return dateTarj;
    }

    public void setDateTarj(String dateTarj) {
        this.dateTarj = dateTarj;
    }

    public String getAliasTarj() {
        return aliasTarj;
    }

    public void setAliasTarj(String aliasTarj) {
        this.aliasTarj = aliasTarj;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNumberDoc() {
        return numberDoc;
    }

    public void setNumberDoc(String numberDoc) {
        this.numberDoc = numberDoc;
    }

    public String getCodeActivacion() {
        return codeActivacion;
    }

    public void setCodeActivacion(String codeActivacion) {
        this.codeActivacion = codeActivacion;
    }

    public void setUsuario(JSONObject objUser) {

        User user = new User();

        try {

            user.setApi_key(objUser.getString("api_key"));
            user.setApi_login(objUser.getString("api_login"));
            user.setAfiliacion_mensaje(objUser.getString("afiliacion_mensaje"));
            user.setBuyer_doc_number(objUser.getString("buyer_doc_number"));
            user.setBuyer_doc_type(objUser.getString("buyer_doc_type"));
            user.setBuyer_email(objUser.getString("buyer_email"));
            user.setBuyer_first_name(objUser.getString("buyer_first_name"));
            user.setBuyer_last_name(objUser.getString("buyer_last_name"));
            user.setBuyer_merchant_id(objUser.getString("buyer_merchant_id"));
            user.setBuyer_mobile_id(objUser.getString("buyer_mobile_id"));
            user.setBuyer_mobile_token_id(objUser.getString("buyer_mobile_token_id"));
            user.setBuyer_token_id(objUser.getString("buyer_token_id"));
        //user.setFlag_mobile(Texto.toInt(objUser.get("flag_mobile").toString()));
            user.setMerchant_code(objUser.getString("merchant_code"));
            user.setResponse_code(objUser.getString("response_code"));
            user.setResponse_message(objUser.getString("response_message"));
            user.setTarjeta_promocion(objUser.getString("tarjeta_promocion"));
            user.setTransaction_currency(objUser.getString("transaction_currency"));

        } catch (Throwable tx) {}

        Memoria.setUser(user);
    }
}
