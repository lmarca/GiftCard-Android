package peru.pmp.pe.giftcard.activity;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.TramaE;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.networking.GoogleApi;
import peru.pmp.pe.giftcard.utilitarios.ConnectionChecker;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.ReportUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by apple on 7/11/17.
 */

public class LoginActivity extends MyActivity {

    TramaE tramaE;
    Timer timer;
    int currentPage = 0;
    ConnectionChecker connectionChecker = new ConnectionChecker();

    @Bind(R.id.logo)
    ImageView logo;

    @Bind(R.id.btnRegistrar)
    Button btnRegistrar;

    @Bind(R.id.btnOlvido)
    Button btnOlvido;

    @Bind(R.id.edit_cuenta)
    TextView edit_cuenta;

    @Bind(R.id.edit_email)
    EditText edit_email;

    @Bind(R.id.edit_pwd)
    EditText edit_pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        DesignUtil.customStatusBar(getWindow(), this);

        ButterKnife.bind(this);
        configurationStarted();

        //logo.getLayoutParams().width = DeviceScreen.getScreenWidth(Memoria.getContext());
        //logo.getLayoutParams().height = (int) (DeviceScreen.getScreenWidth(Memoria.getContext()) * 0.471);
    }

    private void configurationStarted() {

        timer = new Timer();
        feedImages();

        Typeface face=Typeface.createFromAsset(getAssets(),"fonts/poppins-regular.ttf");
        edit_cuenta.setTypeface(face);
        btnOlvido.setTypeface(face);
        btnRegistrar.setTypeface(face);
        btnRegistrar.setPaintFlags(btnRegistrar.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btnOlvido.setPaintFlags(btnOlvido.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    public void feedImages() {

        final Drawable[] array = new Drawable[4];
        array[0] = getResources().getDrawable(R.drawable.img1);
        array[1] = getResources().getDrawable(R.drawable.img2);
        array[2] = getResources().getDrawable(R.drawable.img3);
        array[3] = getResources().getDrawable(R.drawable.img4);

        //Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                try {
                    runOnUiThread(new  Runnable() {

                        public void run() {

                            if (currentPage < array.length - 1)
                                currentPage = currentPage  + 1;
                            else
                                currentPage = 0;

                            logo.setImageDrawable(array[currentPage]);
                            //Animation animation = AnimationUtils.loadAnimation(Memoria.getContext(), R.anim.fadein_image);
                            //logo.setAnimation(animation);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 0, 5000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer = null;
    }

    @OnClick(R.id.btnRegistrar)
    public void getRegistrar() {
        show(RegisterActivity.class);
    }

    @OnClick(R.id.btnOlvido)
    public void getOlvido() {
        show(RecoveryPassActivity.class);
    }

    @OnClick(R.id.btnLogin)
    public void getLogin(View view){

        //Verificamos la conexión a internet del equipo
        if(connectionChecker.checkConnection(this)){

            if (getFieldsEmpty(edit_email, edit_pwd)) {
                toast(ReportUtil.MENSAJE_CAMPOS_VACIOS);
            } else {
                if (Soporte.getValidateMail(edit_email)) {
                    Memoria.setEmailUser(edit_email.getText().toString());
                    Memoria.getOperacion().setPassword(edit_pwd.getText().toString());
                    apiJson();
                } else {
                    toast(ReportUtil.MENSAJE_CORREO);
                }
            }

        }else {
            connectionChecker.showNoConnectionMessage(view,getString(R.string.no_connection_message));
        }
    }

    private boolean getFieldsEmpty(EditText correo, EditText documento) {
        if (Soporte.isNullOrEmpty(correo.getText().toString()) || Soporte.isNullOrEmpty(documento.getText().toString())) {
            return true;
        }

        return false;
    }

    private void apiJson() {


        showProgressBar("Autenticando Usuario");

        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

        tramaE = new TramaE();

        final Gson gson = new Gson();
        Log.d("AUTENTICA_USUARIO_REQ", gson.toJson(tramaE.buildTramaAutenticaUsuario()));

        restApi.wsGetAutenticaUsuario(tramaE.buildTramaAutenticaUsuario()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();

                if (response.isSuccessful()) {
                    reset();

                    String user = response.body().get("Autentica_Usuario").toString();

                    try {

                        JSONObject obj = new JSONObject(user);

                        Memoria.getOperacion().setUsuario(obj);

                        if (Memoria.getUser().getResponse_code().equals("00"))
                            show(HomeCardActivity.class);
                        else
                            toast(Memoria.getUser().getResponse_message());

                    } catch (Throwable tx) {
                        Soporte.dump("My App", "Could not parse malformed JSON: \"" + user + "\"");
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();
            }
        });

    }

    private void reset() {
        edit_email.setText("");
        edit_pwd.setText("");
    }
}
