package peru.pmp.pe.giftcard.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.TramaE;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.networking.GoogleApi;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.ReportUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 9/11/17.
 */

public class ConfirmCodeActivity extends MyActivity {

    TramaE tramaE;
    Timer timer;
    int currentPage = 0;

    @Bind(R.id.logo)
    ImageView logo;

    @Bind(R.id.txt_pin_entry)
    PinEntryEditText pinEntry;

    @Bind(R.id.edit_ingrese)
    TextView edit_ingrese;

    @Bind(R.id.edit_title)
    TextView edit_title;

    @Bind(R.id.btnVerificar)
    Button btnVerificar;

    @Bind(R.id.btnBack)
    Button btnBack;

    @Bind(R.id.slide_down)
    RelativeLayout layout;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_code);
        DesignUtil.customStatusBar(getWindow(), this);

        ButterKnife.bind(this);
        configurationStarted();

        layout.setVisibility(View.GONE);

        edit_ingrese.setText("Ingresa el código que acabamos de\n" +
                "enviarte a: " + Memoria.getEmailUser());

        pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
        pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {

                Memoria.getOperacion().setCodeActivacion(str.toString());

                if (str.toString().length() == 5){
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinEntry.getWindowToken(), 0);
                }

            }
        });
    }

    private void configurationStarted() {

        timer = new Timer();
        feedImages();

        Typeface face=Typeface.createFromAsset(getAssets(),"fonts/poppins-regular.ttf");
        //edit_ingrese.setTypeface(face);
        btnBack.setTypeface(face);
        btnVerificar.setTypeface(face);
        //edit_title.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/poppins-medium.ttf"));

    }

    public void feedImages() {

        final Drawable[] array = new Drawable[4];
        array[0] = getResources().getDrawable(R.drawable.img1);
        array[1] = getResources().getDrawable(R.drawable.img2);
        array[2] = getResources().getDrawable(R.drawable.img3);
        array[3] = getResources().getDrawable(R.drawable.img4);

        //Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                try {
                    runOnUiThread(new  Runnable() {

                        public void run() {

                            if (currentPage < array.length - 1)
                                currentPage = currentPage  + 1;
                            else
                                currentPage = 0;

                            logo.setImageDrawable(array[currentPage]);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 0, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer = null;
    }

    @OnClick(R.id.btnBack)
    public void getBack() {
        backTo(RegisterActivity.class);
    }

    @OnClick(R.id.btnSendCode)
    public void sendCode() {

        if (!Memoria.hayInternet()) {
            toast(ReportUtil.MENSAJE_CONEXION_INTERNET);
        }else {
            //pendiente enviar codigo
            getTramaSendCode();
        }
    }

    @OnClick(R.id.btnVerificar)
    public void getVerificar() {

        if (!Memoria.hayInternet()) {
            toast(ReportUtil.MENSAJE_CONEXION_INTERNET);
        }else {
            if (Memoria.getOperacion().getCodeActivacion().length() == 0) {
                toast(ReportUtil.MENSAJE_CAMPOS_VACIOS);
            } else {
                getTramaConfirmCode();
            }
        }
    }

    private boolean getFieldsEmpty(EditText pinEntry) {
        if (Soporte.isNullOrEmpty(pinEntry.getText().toString())) {
            return true;
        }

        return false;
    }

    private void showMessageError() {

        layout.setVisibility(View.VISIBLE);
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        layout.startAnimation(slide_up);

        new CountDownTimer(6000, 1000) {

            public void onTick(long millisUntilFinished) {
                //toast("Seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
                layout.startAnimation(slide_down);
                layout.setVisibility(View.GONE);
            }

        }.start();
    }

    private void getTramaConfirmCode() {

        showProgressBar();

        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetAfiliacionAlta(tramaE.buildTramaAfiliacionAlta()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();

                if (response.isSuccessful()) {

                    reset();

                    String user = response.body().get("Afiliacion_Alta").toString();

                    try {

                        JSONObject obj = new JSONObject(user);

                        Memoria.getOperacion().setUsuario(obj);

                        if (Memoria.getUser().getResponse_code().equals("00"))
                            show(SucessActivity.class);
                        else
                            showMessageError();

                    } catch (Throwable tx) {}

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();
            }
        });

    }

    private void getTramaSendCode() {

        showProgressBar();

        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetAfiliacionCodeActivacion(tramaE.buildTramaAfiliacionCodigoActivacion()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();

                if (response.isSuccessful()) {

                    String user = response.body().get("Afiliacion_Codigo_Activacion").toString();

                    try {

                        JSONObject obj = new JSONObject(user);

                        Soporte.dump(obj.toString());
                        toast(obj.getString("response_message"));

                    } catch (Throwable tx) {}

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();
            }
        });

    }

    private void reset() {pinEntry.setText("");}

}
