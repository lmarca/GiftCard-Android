package peru.pmp.pe.giftcard.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;

import butterknife.ButterKnife;
import butterknife.OnClick;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;

/**
 * Created by apple on 9/11/17.
 */

public class SucessActivity extends MyActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucess);
        DesignUtil.customStatusBar(getWindow(), this);

        ButterKnife.bind(this);
        /*** INICIALIZACION DE VARIABLES ***/

    }

    @OnClick(R.id.btnGiftCard)
    public void showSucess() {
        show(GiftActivity.class);
    }

}
