package peru.pmp.pe.giftcard.libreria.security;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/******** TRUST MANAGER personalizado para que confie en TODOS ********/

public class MyTrustManager implements X509TrustManager {

	private final X509TrustManager tm;
	protected X509Certificate[] chain;
	
	public MyTrustManager(X509TrustManager tm) {
		this.tm = tm;
	}
	
	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		System.out.println("MyTrustManager.checkClientTrusted - event -");
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		System.out.println("MyTrustManager.checkServerTrusted - event -");
		this.chain = chain;
		tm.checkServerTrusted(chain, authType);
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		System.out.println("MyTrustManager.getAcceptedIssuers - event -");
		return null;
	}

}
