package peru.pmp.pe.giftcard.networking;

import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import peru.pmp.pe.giftcard.baseclass.Constantes;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Admin on 1/02/18.
 */

public interface GestorTarjetas {

    interface RestApi {

        final OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        public static final Retrofit RETROFIT = new Retrofit.Builder()
                .baseUrl(Constantes.URL_GESTION_TARJETAS)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        @Headers("Content-Type: application/json")
        @POST("Consulta_Saldos")
        Call<JsonObject> wsGetConsultaSaldos(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Consulta_Movimientos")
        Call<JsonObject> wsGetConsultaMovimientos(@Body String body);
    }
}
