package peru.pmp.pe.giftcard.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import butterknife.ButterKnife;
import butterknife.Bind;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;

public class SplashActivity extends MyActivity {

    @Bind(R.id.image_logo)
    ImageView image_logo;

    private final int SPLASH_DISPLAY_LENGHT = 1 * 6000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /**
         * INICIALIZACION DE VARIABLES
         */

        Memoria.init();

        DesignUtil.customStatusBarFullScreen(getWindow(), this);
        ButterKnife.bind(this);
        realConfiguration();

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fadein_image);
        image_logo.setAnimation(animation);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent;

                if (Memoria.getUser().getBuyer_token_id().length() != 0)
                    intent = new Intent(SplashActivity.this, HomeCardActivity.class);
                else
                    intent = new Intent(SplashActivity.this, MainActivity.class);


                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        }, SPLASH_DISPLAY_LENGHT);
    }

    public void realConfiguration() {

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
