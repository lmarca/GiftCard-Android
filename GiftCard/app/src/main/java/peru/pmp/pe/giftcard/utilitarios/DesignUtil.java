package peru.pmp.pe.giftcard.utilitarios;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Spinner;

import peru.pmp.pe.giftcard.R;


/**
 * Util que se encarga de algunos diseños del aplicativo de SMV
 *
 * @author Leandro castillo
 */

public class DesignUtil {

    /**
     * Metodo que permite que la barra se ponca tranparente
     */

    public static void customStatusBarFullScreen(Window window, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    /**
     * Metodo que permite que la barra se ponca tranparente
     */

    public static void customStatusBar(Window window, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * Metodo que permite que la barra tenga diferentes colores
     */

    public static void customStatusBarColor(Window window, Context context, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(context.getResources().getColor(color));
            //window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    /**
     * Metodo que permite al toolbar sea transparente
     */

    public static void toolbarTransparent(Toolbar toolbar) {
        toolbar.setBackgroundColor(Color.TRANSPARENT);
    }

    /**
     * Metodo que permite al toolbar tenga diferentes colores
     */

    public static void toolbarColor(Toolbar toolbar, Context context, int color) {
        toolbar.setBackgroundColor(context.getResources().getColor(color));
    }

    /**
     * Metodo que desactiva el scroll del navegation menu
     */

    public static void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    /**
     * Metotdo que cambia el color de la flecha del Spinner a otro color
     */

    public static void spinnerArrow(Spinner spinner, Context context) {
        Drawable spinnerDrawable = spinner.getBackground().getConstantState().newDrawable();

        spinnerDrawable.setColorFilter(context.getResources()
                .getColor(R.color.blanco), PorterDuff.Mode.SRC_ATOP);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner.setBackground(spinnerDrawable);
        } else {
            spinner.setBackgroundDrawable(spinnerDrawable);
        }
    }

    /**
     * Metodo que se encarga de poner los item en posiciones correctas atraves de pixeles
     */

    public static int getPixel(Context context, int pixResourceId) {
        int pix = (int) context.getResources().getDimension(pixResourceId);
        return pix;
    }
}
