package peru.pmp.pe.giftcard.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.adapter.TarjsAdapter;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.MovementsItem;
import peru.pmp.pe.giftcard.entidades.Tarjeta;
import peru.pmp.pe.giftcard.entidades.TarjetaInfoE;
import peru.pmp.pe.giftcard.entidades.TramaE;
import peru.pmp.pe.giftcard.fragment.CardFragment;
import peru.pmp.pe.giftcard.libreria.Texto;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.networking.GestorTarjetas;
import peru.pmp.pe.giftcard.networking.GoogleApi;
import peru.pmp.pe.giftcard.realm.RealmController;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.ReportUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by apple on 13/11/17.
 */

public class HomeCardActivity extends MyActivity implements AdapterCallback {

    TramaE tramaE;
    private Realm realm;
    private String card;
    private String fecha_vcto;
    @Bind(R.id.recycler_lista_tarj)
    RecyclerView recycler_lista_tarj;

    private LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(Memoria.getContext());
    private TarjsAdapter tarjsAdapter;
    private CardFragment fragment = new CardFragment();
    private FragmentManager manager = getSupportFragmentManager();
    private ArrayList<TarjetaInfoE> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_card);

        DesignUtil.customStatusBarColor(getWindow(), this, R.color.colorPrimaryDark);
        ButterKnife.bind(this);

        Memoria.setLlaveEnc(Memoria.getUser().getApi_key());

        //get realm instance
        this.realm = RealmController.with(this).getRealm();
        // refresh the realm instance
        RealmController.with(this).refresh();

        linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_lista_tarj.setLayoutManager(linearLayoutManager1);
        recycler_lista_tarj.setItemAnimator(new DefaultItemAnimator());
        tarjsAdapter = new TarjsAdapter(list, this, this);
        recycler_lista_tarj.setAdapter(tarjsAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                show(KindCardActivity.class);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getTramaListadoCard();
    }

    private void refreshStatusTarj() {

        if (Memoria.getHashMisTarjetas().size() == 0) {
            fragment = new CardFragment();
            manager.beginTransaction().add(R.id.main_content, fragment).commit();
        }else {
            manager.beginTransaction().remove(fragment).commit();
        }
    }

    @OnClick(R.id.btnLogout)
    public void logout() { finishSession();}

    private void getTramaListadoCard() {

        showProgressBar("Procesando");

        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetListadoCard(tramaE.buildTramaListadoCard()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();
                TarjetaInfoE tarjetaUsuario;
                JsonObject tarjetaMap;

                if (response.isSuccessful()) {

                    list.clear();
                    Memoria.getHashMisTarjetas().clear();

                    if(response.body().get("Listado_Card").getAsJsonObject().get("buyer_list_card").getAsJsonObject().get("card").isJsonArray()){

                        JsonArray tarjetaList = response.body().get("Listado_Card").getAsJsonObject().get("buyer_list_card").getAsJsonObject().getAsJsonArray("card");

                        for (int i = 0; i < tarjetaList.size(); i++){

                            tarjetaMap = (JsonObject) tarjetaList.get(i);

                            tarjetaUsuario = new TarjetaInfoE();

                            if(tarjetaMap.get("card_token_id").getAsString() != null &&
                                    !tarjetaMap.get("card_token_id").getAsString().equals("")){
                                tarjetaUsuario.setCard_token_id(tarjetaMap.get("card_token_id").getAsString());
                                tarjetaUsuario.setCard_alias(tarjetaMap.get("card_alias").getAsString());
                                tarjetaUsuario.setCard_brand(tarjetaMap.get("card_brand").getAsString());
                                tarjetaUsuario.setCard_mask(tarjetaMap.get("card_mask").getAsString());
                                tarjetaUsuario.setCard_mail(tarjetaMap.get("card_mail").getAsString());
                                tarjetaUsuario.setCard_full_name(tarjetaMap.get("card_full_name").getAsString());
                                tarjetaUsuario.setCard_fecha_vcto(tarjetaMap.get("card_fecha_vcto").getAsString());
                                if(!tarjetaMap.get("card_favorite").equals("")){
                                    tarjetaUsuario.setCard_favorite(Texto.toInt(tarjetaMap.get("card_favorite").getAsString()));
                                }else{
                                    tarjetaUsuario.setCard_favorite(0);
                                }
                                if(!tarjetaMap.get("card_validation").equals("")){
                                    tarjetaUsuario.setCard_validation(Texto.toInt(tarjetaMap.get("card_validation").getAsString()));
                                }else {
                                    tarjetaUsuario.setCard_validation(0);
                                }
                                if(!"".equals(tarjetaMap.get("card_color"))){
                                    tarjetaUsuario.setCard_color(Texto.toInt(tarjetaMap.get("card_color").getAsString()));
                                }else{
                                    tarjetaUsuario.setCard_color(1);
                                }

                                Memoria.getHashMisTarjetas().put(tarjetaUsuario.getCard_token_id(), tarjetaUsuario);
                            }
                        }
                    }else  {

                        tarjetaMap = response.body().get("Listado_Card").getAsJsonObject().get("buyer_list_card").getAsJsonObject().getAsJsonObject("card");

                        tarjetaUsuario = new TarjetaInfoE();

                        if(tarjetaMap.get("card_token_id").getAsString() != null &&
                                !tarjetaMap.get("card_token_id").getAsString().equals("")){
                            tarjetaUsuario.setCard_token_id(tarjetaMap.get("card_token_id").getAsString());
                            tarjetaUsuario.setCard_alias(tarjetaMap.get("card_alias").getAsString());
                            tarjetaUsuario.setCard_brand(tarjetaMap.get("card_brand").getAsString());
                            tarjetaUsuario.setCard_mask(tarjetaMap.get("card_mask").getAsString());
                            tarjetaUsuario.setCard_mail(tarjetaMap.get("card_mail").getAsString());
                            tarjetaUsuario.setCard_full_name(tarjetaMap.get("card_full_name").getAsString());
                            tarjetaUsuario.setCard_fecha_vcto(tarjetaMap.get("card_fecha_vcto").getAsString());
                            if(!tarjetaMap.get("card_favorite").equals("")){
                                tarjetaUsuario.setCard_favorite(Texto.toInt(tarjetaMap.get("card_favorite").getAsString()));
                            }else{
                                tarjetaUsuario.setCard_favorite(0);
                            }
                            if(!tarjetaMap.get("card_validation").equals("")){
                                tarjetaUsuario.setCard_validation(Texto.toInt(tarjetaMap.get("card_validation").getAsString()));
                            }else {
                                tarjetaUsuario.setCard_validation(0);
                            }
                            if(!"".equals(tarjetaMap.get("card_color"))){
                                tarjetaUsuario.setCard_color(Texto.toInt(tarjetaMap.get("card_color").getAsString()));
                            }else{
                                tarjetaUsuario.setCard_color(1);
                            }

                            Memoria.getHashMisTarjetas().put(tarjetaUsuario.getCard_token_id(), tarjetaUsuario);
                        }
                    }

                    refreshStatusTarj();
                    for (TarjetaInfoE o : Memoria.getHashMisTarjetas().values())
                        list.add(o);

                    tarjsAdapter.notifyItemRangeInserted(tarjsAdapter.getItemCount(), list.size());
                }else
                    refreshStatusTarj();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void deleteTarj(final TarjetaInfoE tarj, final int position) {

        if (!Memoria.hayInternet()) {
            toast(ReportUtil.MENSAJE_CONEXION_INTERNET);
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder .setTitle("Gift Card")
                .setMessage("¿Estás seguro de eliminar la tarjeta " + tarj.getCard_alias() + "?")
                .setCancelable(false)
                .setPositiveButton("SI", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();

                        showProgressBar("Procesando");

                        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

                        tramaE = new TramaE();

                        restApi.wsGetEliminaCard(tramaE.buildTramaEliminaCard(tarj)).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                tramaE = null;
                                hideProgressBar();

                                if (response.isSuccessful()) {

                                    try {

                                        String user = response.body().get("Elimina_Token_Card").toString();

                                        Memoria.getHashMisTarjetas().remove(tarj.getCard_token_id());
                                        tarjsAdapter.removeAt(position);
                                        refreshStatusTarj();

                                    } catch (Throwable tx) {}

                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                tramaE = null;
                                toast(ReportUtil.MENSAJE_ERROR_RED);
                                hideProgressBar();
                            }
                        });

                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void selectTarj(final TarjetaInfoE tarj) {

        if (!Memoria.hayInternet()) {
            toast(ReportUtil.MENSAJE_CONEXION_INTERNET);
            return;
        }


        Tarjeta tarjeta =  RealmController.with(this).getBook(tarj.getCard_mask(), tarj.getCard_alias());

        card = Memoria.decrypTDES(tarjeta.getCard_mask_encrypted());
        fecha_vcto = Memoria.decrypTDES(tarjeta.getCard_fecha_vcto_encrypted());

        showProgressBar("Consultando saldo de la tarjeta");

        GestorTarjetas.RestApi restApi = GestorTarjetas.RestApi.RETROFIT.create(GestorTarjetas.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetConsultaSaldos(tramaE.buildTramaConsultas("Consulta_Saldos", card, fecha_vcto)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();

                if (response.isSuccessful()) {

                    try {

                        String user = response.body().get("Consulta_Saldos").toString();
                        JSONObject obj = new JSONObject(user);
                        if (!obj.getString("SaldoDispActual").isEmpty()){
                            tarj.setSaldoDispActual(obj.getString("SaldoDispActual"));
                        }else {
                            tarj.setSaldoDispActual("0.00");
                        }

                        buildConsultaMovimientos(tarj);

                    } catch (Throwable tx) {}

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();
            }
        });

/*
        Intent intent = new Intent(activity, ListMovementsActivity.class);
        intent.putExtra("card_mask", lista.get(position).getCard_mask());
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
*/
    }

    public void buildConsultaMovimientos(final TarjetaInfoE tarj) {

        showProgressBar("Consultando movimientos de la tarjeta");
        Tarjeta tarjeta =  RealmController.with(this).getBook(tarj.getCard_mask(), tarj.getCard_alias());

        GestorTarjetas.RestApi restApi = GestorTarjetas.RestApi.RETROFIT.create(GestorTarjetas.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetConsultaMovimientos(tramaE.buildTramaConsultas("Consulta_Movimientos", card, fecha_vcto)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();

                if (response.isSuccessful()) {

                    try {

                        String user = response.body().get("Consulta_Movimientos").toString();

                        JSONObject obj = new JSONObject(user);
                        Memoria.setMovements(obj);

                        if (Memoria.getHashMovements().size() > 0) {
                            MovementsItem item = Memoria.getHashMovements().get(0);
                            tarj.setLastMontoTxn(item.getMovMontoTxn());
                        }else {
                            tarj.setLastMontoTxn("S/ 0.00");
                        }

                        showScreenMovements(tarj);

                    } catch (Throwable tx) {
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();
            }
        });
    }

    public void showScreenMovements(TarjetaInfoE tarj) {
        Intent intent = new Intent(this, ListMovementsActivity.class);
        intent.putExtra("card_mask", tarj.getCard_mask());
        intent.putExtra("SaldoDispActual", tarj.getSaldoDispActual());
        intent.putExtra("LastMontoTxn", tarj.getLastMontoTxn());
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}
