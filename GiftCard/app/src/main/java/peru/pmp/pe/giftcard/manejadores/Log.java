package peru.pmp.pe.giftcard.manejadores;


import peru.pmp.pe.giftcard.baseclass.*;
import peru.pmp.pe.giftcard.libreria.*;
import peru.pmp.pe.giftcard.utilitarios.*;
import peru.pmp.pe.giftcard.entidades.*;

import java.io.File;
import java.io.PrintWriter;


/**
 * Clase que administra el registro del log en el sistema movil
 * @author Billy Vera Castellanos
 * @version 1.0
 */
public class Log implements Constantes {

	private static PrintWriter outSistema = null;

	private static boolean bPrintTrace = false;

	public static void abrirArchivosLog() {
		String fullpath = Memoria.getAppFolder() +Archivo.SEPARADOR+ APP_FOLDER_LOGS + Archivo.SEPARADOR;
		String sFile;
		if (bPrintTrace)
			Soporte.trace("Abriendo archivos de log");
		try {
			if (outSistema == null) {
				sFile = fullpath + ARCH_LOG_SISTEMA;
				Archivo.crearArchivo(sFile);
				outSistema = new PrintWriter(
						Archivo.streamArchivo(sFile, true), true);
			}
		} catch (Exception io) {
			Soporte.dump("Apertura de Archivos de Log", io);
		}
	}

	public static void cerrarArchivosLog() {
		if (bPrintTrace)
			Soporte.trace("Cerrando archivos de log");
		if (outSistema != null) {
			outSistema.close();
			outSistema = null;
		}
	}

	// Los mensajes de Log no escriben directamente en los archivos
	// Adicionan los mensajes al vector 'vectorEventos'.-
	public static void mensaje(String sEvento) {
		sistema(sEvento);
	}

	public static void d(String sEvento) {
		sistema(sEvento);
	}
	
	public static void d(String sEvento, String texto) {
		sistema(sEvento+ " " + texto);
	}
	
	public static void sistema(String sEvento) {
		StringBuffer buffer = new StringBuffer(sEvento);
		Texto.eliminarCaracteresEspeciales(buffer);
		buffer.insert(0, FechaHora.fechaHora() + " | ");
		addEvento(new EventoLogE(EventoLogE.iEVENTO_SISTEMA, buffer.toString()));
		buffer = null;
		///Soporte.dump(sEvento);
		// showEnMainMenu(sText);
	}

	public static void trace(String sEvento) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FechaHora.fechaHora() + " | ");
		buffer.append(sEvento);
		addEvento(new EventoLogE(EventoLogE.iEVENTO_TRACE, buffer.toString()));
	}

	public static void printStack(Exception e) {
		printStack(e.toString());
	}

	public static void printStack(String texto) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FechaHora.fechaHora() + " | ");
		buffer.append(texto);

		Texto.eliminarCaracteresEspeciales(buffer);

		addEvento(new EventoLogE(EventoLogE.iEVENTO_PRINTSTACKERROR,
				buffer.toString()));
	}

	public static synchronized void Check_MAX_SIZE_LOGS() {

		cerrarArchivosLog();

		String fullpath = APP_FOLDER_LOGS + Archivo.SEPARADOR;

		verificaSize(fullpath + ARCH_LOG_SISTEMA, outSistema, MAX_LOGSIZE);

		abrirArchivosLog();
	}

	private static void verificaSize(String nomFile, PrintWriter pwFile,
                                     int iMaxSizeMB) {
		File file = new File(nomFile);
		if (file.length() > (iMaxSizeMB * 1000)) {
			if (pwFile != null) {
				pwFile.close();
				pwFile = null;
			}
			String newName = Archivo.getNewNameFile_AMMDDHHMMSSML(file);
			file.renameTo(new File(newName));
		}
	}

	private static void addEvento(EventoLogE evento) {
		addEvento(evento, null);
	}

	private static void addEvento(EventoLogE evento, String consola) {
		writeFileLog(evento);
	}

	private static void writeFileLog(EventoLogE evento){
		try{
			if (evento.getITipo() == EventoLogE.iEVENTO_SISTEMA) {
				if (outSistema != null) {
					outSistema.println(evento.getSMessage());
					outSistema.flush();
				}
			}
		}catch(Exception e){
			Soporte.dump(e.getMessage());
		}
	}
	
}
