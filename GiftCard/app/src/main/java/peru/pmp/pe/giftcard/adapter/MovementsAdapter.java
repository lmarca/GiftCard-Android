package peru.pmp.pe.giftcard.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.entidades.MovementsItem;
import peru.pmp.pe.giftcard.utilitarios.Soporte;
import peru.pmp.pe.giftcard.utilitarios.Utiles;

/**
 * Created by Admin on 21/11/17.
 */

public class MovementsAdapter extends RecyclerView.Adapter<MovementsAdapter.EmisoresHolder> {


    private ArrayList<MovementsItem> lista;
    private Activity activity;

    public MovementsAdapter(ArrayList<MovementsItem> lista, Activity activity) {
        this.lista = lista;
        this.activity = activity;
    }

    @Override
    public EmisoresHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movements, parent, false);
        return new EmisoresHolder(view);
    }

    @Override
    public void onBindViewHolder(EmisoresHolder holder, int position) {

        holder.text_titulo.setText(lista.get(position).getMovDesTxn());
        //holder.text_fecha.setText(lista.get(position).getMovFechaTxn());
        holder.text_monto.setText(lista.get(position).getMovMontoTxn());

        String date_after = Utiles.formateDateFromstring("yyyyMMdd", "dd/MM/yyyy", lista.get(position).getMovFechaTxn());
        holder.text_fecha.setText(date_after);
        //Soporte.dump("date_after", date_after);

        if (lista.get(position).getMovDesTxn().toUpperCase().contains("MAESTRO"))
            holder.image_logo.setImageResource(R.drawable.maestro);
        else if (lista.get(position).getMovDesTxn().toUpperCase().contains("SODIMAC"))
            holder.image_logo.setImageResource(R.drawable.sodimac);
        else if (lista.get(position).getMovDesTxn().toUpperCase().contains("TOTTUS"))
            holder.image_logo.setImageResource(R.drawable.tottus);
        else
            holder.image_logo.setImageResource(R.drawable.saga);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class EmisoresHolder extends RecyclerView.ViewHolder {

        RelativeLayout linear_container;
        ImageView image_logo;
        TextView text_titulo;
        TextView text_fecha;
        TextView text_monto;

        public EmisoresHolder(View itemView) {
            super(itemView);

            linear_container = (RelativeLayout) itemView.findViewById(R.id.linear_container);
            image_logo = (ImageView) itemView.findViewById(R.id.image_logo);
            text_titulo = (TextView) itemView.findViewById(R.id.text_titulo);
            text_fecha = (TextView) itemView.findViewById(R.id.text_fecha);
            text_monto = (TextView) itemView.findViewById(R.id.text_monto);
        }
    }
}
