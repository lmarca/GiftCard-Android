package peru.pmp.pe.giftcard.libreria;

/**
 * <p>Empresa: MOVILOGIC PERU</p>
 * @author Billy Vera Castellanos
 * @version 1.0
 *
 * NOTA IMPORTANTE :  Calendar.MONTH  empieza los meses desde Cero.
 *
 */

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

@SuppressLint("SimpleDateFormat")
public class FechaHora{

    public static int LUNES = Calendar.MONDAY;
    public static int MARTES = Calendar.TUESDAY;
    public static int MIERCOLES = Calendar.WEDNESDAY;
    public static int JUEVES = Calendar.THURSDAY;
    public static int VIERNES = Calendar.FRIDAY;
    public static int SABADO = Calendar.SATURDAY;
    public static int DOMINGO = Calendar.SUNDAY;
    public static final String NOMENCLATURA_FECHA = "<FECHA>";
    public static final String NOMENCLATURA_FECHAHORA = "<FECHAHORA>";
    public static final String NOMENCLATURA_HORA = "<HORA>";
    public static final String NOMENCLATURA_IDUNICO = "<ID>";

    public static boolean esMayor(Date fechaMayor){
    	return esMayor(fechaMayor, new Date(getCurrentDate()));
    }
    
    public static boolean esMayor(Date fechaMayor, Date fechaMenor){
    	/// Si el año es mayor
    	if(FechaHora.getYear(fechaMayor) > FechaHora.getYear(fechaMenor))
    		return true;
    	/// Si el mes es mayor
    	if(FechaHora.getYear(fechaMayor) == FechaHora.getYear(fechaMenor) &&
    	   FechaHora.getMonth(fechaMayor) > FechaHora.getMonth(fechaMenor)
    		)
    		return true;
    	/// Si el dia es mayor
    	if(FechaHora.getYear(fechaMayor) == FechaHora.getYear(fechaMenor) &&
    	   FechaHora.getMonth(fechaMayor) == FechaHora.getMonth(fechaMenor) &&
    	   FechaHora.getDay(fechaMayor) > FechaHora.getDay(fechaMenor)
    		)
    		return true;
    	
    	return false;
    }
    
	public static enum Format{
	    NONE, YYYYMMDD, DDMMYYYY, MMDDYYYY, DD_MM_YYYY, MM_DD_YYYY, YYYY_MM_DD;
		public static Format get(String name){
			Format[] listado = Format.values();
			Format retorno = NONE;
			for(Format current : listado){
				if(current.toString().equalsIgnoreCase(name)){
					retorno = current;
					break;
				}
			}
			return retorno;
		}
		public String toString(){
			String output = name();
			return output;
		}
	}

	public FechaHora() {
    }

    public static Calendar currentCalendar() {
        return currentCalendar(0);
    }

    public static Calendar currentCalendar(long timeInMiliseg) {
        Calendar calendar = new GregorianCalendar();
        Date trialTime = new Date();
        if (timeInMiliseg > 0) {
            trialTime.setTime(timeInMiliseg);
        }
        calendar.setTime(trialTime);
        return calendar;
    }

    public static int oSegundosFinalizarHora() {
        Calendar fecha = currentCalendar();
        int resto = 60 - fecha.get(Calendar.MINUTE);       // Al poner 60 siempre va a ser 60seg. como minimo
        if (resto == 0) {
            resto = 60 - fecha.get(Calendar.SECOND);
        } else {
            resto = resto * 60;
        }
        return (resto);
    }

    public static int oHoras2Milisegundos(int horas) {
        int resto = 1000 * ((horas * 60) * 60);
        return (resto);
    }

    public static int oMinutos2Milisegundos(int minutos) {
        int resto = 1000 * (minutos * 60);
        return (resto);
    }

    public static int oSegundos2Milisegundos(int segundos) {
        int resto = 1000 * segundos;
        return (resto);
    }

    public static String Fecha() {
        return Fecha(0);
    }

    public static String Fecha(long timeInMiliseg) {
        Calendar calendar = currentCalendar(timeInMiliseg);

        String dia = "", mes = ""; /*seg = "", min = "", hor = "",
        if (calendar.get(Calendar.SECOND) < 10) {
            seg = "0";
        }
        if (calendar.get(Calendar.MINUTE) < 10) {
            min = "0";
        }
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            hor = "0";
        }*/
        if (calendar.get(Calendar.DAY_OF_MONTH) < 10) {
            dia = "0";
        }
        if ((calendar.get(Calendar.MONTH) + 1) < 10) {
            mes = "0";
        }

        String hoy = calendar.get(Calendar.YEAR) + "/"
                + mes + (calendar.get(Calendar.MONTH) + 1) + "/"
                + dia + calendar.get(Calendar.DAY_OF_MONTH);
        return (hoy);
    }

    /*public static String FechaHora(long timeInMiliseg) {
        Calendar calendar = currentCalendar(timeInMiliseg);

        String seg = "", min = "", hor = "", dia = "", mes = "";
        if (calendar.get(Calendar.SECOND) < 10) {
            seg = "0";
        }
        if (calendar.get(Calendar.MINUTE) < 10) {
            min = "0";
        }
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            hor = "0";
        }
        if (calendar.get(Calendar.DAY_OF_MONTH) < 10) {
            dia = "0";
        }
        if ((calendar.get(Calendar.MONTH) + 1) < 10) {
            mes = "0";
        }

        String hoy = calendar.get(Calendar.YEAR) + "/"
                + mes + (calendar.get(Calendar.MONTH) + 1) + "/"
                + dia + calendar.get(Calendar.DAY_OF_MONTH);
        hoy += " " + hor + calendar.get(Calendar.HOUR_OF_DAY) + ":"
                + min + calendar.get(Calendar.MINUTE) + ":"
                + seg + calendar.get(Calendar.SECOND);
        return (hoy);
    }*/

    public static int getDiaSemana() {
        return (getDiaSemana(0));
    }

    public static int getDiaSemana(long timeinmiliseconds) {
        Calendar calendar = currentCalendar(timeinmiliseconds);
        return (calendar.get(Calendar.DAY_OF_WEEK));
    }

    public static int getDay() {
        return getDay(null);
    }

    public static int getDay(Date fecha) {
        Calendar calendar = currentCalendar(fecha == null ? 0 : fecha.getTime());
        return (calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static int getMonth() {
        return getMonth(null);
    }

    public static int getMonth(Date fecha) {
        Calendar calendar = currentCalendar(fecha == null ? 0 : fecha.getTime());
        return (calendar.get(Calendar.MONTH));
    }

    public static int getYear() {
        return getYear(null);
    }

    public static int getYear(Date fecha) {
        Calendar calendar = currentCalendar(fecha == null ? 0 : fecha.getTime());
        return (calendar.get(Calendar.YEAR));
    }

    public static int getHora() {
        Calendar calendar = currentCalendar();
        return (calendar.get(Calendar.HOUR_OF_DAY));
    }

    public static int getMinuto() {
        Calendar calendar = currentCalendar();
        return (calendar.get(Calendar.MINUTE));
    }

    public static int getSegundo() {
        Calendar calendar = currentCalendar();
        return (calendar.get(Calendar.SECOND));
    }

    public static long getTimeMiliseconds() {
        Calendar calendar = currentCalendar();
        return (calendar.getTimeInMillis());
    }

    public static String Hora() {
        return Hora(0);
    }

    public static String Hora(long timeInMilliSeg) {
        Calendar calendar = currentCalendar(timeInMilliSeg);
        String seg = "", min = "", hor = "";
        if (calendar.get(Calendar.SECOND) < 10) {
            seg = "0";
        }
        if (calendar.get(Calendar.MINUTE) < 10) {
            min = "0";
        }
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            hor = "0";
        }

        String hoy = hor + calendar.get(Calendar.HOUR_OF_DAY) + ":"
                + min + calendar.get(Calendar.MINUTE) + ":"
                + seg + calendar.get(Calendar.SECOND);
        return (hoy);
    }

    public static String fechaHora() {
        return (Fecha() + " " + Hora());
    }

    public static String FechaNHora() {
        return (getFechaActual("d MMM yyyy") + " " + Hora());
    }

    public static String ammddhhmmss() {
        return (ammddhhmmssml("", ""));
    }

    public static String ammddhhmmss(String prefijo, String sufijo) {
        StringBuffer buff = new StringBuffer(prefijo + getFechaActual("yyMMddHHmmss") + sufijo);
        Texto.padRight(buff, 15, "0");
        return (buff.toString());
    }

    public static String ammddhhmmssml() {
        return (ammddhhmmssml("", ""));
    }

    public static String ammddhhmmssml(String prefijo, String sufijo) {
        StringBuffer buff = new StringBuffer(prefijo + getFechaActual("yyMMddHHmmssSS") + sufijo);
        Texto.padRight(buff, 15, "0");
        return (buff.toString());
    }

    public static String ammdd() {
        String fec = getFechaActual("yyMMdd");
        return (fec);
    }

    public static String ammmdd() {
        String fec = getFechaActual("yyMMMdd");
        return (fec);
    }

    public static String ddmmm() {
        String fec = getFechaActual("ddMMM");
        return (fec);
    }

    public static String hhmmss() {
        String fec = getFechaActual("HHmmss");
        return (fec);
    }

    public static String aaaammdd(){
    	return aaaammdd(new Date(getCurrentDate()));
    }
    
    public static String aaaammdd(Date fecha){
    	if(fecha == null)
    		return "";
    	return getFormat(fecha, "yyyyMMdd");
    }
    /*
    Letter  Date or Time Component  Presentation  Examples
    ------------------------------------------------------
    G  Era designator  Text  AD
    y  Year  Year  1996; 96
    M  Month in year  Month  July; Jul; 07
    w  Week in year  Number  27
    W  Week in month  Number  2
    D  Day in year  Number  189
    d  Day in month  Number  10
    F  Day of week in month  Number  2
    E  Day in week  Text  Tuesday; Tue
    a  Am/pm marker  Text  PM
    H  Hour in day (0-23)  Number  0
    k  Hour in day (1-24)  Number  24
    K  Hour in am/pm (0-11)  Number  0
    h  Hour in am/pm (1-12)  Number  12
    m  Minute in hour  Number  30
    s  Second in minute  Number  55
    S  Millisecond  Number  978
    z  Time zone  General time zone  Pacific Standard Time; PST; GMT-08:00
    Z  Time zone  RFC 822 time zone  -0800
    
    ejemplo: "yyyyMMdd"
    
    The following examples show how date and time patterns are interpreted in the U.S. locale. The given date and time are 2001-07-04 12:08:56 local time in the U.S. Pacific Time time zone.
    ---------------------------------------------------------------------
    Date and Time Pattern  			Result
    ---------------------------------------------------------------------
    "yyyy.MM.dd G 'at' HH:mm:ss z"  2001.07.04 AD at 12:08:56 PDT
    "EEE, MMM d, ''yy"  			Wed, Jul 4, '01
    "h:mm a"  						12:08 PM
    "hh 'o''clock' a, zzzz"  		12 o'clock PM, Pacific Daylight Time
    "K:mm a, z"  					0:08 PM, PDT
    "yyyyy.MMMMM.dd GGG hh:mm aaa"  02001.July.04 AD 12:08 PM
    "EEE, d MMM yyyy HH:mm:ss Z"  	Wed, 4 Jul 2001 12:08:56 -0700
    "yyMMddHHmmssZ"  				010704120856-0700
     */
    @SuppressLint("SimpleDateFormat")
	public static String getFechaActual(String formato) {
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat(formato);
        Calendar calendar = currentCalendar();
        return formatter.format(calendar.getTime());
    }

    @SuppressLint("SimpleDateFormat")
	public static String getFormat(java.util.Date date, String formato) {
    	if(date == null)
    		return " / / ";
        SimpleDateFormat formatter = new SimpleDateFormat(formato);
        return formatter.format(date);
    }

    // Retorna la fecha en formato java.sql.Date
    // Parametro : Fecha en string formato 'aaaammdd'
    public static java.sql.Date getSQLDate(){
        return getSQLDate(getCurrentStringDate());
    }
    
    public static java.sql.Date getSQLDate(long timestamp){
        return new java.sql.Date(timestamp);
    }

    public static java.sql.Date getSQLDate(String sfecha) {
        return getSQLDate(sfecha, Format.YYYYMMDD);
    }

    public static java.sql.Date getSQLDate(String sfecha, Format iFormato) {
        Calendar fecha = currentCalendar();
        int anio = 2000, mes = 0, dia = 1;
        if (sfecha.length() >= 8) {
            try {
                if (iFormato == Format.YYYYMMDD) {
                    anio = Integer.valueOf(sfecha.substring(0, 4)).intValue();
                    mes = Integer.valueOf(sfecha.substring(4, 6)).intValue();
                    dia = Integer.valueOf(sfecha.substring(6, 8)).intValue();
                } else if (iFormato == Format.DDMMYYYY) {
                    dia = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    mes = Integer.valueOf(sfecha.substring(2, 4)).intValue();
                    anio = Integer.valueOf(sfecha.substring(4, 8)).intValue();
                } else if (iFormato == Format.MMDDYYYY) {
                    mes = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    dia = Integer.valueOf(sfecha.substring(2, 4)).intValue();
                    anio = Integer.valueOf(sfecha.substring(4, 8)).intValue();
                }
                if (iFormato == Format.DD_MM_YYYY) {
                    dia = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    mes = Integer.valueOf(sfecha.substring(3, 5)).intValue();
                    anio = Integer.valueOf(sfecha.substring(6, 10)).intValue();
                } else if (iFormato == Format.MM_DD_YYYY) {
                    mes = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    dia = Integer.valueOf(sfecha.substring(3, 5)).intValue();
                    anio = Integer.valueOf(sfecha.substring(6, 10)).intValue();
                } else if (iFormato == Format.YYYY_MM_DD) {
                    anio = Integer.valueOf(sfecha.substring(0, 4)).intValue();
                    mes = Integer.valueOf(sfecha.substring(5, 7)).intValue();
                    dia = Integer.valueOf(sfecha.substring(8, 10)).intValue();
                }
            } catch (Exception e) {
            }
        }
        fecha.set(Calendar.YEAR, anio);
        fecha.set(Calendar.MONTH, mes - 1);
        fecha.set(Calendar.DAY_OF_MONTH, dia);
        return new java.sql.Date(fecha.getTime().getTime());
    }

    public static String getConvertFormato(String sfecha, Format iFormato,
                                           String sNewFormato) {
        Calendar fecha = Calendar.getInstance();
        int anio = 2000, mes = 0, dia = 1;
        if (sfecha.length() >= 8) {
            try {
                if (iFormato == Format.YYYYMMDD) {
                    anio = Integer.valueOf(sfecha.substring(0, 4)).intValue();
                    mes = Integer.valueOf(sfecha.substring(4, 6)).intValue();
                    dia = Integer.valueOf(sfecha.substring(6, 8)).intValue();
                } else if (iFormato == Format.DDMMYYYY) {
                    dia = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    mes = Integer.valueOf(sfecha.substring(2, 4)).intValue();
                    anio = Integer.valueOf(sfecha.substring(4, 8)).intValue();
                } else if (iFormato == Format.MMDDYYYY) {
                    mes = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    dia = Integer.valueOf(sfecha.substring(2, 4)).intValue();
                    anio = Integer.valueOf(sfecha.substring(4, 8)).intValue();
                }
                if (iFormato == Format.DD_MM_YYYY) {
                    dia = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    mes = Integer.valueOf(sfecha.substring(3, 5)).intValue();
                    anio = Integer.valueOf(sfecha.substring(6, 10)).intValue();
                } else if (iFormato == Format.MM_DD_YYYY) {
                    mes = Integer.valueOf(sfecha.substring(0, 2)).intValue();
                    dia = Integer.valueOf(sfecha.substring(3, 5)).intValue();
                    anio = Integer.valueOf(sfecha.substring(6, 10)).intValue();
                } else if (iFormato == Format.YYYY_MM_DD) {
                    anio = Integer.valueOf(sfecha.substring(0, 4)).intValue();
                    mes = Integer.valueOf(sfecha.substring(5, 7)).intValue();
                    dia = Integer.valueOf(sfecha.substring(8, 10)).intValue();
                }
            } catch (Exception e) {
            }
        }
        fecha.set(Calendar.YEAR, anio);
        fecha.set(Calendar.MONTH, mes - 1);
        fecha.set(Calendar.DAY_OF_MONTH, dia);

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat(sNewFormato);
        Date today = new Date();
        today.setTime(fecha.getTime().getTime());
        String result = formatter.format(today);
        result = result.toUpperCase();
        result = result.replaceAll("DIC", "DEC");
        result = result.replaceAll("SET", "SEP");
        return result;
    }

    public static Vector<String> getVFormatos() {
        Vector<String> v = new Vector<String>();
        Format[] listado = Format.values();
        for(Format o : listado)
        	v.add(o.toString());
        return v;
    }

    public static String Ayer() {
        Calendar calendar = currentCalendar();
        String dia = "", mes = ""; /*seg = "", min = "", hor = "",
        if (calendar.get(Calendar.SECOND) < 10) {
            seg = "0";
        }
        if (calendar.get(Calendar.MINUTE) < 10) {
            min = "0";
        }
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            hor = "0";
        }*/
        if (calendar.get(Calendar.DAY_OF_MONTH) < 10) {
            dia = "0";
        }
        if ((calendar.get(Calendar.MONTH) + 1) < 10) {
            mes = "0";
        }

        String hoy = calendar.get(Calendar.YEAR) + "/"
                + mes + (calendar.get(Calendar.MONTH) + 1) + "/"
                + dia + (calendar.get(Calendar.DAY_OF_MONTH) - 1);
        return (hoy);
    }

    public static int edad(String YYYYMMDD) {
        ///int edad = 0;
        // Hoy en milisegundos.-
        int curDia = getDay();
        int curMes = getMonth();
        int curAnio = getYear();
        if (YYYYMMDD.length() < 8) {
            return 0;
        }
        String[] fecnac = new String[3];
        fecnac[0] = YYYYMMDD.substring(0, 4);
        fecnac[1] = YYYYMMDD.substring(4, 6);
        fecnac[2] = YYYYMMDD.substring(6, 8);
        int nacAnio = Texto.toInt(fecnac[0]);
        int nacMes = Texto.toInt(fecnac[1]);
        int nacDia = Texto.toInt(fecnac[2]);
        int anios = curAnio - nacAnio;
        int meses = curMes - nacMes;
        if (meses < 0) {
            anios--;
        } else if (meses == 0) {
            int dias = curDia - nacDia;
            if (dias < 0) {
                anios--;
            }
        }
        return anios;
    }

    public static int edadMeses(String YYYYMMDD) {
        int edadMeses = 0;
        // Hoy en milisegundos.-
        int curDia = getDay();
        int curMes = getMonth();
        int curAnio = getYear();
        if (YYYYMMDD.length() < 8) {
            return 0;
        }
        String[] fecnac = new String[3];
        fecnac[0] = YYYYMMDD.substring(0, 4);
        fecnac[1] = YYYYMMDD.substring(4, 6);
        fecnac[2] = YYYYMMDD.substring(6, 8);
        int nacAnio = Texto.toInt(fecnac[0]);
        int nacMes = Texto.toInt(fecnac[1]);
        int nacDia = Texto.toInt(fecnac[2]);
        int anios = curAnio - nacAnio;
        int meses = curMes - nacMes;
        if (meses < 0) {
            anios--;
            meses = 12 + meses;
        } else if (meses == 0) {
            int dias = curDia - nacDia;
            if (dias < 0) {
                anios--;
            }
        }
        edadMeses = (anios * 12) + meses;
        return edadMeses;
    }

    public static long getLongDate(String fecha) {
        Calendar calc = Calendar.getInstance();
        if (fecha.length() < 8) {
            return 0;
        }
        String[] fecnac = new String[3];
        fecnac[0] = fecha.substring(0, 4);
        fecnac[1] = fecha.substring(4, 6);
        fecnac[2] = fecha.substring(6, 8);
        int Anio = Texto.toInt(fecnac[0]);
        int Mes = Texto.toInt(fecnac[1]);
        int Dia = Texto.toInt(fecnac[2]);
        calc.set(Calendar.YEAR, Anio);
        calc.set(Calendar.MONTH, Mes - 1);
        calc.set(Calendar.DAY_OF_MONTH, Dia);
        return calc.getTime().getTime();
    }

    public static long getLongDate(int year, int month, int day) {
        Calendar calc = Calendar.getInstance();
        calc.set(Calendar.YEAR, year);
        calc.set(Calendar.MONTH, month - 1);
        calc.set(Calendar.DAY_OF_MONTH, day);
        return calc.getTime().getTime();
    }

    public static Date getLongDate(int year, int month, int day, int hour, int min, int sec) {
        Calendar calc = Calendar.getInstance();
        calc.set(Calendar.YEAR, year);
        calc.set(Calendar.MONTH, month - 1);
        calc.set(Calendar.DAY_OF_MONTH, day);
        calc.set(Calendar.HOUR_OF_DAY, hour);
        calc.set(Calendar.MINUTE, min);
        calc.set(Calendar.SECOND, sec);
        return calc.getTime();
    }
    
    public static String getStringDate(long date) {
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat("yyyyMMdd");
        Date today = new Date();
        today.setTime(date);
        String result = formatter.format(today);
        return result;
    }

    public static long getCurrentDate() {
        Calendar fecha = Calendar.getInstance();
        return fecha.getTime().getTime();
    }

    public static String getCurrentStringDate() {
        Calendar fecha = Calendar.getInstance();
        return getStringDate(fecha.getTime().getTime());
    }

    public static String getCurFormat(String YYYY_MM_DD) {
        if (YYYY_MM_DD.length() == 10) {
            return getConvertFormato(YYYY_MM_DD, Format.YYYY_MM_DD, "dd/MM/yyyy");
        }
        if (YYYY_MM_DD.length() == 8) {
            return getConvertFormato(YYYY_MM_DD, Format.YYYYMMDD, "dd/MM/yyyy");
        }
        return YYYY_MM_DD;
    }

    public static int segundosDiferencia(long fechaInicio, long fechaFin) {
    	int segundos = 0;
        double difference = fechaFin - fechaInicio;
        segundos = (int) (difference / 1000);
    	return segundos;
    }    
    public static int minutosDiferencia(long fechaInicio, long fechaFin) {
    	int segundos = 0;
        double difference = fechaFin - fechaInicio;
        segundos = (int) (difference / (1000*60));
    	return segundos;
    }    
    public static long diasDiferencia(long fechaInicio, long fechaFin) {
        // Next subtract the from date from the to date (make sure the
        // result is a double, this is needed in case of Winter and Summer
        // Time (changing the clock one hour ahead or back) the result will
        // then be not exactly rounded on days. If you use long, this slighty
        // different result will be lost.
        double difference = fechaFin - fechaInicio;

        // Next divide the difference by the number of milliseconds in a day
        // (1000 * 60 * 60 * 24). Next round the result, this is needed of the
        // Summer and Winter time. If the period is 5 days and the change from
        // Winter to Summer time is in the period the result will be
        // 5.041666666666667 instead of 5 because of the extra hour. The
        // same will happen from Winter to Summer time, the result will be
        // 4.958333333333333 instead of 5 because of the missing hour. The
        // round method will round both to 5 and everything is OKE....
        long days = (long) ((difference / (1000 * 60 * 60 * 24)));
        return days;
    }

    public static int mesesDiferencia(long fechaInicio, long fechaFin) {
    	long dias = diasDiferencia(fechaInicio, fechaFin);
    	int meses = (int)dias / 30;
        return meses;
    }
    
    public static long diffSegundos(long fechaInicio, long fechaFin) {
		// Next subtract the from date from the to date (make sure the
		// result is a double, this is needed in case of Winter and Summer
		// Time (changing the clock one hour ahead or back) the result will
		// then be not exactly rounded on days. If you use long, this slighty
		// different result will be lost.
		double difference = fechaFin - fechaInicio;

		// Next divide the difference by the number of milliseconds in a day
		// (1000 * 60 * 60 * 24). Next round the result, this is needed of the
		// Summer and Winter time. If the period is 5 days and the change from
		// Winter to Summer time is in the period the result will be
		// 5.041666666666667 instead of 5 because of the extra hour. The
		// same will happen from Winter to Summer time, the result will be
		// 4.958333333333333 instead of 5 because of the missing hour. The
		// round method will round both to 5 and everything is OKE....
		long horas = (long) ((difference / (1000)));
		return horas;
	}
    public static Calendar getCalendar(String fecha) {
        long longfecha = getLongDate(fecha);
        Date date = new Date(longfecha);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
    
    public static boolean validarDiaSemana(String dia){
        return (Texto.in(dia, "lun,mar,mie,jue,vie,sab,dom,"
                + "lunes,martes,miercoles,jueves,viernes,sabado,domingo"));
    }
    public static boolean validarHoraDia(String hora){
        return (Texto.in(hora, "0,1,2,3,4,5,6,7,8,9,01,02,03,04,05,06,07,08,09,"
                + "10,11,12,13,14,15,16,17,18,19,20,21,22,23"));
    }
    public static String getDiaSemana_TEXT() {
        String dia = "";
        int valor = getDiaSemana();
        switch(valor){
            case 1 : dia = "dom"; break;
            case 2 : dia = "lun"; break;
            case 3 : dia = "mar"; break;
            case 4 : dia = "mie"; break;
            case 5 : dia = "jue"; break;
            case 6 : dia = "vie"; break;
            case 7 : dia = "sab"; break;
        }
        return (dia);
    }
    public static int getDiaSemana(String valor) {
        int dia = 0;
        
        if(Texto.in(valor,"dom,domingo"))
            dia = 1;
        if(Texto.in(valor,"lun,lunes"))
            dia = 2;
        if(Texto.in(valor,"mar,martes"))
            dia = 3;
        if(Texto.in(valor,"mie,miercoles"))
            dia = 4;
        if(Texto.in(valor,"jue,jueves"))
            dia = 5;
        if(Texto.in(valor,"vie,viernes"))
            dia = 6;
        if(Texto.in(valor,"sab,sabado"))
            dia = 7;
        return (dia);
    }
}