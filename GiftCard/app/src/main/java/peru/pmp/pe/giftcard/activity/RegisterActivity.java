package peru.pmp.pe.giftcard.activity;

/**
 * Created by apple on 2/11/17.
 */

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.TramaE;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.networking.GoogleApi;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.ReportUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends MyActivity {

    TramaE tramaE;

    @Bind(R.id.edit_name)
    EditText edit_name;

    @Bind(R.id.edit_email)
    EditText edit_email;

    @Bind(R.id.edit_pwd)
    EditText edit_pwd;

    @Bind(R.id.edit_dni)
    EditText edit_dni;

    @Bind(R.id.tvTerminos)
    TextView tvTerminos;

    @Bind(R.id.chbTerminos)
    AppCompatCheckBox chbTerminos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);
        DesignUtil.customStatusBar(getWindow(), this);

        final String condiciones = "https://wsmobile.punto-web.com/security/Condiciones/terminos_condiciones_ripley.html";
        final String politicas = "https://wsmobile.punto-web.com/security/Privacidad/politicas_privacidad_ripley.html";


        ButterKnife.bind(this);
        /*** INICIALIZACION DE VARIABLES ***/

        edit_dni.setFilters(new InputFilter[] {new InputFilter.LengthFilter(8)});

        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.spinner);
        spinner.setItems("DNI", "CARNET EXTRANJERIA");
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                //Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                Soporte.dump("Clicked " + item);
                if (item.equals("DNI")) {
                    edit_dni.setFilters(new InputFilter[] {new InputFilter.LengthFilter(8)});
                }else {
                    edit_dni.setFilters(new InputFilter[] {new InputFilter.LengthFilter(14)});
                }
                edit_dni.setText("");
            }
        });

        SpannableString terminos = new SpannableString("Acepto los Términos y Condiciones y la Política de Privacidad");
        ClickableSpan terminosClickable = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (condiciones != null) {
                    //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(condiciones));
                    //startActivity(browserIntent);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan politicasClickable = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (politicas != null) {
                    //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(politicas));
                    //startActivity(browserIntent);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.BLUE);
        terminos.setSpan(terminosClickable, 11, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        terminos.setSpan(fcs, 11, 34, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        terminos.setSpan(politicasClickable, 39, 61, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        terminos.setSpan(fcs, 39, 61, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        tvTerminos.setText(terminos);
        tvTerminos.setMovementMethod(LinkMovementMethod.getInstance());
        tvTerminos.setHighlightColor(Color.TRANSPARENT);

    }

    @OnClick(R.id.btnCrear)
    public void getCrearCuenta() {

        if (!Memoria.hayInternet()) {
            toast(ReportUtil.MENSAJE_CONEXION_INTERNET);
            return;
        }

        if (getFieldsEmpty(edit_email, edit_name, edit_pwd, edit_dni)) {
            toast(ReportUtil.MENSAJE_CAMPOS_VACIOS);
            return;
        }

        if (edit_pwd.getText().length() != 4){
            toast("Ingrese una contraseña válida");
            return;
        }

        if (!Soporte.getValidateMail(edit_email)) {
            toast(ReportUtil.MENSAJE_CORREO);
            return;
        }

        if (!chbTerminos.isChecked()) {
            toast("Debe aceptar los términos y condiciones y la política de privacidad");
            return;
        }

        Memoria.setEmailUser(edit_email.getText().toString());
        Memoria.getOperacion().setNombres(edit_name.getText().toString());
        Memoria.getOperacion().setPassword(edit_pwd.getText().toString());
        Memoria.getOperacion().setNumberDoc(edit_dni.getText().toString());
        Memoria.setPasswordUser(edit_pwd.getText().toString());
        Memoria.getOperacion().setTipoDoc("DNI");
        apiJson();
    }

    private boolean getFieldsEmpty(EditText correo, EditText name, EditText pwd, EditText numberDoc) {
        if (Soporte.isNullOrEmpty(correo.getText().toString()) ||
                Soporte.isNullOrEmpty(name.getText().toString()) ||
                Soporte.isNullOrEmpty(pwd.getText().toString()) ||
                Soporte.isNullOrEmpty(numberDoc.getText().toString())) {
            return true;
        }

        return false;
    }

    private void apiJson() {

        showProgressBar("Procesando");

        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetAfiliacionUsuario(tramaE.buildTramaAfiliacionUsuario()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();

                if (response.isSuccessful()) {

                    try {

                        String user = response.body().get("Afiliacion_Usuario").toString();

                        JSONObject obj = new JSONObject(user);

                        if (obj.getString("response_code").equals("00"))
                            show(ConfirmCodeActivity.class);
                        else
                            toast(obj.getString("response_message"));

                    } catch (Throwable tx) {}
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                toast(ReportUtil.MENSAJE_ERROR_RED);
                hideProgressBar();

            }
        });

    }
}
