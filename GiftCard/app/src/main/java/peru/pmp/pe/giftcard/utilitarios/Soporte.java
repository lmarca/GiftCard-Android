package peru.pmp.pe.giftcard.utilitarios;

import android.annotation.SuppressLint;
import android.util.Log;
import android.widget.EditText;

import peru.pmp.pe.giftcard.libreria.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase utilitaria que permite manejar diversos metodos utilizados en el sistema movil
 * @author Billy Vera Castellanos
 * @version 1.0
 */
public class Soporte {
	
    public static String showCurrentInfo(){
        StringBuffer texto = new StringBuffer();
        Runtime rt = Runtime.getRuntime();
        texto.append(" Fecha de Lectura : " + FechaHora.fechaHora() + "\n");
        texto.append(" Procesadores     : " + rt.availableProcessors() + "\n");
        texto.append(" Memoria montada  : " + Soporte.maskBytes(rt.totalMemory()) + "\n");
        texto.append(" Memoria libre    : " + Soporte.maskBytes(rt.freeMemory()) + "\n");
        texto.append(" Memoria maxima   : " + Soporte.maskBytes(rt.maxMemory()) + "\n");
        return texto.toString();
    }
    
    public static String readTeclado(){
        return readTeclado(null);
    }
    public static String readTeclado(String prompt){
        String teclado = "";
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        try{
            if(prompt!=null)
                System.out.print(prompt);
            teclado = input.readLine();
        }catch(IOException io){teclado="";}
        input = null;
        return teclado;
    }
    
    /************************************************************************
     * @funciones numericas
     */
    public static int toInt(String valor) {
        int intValue = 0;
        try {
        	if(!Soporte.isNullOrE(valor))
        		intValue = Integer.parseInt(valor);
        } catch (Exception e) {
        }
        return intValue;
    }

    public static short toShort(String valor) {
        short intValue = 0;
        try {
        	if(!Soporte.isNullOrE(valor))
        		intValue = Short.parseShort(valor);
        } catch (Exception e) {
        }
        return intValue;
    }

    public static long toLong(String valor) {
        long retorno = 0;
        try {
        	if(!Soporte.isNullOrE(valor))
        		retorno = Long.parseLong(valor);
        } catch (Exception e) {
        }
        return retorno;
    }

    public static double toDouble(String valor) {
        double retorno = 0;
        try {
        	if(!Soporte.isNullOrE(valor))
        		retorno = Double.parseDouble(valor);
        } catch (Exception e) {
        }
        return retorno;
    }

    public static void dump(String prefijo, Exception e) {
        dump(prefijo + " : " +e.toString());
    }
    public static void dump(String prefijo, String e) {
        dump(prefijo + " : " +e);
    }
    public static void dump(String mensaje) {
        Log.d("debug", "BOTON:"+mensaje);
        ///com.procesos.mc.boton.manejadores.Log.mensaje(mensaje);
    }
    private static boolean toLog = false;
    
    public static void toLog(boolean valor){
    	toLog = valor;
    }
    public static void info(String prefijo, Exception e) {
        info(prefijo + " : " +e.toString());
    }
    public static void info(String prefijo, String e) {
        info(prefijo + " : " +e);
    }
    public static void info(String mensaje) {
        ///Log.i("debug", mensaje);
    }
    public static void trace(String mensaje) {
    	///Log.trace(mensaje);
    	///System.out.println(FechaHora.fechaHora() +" | trace | " + mensaje);
    }
    public static void alert(String mensaje) {
    	//log.severe(mensaje);
        System.out.println(FechaHora.fechaHora() +" | " + mensaje);
    }
    public static void system(String mensaje) {
    	//Log.trace(mensaje);
        dump(mensaje);
    	///Log.sistema(mensaje);
    }
    public static void err(Exception mensaje) {
        dump(mensaje.getMessage());
    	///Log.printStack(mensaje);
    }
    public static void err(String mensaje) {
    	//Log.trace(mensaje);
        dump(mensaje);
    	///Log.printStack(mensaje);
    }

    public static boolean isNullOrE(String texto) {
        return isNullOrEmpty(texto);
    }

    public static boolean isNullOrEmpty(String texto) {
        return texto == null || texto.trim().length() == 0;
    }

    public static void sleep(long milisegundos){
        try{
            Thread.sleep(milisegundos);	/// Puede ser interrumpido
        	///SystemClock.sleep(milisegundos); //// Cuidado!!! Ignora el comando interrupted!!!
        }catch(Exception e){
            
        }
    }

    /**
     * Convierte los {@link EditText} en String
     */

    public String getConvertFieldsString(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static boolean getValidateMail(EditText correo) {

        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        String email = correo.getText().toString().trim();

        Matcher matcher = pattern.matcher(email);

        if (matcher.find() == true) {
            return true;
        }

        return false;
    }

    public static String maskBytes(long bytes){
    	String umed = "bytes";
		long sizeInKB = bytes / 1024;
		long sizeInMB = sizeInKB / 1024;
		String total = String.valueOf(bytes);

		if(sizeInMB>=1){
			sizeInMB = (sizeInKB * 100) / 1024;
    		umed = "MB";
    		String texto = ""+sizeInMB;
    		if(texto.length()>2)
    			total = texto.substring(0,texto.length()-2)+"."+texto.substring(texto.length()-2);
    		else
    			total = texto;
    	}else{
    		if(sizeInKB>=1){
    			umed = "KB";
    			total = String.valueOf(sizeInKB);
    		}
    	}
    	return total +" "+ umed;
    }

	
    public static String extractDominio(String email){
        String[] bloques = email.split("@");
        if(bloques.length>1)
            return bloques[1];
        else
            return email;
    }
    
    public static boolean isGmail(String email){
        return email.endsWith("gmail.com");
    }

    public static boolean isYahoo(String email){
        return email.endsWith("yahoo.com") ||
                email.endsWith("yahoo.es") ||
                email.endsWith("ymail.com") ||
                email.endsWith("rocketmail.com");
    }
    
    public static boolean isHotmail(String email){
        return email.endsWith("hotmail.com");
    }
    
    @SuppressLint("NewApi")
	public static String UTF8Decode(byte in[], int offset, int length) {
    	if(in != null){
    		try{
    			return new String(in, offset, length, Charset.forName("UTF-8"));
    		}catch(Exception e){
    			return new String(in, offset, length);
    		}
    	}
    	return "";
    }

    public static String ISO8859_1Decode(byte in[], int offset, int length) {
    	if(in != null){
    		try{
    			return new String(in, offset, length, Charset.forName(ByteBuffer.ISO8859_1));
    		}catch(Exception e){
    			return new String(in, offset, length);
    		}
    	}
    	return "";
    }

    public static byte[] UTF8Encode(String str) {
    	if(str != null){
    		try{
    			return str.getBytes(ByteBuffer.DEFAULT_CHAR_ENCODING);
    		}catch(UnsupportedEncodingException u){
    			return str.getBytes();
    		}
    	}
    	return new byte[0];
    }

}
