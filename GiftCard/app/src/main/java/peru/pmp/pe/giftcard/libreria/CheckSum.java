/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package peru.pmp.pe.giftcard.libreria;

import peru.pmp.pe.giftcard.utilitarios.Soporte;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * @author Billy Vera Castellanos
 */
public class CheckSum {

	/***
	 * Convierte un arreglo de bytes a String usando valores hexadecimales
	 * 
	 * @param digest
	 *            arreglo de bytes a convertir
	 * @return String creado a partir de <code>digest</code>
	 */
	private static String toHexadecimal(byte[] digest) {
		String hash = "";
		for (byte aux : digest) {
			int b = aux & 0xff;
			if (Integer.toHexString(b).length() == 1) {
				hash += "0";
			}
			hash += Integer.toHexString(b);
		}
		return hash;
	}

	/***
	 * Realiza la suma de verificaciÃ³n de un archivo mediante MD5
	 * 
	 * @param archivo
	 *            archivo a que se le aplicara la suma de verificaciÃ³n
	 * @return valor de la suma de verificaciÃ³n.
	 */
	public static String getMD5Checksum(File archivo) {
		byte[] textBytes = new byte[1024];
		MessageDigest md = null;
		int read = 0;
		String md5 = null;
		try {
			InputStream is = new FileInputStream(archivo);
			md = MessageDigest.getInstance("MD5");
			while ((read = is.read(textBytes)) > 0) {
				md.update(textBytes, 0, read);
			}
			is.close();
			byte[] md5sum = md.digest();
			md5 = toHexadecimal(md5sum);
		} catch (FileNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		} catch (IOException e) {
		}

		return md5;
	}

	/***
	 * Realiza la suma de verificaciÃ³n de un archivo mediante MD5
	 * 
	 * @param archivo
	 *            archivo a que se le aplicara la suma de verificaciÃ³n
	 * @return valor de la suma de verificaciÃ³n.
	 */
	public static String getMD5Checksum(byte[] textBytes) {
		MessageDigest md = null;
		int read = textBytes.length;
		String md5 = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(textBytes, 0, read);
			byte[] md5sum = md.digest();
			md5 = toHexadecimal(md5sum);
		} catch (NoSuchAlgorithmException e) {
		}

		return md5;
	}

	public static String getSHA1Checksum(String s, String keyString) {
		byte[] bytes = null;
		try {
		SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"),
				"HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(key);

		bytes = mac.doFinal(s.getBytes("UTF-8"));
		}catch(UnsupportedEncodingException e){
			
		}catch(NoSuchAlgorithmException e){
			
		}catch(InvalidKeyException e){
			
		}
		return bytes!=null ? new String(Base64Coder.encode(bytes)) : "";

	}

	public static String getSubToken(String stringhex) {
		String token = null;
		long valorFinal = 0;
		try {
			String valor = stringhex + stringhex;

			int posc = Texto.toInt("" + valor.charAt(valor.length() - 1), 16) * 2;

			String fragmento = valor.substring(posc, posc + (7 * 2));

			valorFinal = Long.parseLong(fragmento, 16);
			valorFinal = valorFinal % 1000000000000000L;

			valor = null;
			fragmento = null;

		} catch (Exception e) {
			Soporte.dump("CheckSum.getSubToken:" + e.getMessage());
			valorFinal = 0;
		} finally {
			token = Texto.padLeft("" + valorFinal, 15, "0");
		}
		return token;
	}
}
