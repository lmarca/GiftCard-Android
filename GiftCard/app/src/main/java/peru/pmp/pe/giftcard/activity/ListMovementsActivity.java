package peru.pmp.pe.giftcard.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.adapter.MovementsAdapter;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.MovementsItem;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;

/**
 * Created by Admin on 21/11/17.
 */

public class ListMovementsActivity extends MyActivity {

    @Bind(R.id.text_pan)
    TextView text_pan;

    @Bind(R.id.text_saldo)
    TextView text_saldo;

    @Bind(R.id.text_monto)
    TextView text_monto;


    @Bind(R.id.recycler_lista_mov)
    RecyclerView recycler_lista_mov;
    private LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(Memoria.getContext());
    private MovementsAdapter movementsAdapter;
    private ArrayList<MovementsItem> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_movements);

        DesignUtil.customStatusBarColor(getWindow(), this, R.color.colorPrimaryDark);
        ButterKnife.bind(this);

        text_pan.setText(getIntent().getExtras().getString("card_mask"));
        text_saldo.setText("S/ " + getIntent().getExtras().getString("SaldoDispActual"));
        text_monto.setText(getIntent().getExtras().getString("LastMontoTxn"));

        linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_lista_mov.setLayoutManager(linearLayoutManager1);
        recycler_lista_mov.setItemAnimator(new DefaultItemAnimator());
        movementsAdapter = new MovementsAdapter(Memoria.getHashMovements(), this);
        recycler_lista_mov.setAdapter(movementsAdapter);
        movementsAdapter.notifyItemRangeInserted(movementsAdapter.getItemCount(), Memoria.getHashMovements().size());

    }

    @OnClick(R.id.btnLogout)
    public void logout() { finishSession();}

    private void llenarData() {

        MovementsItem mov01 = new MovementsItem("2017-10-11","Descripción Compra TOTTUS","","S/ 10.23","","","");
        MovementsItem mov02 = new MovementsItem("2017-02-06","Descripción Compra TOTTUS","","S/ 100.00","","","");
        MovementsItem mov03 = new MovementsItem("2017-04-08","Descripción Compra SODIMAC","","S/ 568.10","","","");
        MovementsItem mov04 = new MovementsItem("2017-09-11","Descripción Compra SAGA FALABELLA","","S/ 50.68","","","");
        MovementsItem mov05 = new MovementsItem("2017-12-06","Descripción Compra MAESTRO","","S/ 48.23","","","");
        MovementsItem mov06 = new MovementsItem("2017-11-08","Descripción Compra SODIMAC","","S/ 30.00","","","");

        list.add(0, mov01);
        list.add(1, mov02);
        list.add(2, mov03);
        list.add(3, mov04);
        list.add(4, mov05);
        list.add(5, mov06);
        list.add(6, mov01);

        movementsAdapter.notifyItemRangeInserted(movementsAdapter.getItemCount(), list.size());

    }
}
