package peru.pmp.pe.giftcard.libreria;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import peru.pmp.pe.giftcard.utilitarios.Soporte;
import peru.pmp.pe.giftcard.baseclass.MyActivity;


public class ProgressBarHandler {
	private Dialog dialog;
    private TextView mTextView;
    private Context mContext;

    public ProgressBarHandler(Context contexto) {
        mContext = contexto;

		dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(MyActivity.buildLayoutProgressBar(contexto, "", 20));
		dialog.setCancelable(false);
        
        //mTextView = (TextView)dialog.findViewById(1000);

        hide();
    }

    public void show() {
    	showInUIThread(null);
    }

    public void showInUIThread(final String texto) {
    	((Activity)mContext).runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Soporte.dump("showInUIThread="+texto);
				if(texto!=null){
			    	mTextView.setText(texto);
			    	mTextView.setVisibility(View.VISIBLE);
				}else{
			        mTextView.setVisibility(View.GONE);
				}
				dialog.show();
			}
		});
    }    
    public void hide() {
    	((Activity)mContext).runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				dialog.hide();
			}
		});
    }
    
    public void dismiss(){
    	dialog.dismiss();
    }
}