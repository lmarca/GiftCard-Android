package peru.pmp.pe.giftcard.libreria;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;

import peru.pmp.pe.giftcard.utilitarios.Soporte;

import java.util.concurrent.Semaphore;

public class SMS {

	private Context localContext;
	private boolean smsEnviado;
	private Semaphore semaforo;
	
	public SMS(Context c) {
		localContext = c;
		smsEnviado = false;
		semaforo = new Semaphore(0);
	}

	public boolean sendSMS(String phoneNumber, String message) {
		String SENT = "SMS_SENT";
		///String DELIVERED = "SMS_DELIVERED";
		BroadcastReceiver receiver = null;
		PendingIntent sentPI = PendingIntent.getBroadcast(localContext, 0,
				new Intent(SENT), 0);

		/*PendingIntent deliveredPI = PendingIntent.getBroadcast(localContext, 0,
				new Intent(DELIVERED), 0);*/

		 
		smsEnviado = false;
		try{
			receiver = new BroadcastReceiver() {

				@Override
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Soporte.dump("SMS sent");
						smsEnviado = true;
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						Soporte.dump("Generic failure");
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						Soporte.dump("No service");
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						Soporte.dump("Null PDU");
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						Soporte.dump("Radio off");
						break;
					}
					semaforo.release();
				}

			};
		// ---when the SMS has been sent---
		localContext.registerReceiver(receiver, new IntentFilter(SENT));

		// ---when the SMS has been delivered---
		/*localContext.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Soporte.dump("SMS delivered");
					break;
				case Activity.RESULT_CANCELED:
					Soporte.dump("SMS not delivered");
					break;
				}
			}
		}, new IntentFilter(DELIVERED));*/

			SmsManager sms = SmsManager.getDefault();
			Soporte.dump("Enviando SMS("+phoneNumber+"):" + message );
			sms.sendTextMessage(phoneNumber, null, message, sentPI, null); ///sentPI, deliveredPI);
			semaforo.acquire();
			
		}catch(Exception e){
			
		}finally{
			if(receiver!=null)
				localContext.unregisterReceiver(receiver);
			receiver = null;
		}
		return smsEnviado;
	}

}
