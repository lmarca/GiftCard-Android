package peru.pmp.pe.giftcard.activity;


import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.adapter.ViewPagerAdapter;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;


public class MainActivity extends MyActivity {

    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;

    @Bind(R.id.text_titulo)
    TextView text_titulo;

    @Bind(R.id.text_subtitulo)
    TextView text_subtitulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DesignUtil.customStatusBarFullScreen(getWindow(), this);

        ButterKnife.bind(this);

        Typeface face=Typeface.createFromAsset(getAssets(),"fonts/poppins-medium.ttf");
        text_titulo.setTypeface(face);

        Typeface face1=Typeface.createFromAsset(getAssets(),"fonts/poppins-regular.ttf");
        //text_subtitulo.setTypeface(face1);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);

        viewPager.setAdapter(viewPagerAdapter);

        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    text_titulo.setText("Bienvenido a tu app GiftCard Falabella");
                    text_subtitulo.setText("La forma más rápida de dar seguimiento a tus compras.");
                }else if (position == 1){
                    text_titulo.setText("Solo necesitas un dispositivo móvil");
                    text_subtitulo.setText("Puedes registrar más de un GiftCard a la vez.");
                }else{
                    text_titulo.setText("Utiliza tu GiftCard en las tiendas del Grupo Falabella");
                    text_subtitulo.setText("Y vive una nueva experiencia de compra.");
                }

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);

    }

    @OnClick(R.id.btnLogin)
    public void getLogin() { show(LoginActivity.class);}

    @OnClick(R.id.btnRegister)
    public void getRegister() {show(RegisterActivity.class);}

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(viewPager.getCurrentItem() == 0){
                        viewPager.setCurrentItem(1);
                    } else if(viewPager.getCurrentItem() == 1){
                        viewPager.setCurrentItem(2);
                    } else {
                        viewPager.setCurrentItem(0);
                    }

                }
            });

        }
    }

}
