package peru.pmp.pe.giftcard.manejadores;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import peru.pmp.pe.giftcard.utilitarios.Soporte;

public class TripleDES {

	Cipher TDB64Encrypt;
	Cipher TDB64Decrypt;
	static String IV;

	public TripleDES(String Key) {
		try {
			IV = "B36B6BFB6231084E";

			byte[] encryptKey = Key.getBytes();
			DESedeKeySpec ks = new DESedeKeySpec(encryptKey);
			SecretKeyFactory kf = SecretKeyFactory.getInstance("DESede");
			javax.crypto.SecretKey sKey = kf.generateSecret(ks);

			this.TDB64Encrypt = Cipher.getInstance("DESede/CBC/PKCS5Padding");

			this.TDB64Decrypt = Cipher.getInstance("DESede/CBC/PKCS5Padding"); ///CFB/NoPadding

			IvParameterSpec IVParametro = new IvParameterSpec(hexToBytes(IV));

			this.TDB64Encrypt.init(Cipher.ENCRYPT_MODE, sKey, IVParametro);
			this.TDB64Decrypt.init(Cipher.DECRYPT_MODE, sKey, IVParametro);
		} catch (Exception e) {
		}
	}

	public String Encripta(String Texto) {
		try {
			byte[] TextoPlano = Texto.getBytes();
			byte[] encTextoPlano = this.TDB64Encrypt.doFinal(TextoPlano);
			return EncodingBase64(encTextoPlano); ///  
		} catch (Exception e) {
		}

		return null;
	}

	public String Desencripta(String Texto) {
		try {
			byte[] encTexto = Base64.decode(Texto.getBytes(), Base64.DEFAULT);
			byte[] desTexto = this.TDB64Decrypt.doFinal(encTexto);
			
			return new String(desTexto);
		} catch (Exception e) {
			Soporte.dump("Desencripta:"+e.getMessage());
		}
		return null;
	}

	public static String EncodingBase64(byte[] Data)
			throws UnsupportedEncodingException {
		return new String(Base64.encode(Data, Base64.DEFAULT));
	}

	public static String UrlEncodingBase64(byte[] Data)
			throws UnsupportedEncodingException {
		return java.net.URLEncoder.encode(new String(Base64.encode(Data, Base64.DEFAULT)),
				"UTF-8");
	}

	public static String DecodingBase64(String Data)
			throws UnsupportedEncodingException {
		return URLDecoder.decode(Data, "UTF-8");
	}

	public static byte[] hexToBytes(String str) {
		byte[] bytes = new byte[(str.length() + 1) / 2];
		if (str.length() == 0) {
			return bytes;
		}
		bytes[0] = 0;
		int nibbleIdx = str.length() % 2;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!isHex(c)) {
				throw new IllegalArgumentException(
						"string contains non-hex chars");
			}
			if (nibbleIdx % 2 == 0) {
				bytes[(nibbleIdx >> 1)] = ((byte) (hexValue(c) << 4));
			} else {
				int tmp92_91 = (nibbleIdx >> 1);
				byte[] tmp92_88 = bytes;
				tmp92_88[tmp92_91] = ((byte) (tmp92_88[tmp92_91] + (byte) hexValue(c)));
			}
			nibbleIdx++;
		}
		return bytes;
	}

	private static boolean isHex(char c) {
		return ((c >= '0') && (c <= '9')) || ((c >= 'a') && (c <= 'f'))
				|| ((c >= 'A') && (c <= 'F'));
	}

	private static int hexValue(char c) {
		if ((c >= '0') && (c <= '9'))
			return c - '0';
		if ((c >= 'a') && (c <= 'f')) {
			return c - 'a' + 10;
		}
		return c - 'A' + 10;
	}
}
