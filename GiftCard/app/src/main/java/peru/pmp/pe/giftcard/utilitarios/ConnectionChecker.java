package peru.pmp.pe.giftcard.utilitarios;

import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;

import peru.pmp.pe.giftcard.R;


public class ConnectionChecker {

    public static boolean hasInternetConnection(Context context){
        if(context != null){
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            assert connMgr != null;
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        }else{
            return false;
        }
    }

    public static void showConnectionAlertDialog(Context context){
        if(context != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setMessage(context.getString(R.string.no_connection_message))
                    .setPositiveButton("OK", null);
            builder.show();
        }
    }

    public static void showConnectionAlertDialog(Context context, String mensaje){
        if(context != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setMessage(mensaje)
                    .setPositiveButton("OK", null);
            builder.show();
        }
    }

    public boolean checkConnection(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo != null  && networkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }

    /**Muestra un snackbar indicando que el dispositivo no tiene conexión a internet
     * @param view Vista a la cual se anclará el snackbar
     * @param message Mensaje de error a mostrar en el snackbar*/
    public void showNoConnectionMessage(View view, String message){
        Snackbar snackbar = Snackbar
                                .make(view, message, Snackbar.LENGTH_INDEFINITE)
                                .setAction(android.R.string.ok, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                });
        snackbar.show();
    }

}
