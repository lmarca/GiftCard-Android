package peru.pmp.pe.giftcard.utilitarios;

/**
 * util que se encarga de mostrar datos del aplicativo
 *
 * @author Leandro Castillo
 */
public class ReportUtil {

    /**
     * Variables que se encargan de ser los titulos
     */

    public static String TITLE_FRECUENTES = "PREGUNTAS FRECUENTES";
    public static String TITLE_GENERALES = "CONSULTAS GENERALES";
    public static String TITLE_REGLAMOS = "RECLAMOS";
    public static String TITLE_SUGERENCIAS = "SUGERENCIAS";

    /**
     * Variables que se encargan de ser la descripcion
     */

    public static String DESCRIP_GENERAL = "Si usted requiere orientación general deberá completar el formulario.";
    public static String DESCRIP_REGLAMOS = "DEFENSORÍA DEL INVERSIONISTA\n" +
            "\n" + "Si requiere orientación gratuita ante situaciones concretas en las que considere que se podrían estar afectando sus derechos en materias relacionadas con los mercados bajo la competencia de la Superintendencia del Mercado de Valores (SMV) complete el siguiente formulario";
    public static String DESCRIP_SUGERENCIAS = "Si cuenta con alguna sugerencia o propuesta complete el siguiente formulario";
    public static String MENSAJE_SECTOR = "SECTOR ECONÓMICO";
    public static String MENSAJE_MUTUOS = "SOCIEDAD ADMINISTRADORA DE FONDOS";
    public static String MENSAJE_EMPRESA = "TIPO DE EMPRESA";

    /**
     * Mensajes del aplicaivo
     */

    public static String MENSAJE_PDF = "El PDF se descargo correctamente";

    /**
     * Mensajes para conexion a internert
     */

    public static String MENSAJE_CONEXION_INTERNET = "No hay conexión a Internet";

    /**
     * Mensajes para verificar si hay datos o no
     */

    public static String MENSAJE_RESULTADO = "No se encontró resultados";

    /**
     * Mensajes de agregacion de pordafolio
     */

    public static String MENSAJE_COTIZACION_AGREGADO = "Se agregó la cotización a su portafolio";
    public static String MENSAJE_COTIZACION_DESAGREGADO = "Se ha removido la cotización de su portafolio";

    public static String MENSAJE_MUTUOS_AGREGADO = "Se agregó el fondo mutuo a su portafolio";
    public static String MENSAJE_MUTUOS_DESAGREGADO = "Se ha removido el fondo mutuo de su portafolio";

    /**
     * Campos vacios
     */

    public static String MENSAJE_CAMPOS_VACIOS = "Complete todos los campos requeridos";

    /**
     * Usuario no valido
     */

    public static String MENSAJE_USUARIO_INVALIDO = "El correo o la contraseña no son correctos";

    /**
     * Correo no valido
     */

    public static String MENSAJE_CORREO = "El correo que ingreso no es válido";

    /**
     * Telefono no validos
     */

    public static String MENSAJE_TELEFONO = "El número de teléfono que ingreso no es valido";

    /**
     * Mensaje de documento
     */

    public static String MENSAJE_DOCUMENTO = "El documento que ingreso no es valido";

    /**
     * Mensajes de formulario
     */

    public static String MENSSAJE_ENVIADO = "El formulario se envió correctamente";

    /**
     * Fecha de fondos mutuos
     */

    public static String FECHA_MUTUOS = "ÚLTIMA ACTUALIZACIÒN :";

    /**
     * Mensaje cuando suceda un error en login con facebook
     */

    public static String MENSAJE_ERROR_RED = "Hubo un problema con la conexión, inténtelo de nuevo";

    public static String MENSAJE_PORDAFOLIO_COTIZACION = "Aún no tienes favoritos.\nPara agregar una cotización a tu portafolio es necesario seleccionar el maletín en la parte superior.";
    public static String MENSAJE_PORDAFOLIO_FONDOSMUTUOS = "Aún no tienes favoritos.\nPara agregar un fondo mutuo a tu portafolio es necesario seleccionar el maletín en la parte superior.";

    /**
     * PDF mensaje
     */

    public static String MENSAJE_PDF_ERROR = "No se puede visualizar el documento.";

    /**
     * Registro
     */

    public static String MENSAJE_REGISTRO_EXISTE = "El usuario que desea ingresar ya existe";


    public static String MENSAJE_REGISTRO_EXITOSO = "Registro exitoso";

    public static String MENSAJE_FALTA_NOMBRE = "Por favor ingrese su nombre";
    public static String MENSAJE_FALTA_APELLIDO = "Por favor ingrese su apellidos";
    public static String MENSAJE_FALTA_CORREO = "Por favor ingrese su email";
    public static String MENSAJE_FALTA_DOCUMENTO = "Por favor ingrese su número de documento";
    public static String MENSAJE_FALTA_ASUNTO = "Por favor ingrese el asunto de su consulta";
    public static String MENSAJE_FALTA_SITUACION = "Por favor ingrese su consulta";

}