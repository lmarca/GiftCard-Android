package peru.pmp.pe.giftcard.entidades;

import org.json.JSONObject;

import java.net.URLEncoder;

import peru.pmp.pe.giftcard.baseclass.Constantes;
import peru.pmp.pe.giftcard.baseclass.Entidad;
import peru.pmp.pe.giftcard.libreria.FechaHora;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.utilitarios.Soporte;

/**
 * Created by Admin on 14/11/17.
 */

public class TramaE extends Entidad {

    JSONObject paramObject;
    JSONObject json_obj;

    public TramaE() {
        super();
        reset();
    }

    public void reset(){
        paramObject = new JSONObject();
        json_obj    = new JSONObject();
    }

    public String buildTramaAutenticaUsuario() {

        try {

            paramObject.put("merchant_code", Constantes.MERCHANT_CODE);
            paramObject.put("entry_mode_app","A");
            paramObject.put("buyer_merchant_id", Memoria.getEmailUser());
            paramObject.put("buyer_merchant_password", Memoria.getOperacion().getPassword());
            paramObject.put("buyer_mobile_id", Memoria.getIMEI());
            //paramObject.put("buyer_mobile_id", "C46C0D0A-AFE4-4778-AC59-5723CEB6D27C");
            paramObject.put("mobile_firebase_id","");
            paramObject.put("autenticacion_usuario_date", FechaHora.aaaammdd());
            paramObject.put("autenticacion_usuario_time", FechaHora.hhmmss());

            json_obj.put("Autentica_Usuario", paramObject);
            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public String buildTramaOlvidoPassword() {

        try {

            paramObject.put("merchant_code", Constantes.MERCHANT_CODE);
            paramObject.put("entry_mode_app","A");
            paramObject.put("buyer_email", Memoria.getEmailUser());
            paramObject.put("olvido_password_date", FechaHora.aaaammdd());
            paramObject.put("olvido_password_time", FechaHora.hhmmss());

            json_obj.put("Olvido_Password", paramObject);
            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public String buildTramaAfiliacionCodigoActivacion() {

        try {

            paramObject.put("merchant_code", Constantes.MERCHANT_CODE);
            paramObject.put("entry_mode_app","A");
            paramObject.put("buyer_merchant_id", Memoria.getEmailUser());
            paramObject.put("buyer_email", Memoria.getEmailUser());
            paramObject.put("mobile_firebase_id","");
            paramObject.put("afiliacion_alta_date", FechaHora.aaaammdd());
            paramObject.put("afiliacion_alta_time", FechaHora.hhmmss());

            json_obj.put("Afiliacion_Codigo_Activacion", paramObject);
            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public String buildTramaAfiliacionAlta() {

        try {

            paramObject.put("merchant_code", Constantes.MERCHANT_CODE);
            paramObject.put("entry_mode_app", "A");
            paramObject.put("buyer_merchant_id", Memoria.getEmailUser());
            paramObject.put("codigo_activacion", Memoria.getOperacion().getCodeActivacion());
            paramObject.put("buyer_nro_celular", "");
            paramObject.put("buyer_mobile_id", Memoria.getIMEI());
            paramObject.put("buyer_mobile_so", "2");
            paramObject.put("buyer_mobile_tipo", "4.2");
            paramObject.put("buyer_mobile_modelo", "");
            paramObject.put("buyer_email", Memoria.getEmailUser());
            paramObject.put("mobile_firebase_id", "");
            paramObject.put("afiliacion_alta_date", FechaHora.aaaammdd());
            paramObject.put("afiliacion_alta_time", FechaHora.hhmmss());

            json_obj.put("Afiliacion_Alta", paramObject);
            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public String buildTramaAfiliacionUsuario() {

        try {

            paramObject.put("merchant_code", Constantes.MERCHANT_CODE);
            paramObject.put("entry_mode_app", "A");
            paramObject.put("buyer_first_name", Memoria.getOperacion().getNombres());
            paramObject.put("buyer_last_name", "");
            paramObject.put("buyer_doc_type", Memoria.getOperacion().getTipoDoc());
            paramObject.put("buyer_doc_number", Memoria.getOperacion().getNumberDoc());
            paramObject.put("buyer_nro_celular", "");
            paramObject.put("buyer_email", Memoria.getEmailUser());
            paramObject.put("buyer_merchant_id", Memoria.getEmailUser());
            paramObject.put("buyer_merchant_password", Memoria.getOperacion().getPassword());
            paramObject.put("aceptar_condicion", "1");
            paramObject.put("afiliacion_date", FechaHora.aaaammdd());
            paramObject.put("afiliacion_time", FechaHora.hhmmss());

            json_obj.put("Afiliacion_Usuario", paramObject);

            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public String buildTramaRegistraTarjeta() {

        //TDESBase64 objTDESB64 = new TDESBase64(Memoria.getUser().getApi_key());

        Memoria.setLlaveEnc(Memoria.getUser().getApi_key());

        try {

            paramObject.put("api_login", Memoria.getUser().getApi_login());
            paramObject.put("api_key", Memoria.getUser().getApi_key());
            paramObject.put("merchant_code", Memoria.getUser().getMerchant_code());
            paramObject.put("transaction_order_number", "");
            paramObject.put("transaction_currency", Memoria.getUser().getTransaction_currency());
            paramObject.put("transaction_amount", "1.00");
            paramObject.put("transaction_merchant_code", Memoria.getUser().getMerchant_code());
            paramObject.put("transaction_language", "ES");
            paramObject.put("brand_validar_tarjeta_monto", "0");
            paramObject.put("buyer_token_id", Memoria.getUser().getBuyer_token_id());
            paramObject.put("buyer_merchant_id", Memoria.getUser().getBuyer_email());
            paramObject.put("buyer_doc_type", Memoria.getUser().getBuyer_doc_type());
            paramObject.put("buyer_doc_number", Memoria.getUser().getBuyer_doc_number());
            paramObject.put("buyer_first_name", Memoria.getUser().getBuyer_first_name());
            paramObject.put("buyer_last_name", Memoria.getUser().getBuyer_last_name());
            paramObject.put("buyer_email", Memoria.getUser().getBuyer_email());
            paramObject.put("transaction_autogenerate", FechaHora.ammddhhmmssml());
            paramObject.put("transaction_autentication_date", FechaHora.aaaammdd());
            paramObject.put("transaction_autentication_time", FechaHora.hhmmss());
            paramObject.put("card_token_id", "");
            paramObject.put("card_validation", "0");
            paramObject.put("card_mask", "");
            paramObject.put("payer_card_brand", "GC");
            paramObject.put("payer_card_number", replaceEncrypt(Memoria.crypTDES(Memoria.getOperacion().getNumberTarj())));
            paramObject.put("payer_card_date_exp", replaceEncrypt(Memoria.crypTDES(Memoria.getOperacion().getDateTarj())));
            paramObject.put("payer_card_cvc", replaceEncrypt(Memoria.crypTDES("111")));
            paramObject.put("payer_card_full_name", Memoria.getUser().getBuyer_first_name());
            paramObject.put("payer_card_doc_type", Memoria.getUser().getBuyer_doc_type());
            paramObject.put("payer_card_doc_number", Memoria.getUser().getBuyer_doc_number());
            paramObject.put("payer_card_email", Memoria.getUser().getBuyer_email());
            paramObject.put("payer_card_alias", Memoria.getOperacion().getAliasTarj());
            paramObject.put("payer_card_save", "1");
            paramObject.put("payer_card_favorite", "1");
            paramObject.put("payer_card_cuotas", "00");
            paramObject.put("payer_card_diferido", "0");
            paramObject.put("buyer_merchant_password", Memoria.getPasswordUser());
            paramObject.put("buyer_mobile_id", Memoria.getUser().getBuyer_mobile_id());
            paramObject.put("buyer_mobile_token_id", Memoria.getUser().getBuyer_mobile_token_id());
            paramObject.put("entry_mode_app", "A");
            paramObject.put("transaction_authorization_date", FechaHora.aaaammdd());
            paramObject.put("transaction_authorization_time", FechaHora.hhmmss());

            json_obj.put("Registra_Card", paramObject);

            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public String buildTramaListadoCard() {

        try {

            paramObject.put("api_login", Memoria.getUser().getApi_login());
            paramObject.put("api_key", Memoria.getUser().getApi_key());
            paramObject.put("merchant_code", Memoria.getUser().getMerchant_code());
            paramObject.put("buyer_token_id", Memoria.getUser().getBuyer_token_id());
            paramObject.put("transaction_merchant_code", "");
            paramObject.put("buyer_merchant_id", Memoria.getUser().getBuyer_merchant_id());
            paramObject.put("buyer_mobile_id", Memoria.getUser().getBuyer_mobile_id());
            paramObject.put("buyer_mobile_token_id", Memoria.getUser().getBuyer_mobile_token_id());
            paramObject.put("entry_mode_app", "A");
            paramObject.put("listado_card_date", FechaHora.aaaammdd());
            paramObject.put("listado_card_time", FechaHora.hhmmss());

            json_obj.put("Listado_Card", paramObject);
            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public String buildTramaEliminaCard(TarjetaInfoE tarj) {

        try {

            paramObject.put("api_login", Memoria.getUser().getApi_login());
            paramObject.put("api_key", Memoria.getUser().getApi_key());
            paramObject.put("merchant_code", Memoria.getUser().getMerchant_code());
            paramObject.put("transaction_language", "ES");
            paramObject.put("transaction_merchant_code", Memoria.getUser().getMerchant_code());
            paramObject.put("buyer_token_id", Memoria.getUser().getBuyer_token_id());
            paramObject.put("buyer_merchant_id", Memoria.getUser().getBuyer_merchant_id());
            paramObject.put("buyer_mobile_id", Memoria.getUser().getBuyer_mobile_id());
            paramObject.put("buyer_mobile_token_id", Memoria.getUser().getBuyer_mobile_token_id());
            paramObject.put("card_token_id", tarj.getCard_token_id());
            paramObject.put("buyer_merchant_password", Memoria.getPasswordUser());
            paramObject.put("entry_mode_app", "A");
            paramObject.put("elimina_token_card_date", FechaHora.aaaammdd());
            paramObject.put("elimina_token_card_time", FechaHora.hhmmss());

            json_obj.put("Elimina_Token_Card", paramObject);
            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

    public static String replaceEncrypt(String data) {

        return data.replace("%2B", "+").replace("%3D","=").replace("%2F","/").replace("\\","");
    }

    /************************ GESTOR DE TARJETAS ********************************/

    public String buildTramaConsultas(String kind, String card, String fecha_vcto) {


        try {

            String transaccionCodigoAutogenerado = System.currentTimeMillis() + "";

            paramObject.put("CodEmisor", Constantes.COD_EMISOR);
            paramObject.put("CodUsuario", Constantes.COD_USUARIO);
            paramObject.put("NumTerminal", Constantes.NUM_TERMINAL);
            paramObject.put("NumReferencia", FechaHora.ammdd() + FechaHora.hhmmss());
            paramObject.put("NumTarjetaMonedero", card);
            paramObject.put("FechaExpiracion", fecha_vcto);
            paramObject.put("Comercio", Constantes.COMERCIO);
            paramObject.put("Moneda", Constantes.MONEDA);
            paramObject.put("FechaTxnTerminal", FechaHora.aaaammdd());
            paramObject.put("HoraTxnTerminal", FechaHora.hhmmss());
            paramObject.put("WSUsuario", Constantes.WS_USUARIO);
            paramObject.put("WSClave", Constantes.WS_CLAVE);
            paramObject.put("Reservado", "");

            json_obj.put(kind, paramObject);

            /**
             * data de prueba:
             * 9801950100006723
             * 9801950500001183
             * 02/18
             */

            paramObject = null;

        } catch (Exception e) {
            Soporte.dump("exception " + e.getLocalizedMessage());
        }

        return json_obj.toString();
    }

}
