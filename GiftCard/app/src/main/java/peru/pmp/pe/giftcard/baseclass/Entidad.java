/*
 * Entidad.java
 *
 * Created on 8 de octubre de 2003, 02:32 AM
 */
package peru.pmp.pe.giftcard.baseclass;

import android.content.ContentValues;
import android.database.Cursor;

import peru.pmp.pe.giftcard.libreria.Texto;
import peru.pmp.pe.giftcard.utilitarios.Soporte;

import java.io.Serializable;
import java.sql.Date;

/**
 * Clase base utilizada para la definicion de las entidades del sistema
 * @author Billy Vera Castellanos
 * @version 1.0
 */

public class Entidad implements Constantes, Cloneable, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8839314507710696401L;
	protected char _flag1;
    protected char _flag2;
	protected int _rowid;
	protected int _id;
    protected String _code;
    protected String _descripcion;
    protected String _tag;
    protected boolean _activo;
    protected boolean _refresh;
    protected Date _fcreated;
    protected Date _fupdated;
	protected String _usuario;
    
    /// Propiedades internas de manejo de datos
    protected String _dataraw;
    protected boolean _nuevo;

    public Entidad() {
    	_flag1 = FLG_NEW_MOVIL;
    	_flag2 = FLG_EMPTY;
    	_rowid = -1;
    	_id = 0;
        _code = "";
        _descripcion = "";
        _tag = "";
        _activo = true;
        _usuario = "";
        /// atributos
        _dataraw = "";
        _nuevo = true;
    }
    
    public Entidad(String code, String desc){
    	this(0, code, desc);
    }

    public Entidad(int id, String code, String desc){
    	this();
    	_id = id;
    	_code = code;
    	_descripcion = desc;
    }
    public Entidad(String code, String desc, String tag){
    	this(code, desc);
    	setTag(tag);
    }    
    public Entidad(String desc, boolean onoff){
    	this();
    	_descripcion = desc;
    	_activo = onoff;
    }

    public Entidad(int id, String desc, boolean onoff){
    	this();
    	_id = id;
    	_descripcion = desc;
    	_activo = onoff;
    }

    public Entidad(String rawdata){
        this();
        asignaDataFields(rawdata);
    }
	/******
	 * Metodo que asigna los valores del parametro en cada campo definido en la entidad
	 * @param String Valor de campos separado por la constante FIELD_DELIMITED
     * @return none
	 */
    public void asignaDataFields(String data){
		_nuevo = true;
    }
	/******
	 * Metodo que asigna los valores del parametro en cada campo definido en la entidad
	 * @param Cursor Objeto que representa el resultado de una fila de una tabla de base 
	 * de datos, se asignan el valor de las columnas a cada campo definido en la entidad 
     * @return none
	 */
    public void asignaDataFields(Cursor data){
		_nuevo = false;
    }
    
	/******
	 * Metodo que retorna el valor de los campos de la entidad separado por FIELD_DELIMITED.
	 * Solo es aplicable en plataforma J2ME.
     * @return String
	 */
    public String getRawData(){
        return _dataraw;
    }
    
    /***********************************
     * Metodo que debera retornar un array con los valores clave de la tabla,
     * utilizado para los procesos de busqueda y de actualizacion
     * @return String[]
     */
    public String[] getPKData(){
    	Soporte.dump(getClass().getName()+":getPKData no instanciado");
    	return null;
    }

    /***********************************
     * Metodo que debera retornar un objeto ContentValue con los valores de la tabla,
     * para la insercion o actualizacion de los datos.
     * @return ContentValues
     */
    public ContentValues getContentValues(){
    	Soporte.dump(getClass().getName()+":getContentValues no instanciado");
    	return null;
    }

    public int getId() {
		return _id;
	}
    public void setId(int _id) {
		this._id = _id;
	}
    public int getRowid() {
        return _rowid;
    }
    public void setRowid(int piRowid) {
        this._rowid = piRowid;
    }
	public String getCode() {
		return _code;
	}
	public void setCode(String _code) {
		this._code = _code;
	}
    public String getDescripcion() {
        return _descripcion; ///.toUpperCase();
    }
    public void setDescripcion(String sDescripcion) {
        this._descripcion = sDescripcion;
    }
    public void setTag(String sTag) {
        this._tag = sTag;
    }
    public String getTag() {
        return _tag;
    }
    public void setActivo(boolean _activo) {
		this._activo = _activo;
	}
    public boolean isActivo() {
		return _activo;
	}
    public void setRefresh(boolean _refresh) {
		this._refresh = _refresh;
	}
    public boolean isRefresh() {
		return _refresh;
	}
	public Date getFcreated() {
		return _fcreated;
	}
	public void setFcreated(Date _fcreated) {
		this._fcreated = _fcreated;
	}
	public Date getFupdated() {
		return _fupdated;
	}
	public void setFupdated(Date _fupdated) {
		this._fupdated = _fupdated;
	}
    public char getFlag1(){
        return _flag1;
    }
    public char getFlag2(){
        return _flag2;
    }
    public void setFlag1(char car){
        _flag1 = car;
    }
    public void setFlag2(char car){
        _flag2 = car;
    }
    public boolean isNuevo(){
        return _nuevo;
    }
	public String getUsuario() {
		return _usuario;
	}
	public void setUsuario(String _usuario) {
		this._usuario = _usuario;
	}
	public String nvl(String texto){
		return Texto.nvl(texto);
	}
	 /***********************************
     * Metodo que determina si el registro actual esta CERRADO es decir si el valor del 
     * campo <i>FLAG1</i> es <i>FLAG_NEW_MOVIL ó FLAG_UPD_MOVIL ó FLAG_TRANSMITIDO</i>.
     * @return boolean Retorna True si el registro esta cerrado. 
     */
	public boolean isRowClosed(){
		return _flag1 == FLG_NEW_MOVIL || _flag1 == FLG_UPD_MOVIL || _flag1 == FLG_TRANSMITIDO;
	}
    /***********************************
     * Metodo que determina si el registro actual esta TRANSMITIDO es decir si el valor del 
     * campo <i>FLAG1</i> es <i>FLAG_TRANSMITIDO</i>.
     * @return boolean Retorna True si el registro esta transmitido. 
     */
	public boolean isRowTransmitted(){
		return _flag1 == FLG_TRANSMITIDO;
	}

	public String getString(String texto){
		return Texto.codeDecode(texto);
	}
	
	@Override
	public String toString() {return getCode()+"="+ getDescripcion();}
	
    public Object clone(){
        Object obj=null;
        try{
            obj = super.clone();
        }catch (CloneNotSupportedException e){
        	Soporte.dump("Objeto clone no soportado:"+e);
        }
        return obj;
    }
    
}
