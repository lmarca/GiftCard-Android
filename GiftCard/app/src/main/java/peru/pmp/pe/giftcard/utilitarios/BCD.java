package peru.pmp.pe.giftcard.utilitarios;

import peru.pmp.pe.giftcard.libreria.Texto;

public class BCD {

	public static String byteArrayToStringHex(byte[] bites) {
		StringBuilder sb = new StringBuilder();
		for (byte b : bites)
			sb.append(String.format("%02x", b));
		System.out.println(sb.toString());
		// prints "FF 00 01 02 03 " }
		return sb.toString();
	}

	public static byte[] stringHexToByteArray(String hex) {
		ByteBuffer buffer = new ByteBuffer();
		hex = hex.replace(" ", "").replace("-", "").replace(":", "");
		for (int i = 0; i < hex.length() - 1; i += 2) {
			String output = hex.substring(i, (i + 2));
			// convert hex to decimal
			int decimal = Integer.parseInt(output, 16);
			buffer.write((byte)decimal);
		}
		return buffer.toByteArray();
	}

    public static String stringHexToStringByte(String hex)
    {
    	byte[] bites = stringHexToByteArray(hex);
    	
    	return Texto.ByteArrayToStr(bites);
    }
	
	static final String HEXES = "0123456789ABCDEF";

	public static String getHex(byte[] raw) {
		return getHex(raw, 0, raw.length);
	}
	
	public static String getHex(byte[] raw, int offset, int len) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(2 * len);
		byte b;
		for (int z=offset; z<offset+len; z++){
			b = raw[z];
			if(hex.length()>0)
				hex.append("-");
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(
					HEXES.charAt((b & 0x0F)));
		}
		return hex.toString();
	}

	/*
	 * long number to bcd byte array e.g. 123 --> (0000) 0001 0010 0011 e.g. 12
	 * ---> 0001 0010
	 */
	public static byte[] DecToBCDArray(long num) {
		int digits = 0;

		long temp = num;
		while (temp != 0) {
			digits++;
			temp /= 10;
		}

		int byteLen = digits % 2 == 0 ? digits / 2 : (digits + 1) / 2;
		boolean isOdd = digits % 2 != 0;

		byte bcd[] = new byte[byteLen];

		for (int i = 0; i < digits; i++) {
			byte tmp = (byte) (num % 10);

			if (i == digits - 1 && isOdd)
				bcd[i / 2] = tmp;
			else if (i % 2 == 0)
				bcd[i / 2] = tmp;
			else {
				byte foo = (byte) (tmp << 4);
				bcd[i / 2] |= foo;
			}

			num /= 10;
		}

		for (int i = 0; i < byteLen / 2; i++) {
			byte tmp = bcd[i];
			bcd[i] = bcd[byteLen - i - 1];
			bcd[byteLen - i - 1] = tmp;
		}

		return bcd;
	}

	/*
	 * binario to ascii --> byte 211 (D3) return => String Hex 'D3'   
	 * 
	 */
	public static String BCDtoString(byte bcd) {
		StringBuffer sb = new StringBuffer();
		byte high = (byte) (bcd & 0xf0);
		high >>>= (byte) 4;
		high = (byte) (high & 0x0f);
		byte low = (byte) (bcd & 0x0f);
		sb.append(high);
		sb.append(low);
		return sb.toString();
	}

	/*
	 * array binario to ascii string --> byte {D3,FF,10,A1} return => String Hex 'D3FF10A1'
	 * 
	 */
	public static String BCDtoString(byte[] bcd) {

		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < bcd.length; i++)
			sb.append(BCDtoString(bcd[i]));

		return sb.toString();
	}

	public String convertStringToHex(String str) {

		char[] chars = str.toCharArray();

		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			hex.append(Integer.toHexString((int) chars[i]));
		}

		return hex.toString();
	}

	public static String convertHexToString(String hex) {

		StringBuilder sb = new StringBuilder();
		StringBuilder temp = new StringBuilder();

		// 49204c6f7665204a617661 split into two characters 49, 20, 4c...
		for (int i = 0; i < hex.length() - 1; i += 2) {

			// grab the hex in pairs
			String output = hex.substring(i, (i + 2));
			// convert hex to decimal
			int decimal = Integer.parseInt(output, 16);
			// convert the decimal to character
			sb.append((char) decimal);

			temp.append(decimal);
		}
		System.out.println("Decimal : " + temp.toString());

		return sb.toString();
	}

    public static void printHex(byte[] bytes)
    {
        for (int j=0; j<bytes.length; j++)
            System.out.format("%02X ", bytes[j]);
        System.out.println();
    }
    
}