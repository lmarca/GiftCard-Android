package peru.pmp.pe.giftcard.networking;

import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import peru.pmp.pe.giftcard.baseclass.Constantes;
import peru.pmp.pe.giftcard.entidades.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Admin on 1/02/18.
 */

public interface GoogleApi {

    interface RestApi {

        final OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        public static final Retrofit RETROFIT = new Retrofit.Builder()
                .baseUrl(Constantes.PRE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        @Headers("Content-Type: application/json")
        @POST("Afiliacion_Usuario")
        Call<JsonObject> wsGetAfiliacionUsuario(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Autentica_Usuario")
        Call<JsonObject> wsGetAutenticaUsuario(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Afiliacion_Alta")
        Call<JsonObject> wsGetAfiliacionAlta(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Afiliacion_Codigo_Activacion")
        Call<JsonObject> wsGetAfiliacionCodeActivacion(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Olvido_Password")
        Call<JsonObject> wsGetOlvidoPassword(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Registra_Card")
        Call<JsonObject> wsGetRegistraCard(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Listado_Card")
        Call<JsonObject> wsGetListadoCard(@Body String body);

        @Headers("Content-Type: application/json")
        @POST("Elimina_Token_Card")
        Call<JsonObject> wsGetEliminaCard(@Body String body);

    }

}
