package peru.pmp.pe.giftcard.libreria;

import android.os.Environment;

import peru.pmp.pe.giftcard.utilitarios.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * @author Billy Vera Castellanos
 * @version 1.0
 */
public class Archivo {

	public static final String SEPARADOR = File.separator;
	public static final String LABEL_ENTER = "{char(10)}";
	public static final String LABEL_TAB = "{char(9)}";
	public static final byte[] RETORNO = { 13, 10 };
	public static final byte[] TAB = { 9 };

    public static String getRootDefault() {
    	/// Importante: La ruta Absoluta está restringido en algunos modelos Android,
    	///             cuando la app es instalada desde la tienda en linea.
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
    	return root;
    }    
	
	public static boolean vector2TXT(Vector<String> vector, String fileDestino) {
		return vector2TXT(vector, fileDestino, false);
	}

	public static boolean vector2TXT(Vector<String> vector, String fileDestino,
                                     boolean append) {
		boolean ok = false;
		ByteArrayOutputStream bOut = null;
		FileOutputStream fos = null;
		if (vector != null) {
			ok = crearArchivo(fileDestino, !append);
			if (ok) {
				bOut = new ByteArrayOutputStream();
				// Se almacena la informacion del vector en memoria y luego
				// se vuelca al fileOutput.-
				try {
					for (Object o : vector) {
						bOut.write(((String) o).getBytes());
						bOut.write(Archivo.RETORNO);
					}
					fos = new FileOutputStream(fileDestino, append);
					bOut.writeTo(fos);
					// Common.esperar();
				} catch (IOException io) {
					Soporte.err("Archivo.vector2TXT:" + io.toString());
					ok = false;
				} finally {
					try {
						bOut.close();
						bOut = null;
						if (fos != null) {
							fos.close();
						}
						fos = null;
					} catch (IOException io) {
					}
				}
			}
		}
		bOut = null;
		fos = null;
		return ok;
	}

	public static Vector<String> TXT2vector(String rutnomTXT) {
		return TXT2vector(rutnomTXT, 1);
	}

	public static Vector<String> TXT2vector(String rutnomTXT, int iPosDesde) {
		Vector<String> vector = new Vector<String>();
		File[] fi = getFiles(rutnomTXT);
		File fil;
		CharArrayWriter cOut = new CharArrayWriter();
		BufferedReader input = null;
		int iPosCur;
		for (int u = 0; u < fi.length; u++) {
			fil = fi[u];
			try {
				InputStreamReader is = new InputStreamReader(
						new FileInputStream(fil), "UTF-8"); /// "ISO-8859-1");
				input = new BufferedReader(is, 8192);
				// Leer el archivo por bloques de 8K.-
				char[] rChars = new char[8192];
				int iCount = 0;
				while (true) {
					iCount = input.read(rChars);
					if (iCount == -1) {
						break;
					}
					cOut.write(rChars, 0, iCount);
				}
				// Common.esperar();
				rChars = null;
				input.close();
				input = null;
				// Tenemos todo en memoria. Ahora tenemos que pasarlo al vector.
				input = new BufferedReader(new CharArrayReader(
						cOut.toCharArray()), 8192);
				String lin = "";
				iPosCur = 1;
				while ((lin = input.readLine()) != null) {
					if (iPosCur >= iPosDesde) {
						vector.add(lin);
					}
					iPosCur++;
				}
			} catch (IOException e) {
				Soporte.err("Archivo.vector2TXT:" + e);
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException io) {
					}
					input = null;
				}
			}
		}
		fi = null;
		fil = null;
		cOut = null;
		input = null;
		return vector;
	}

	public static Vector<String> TXT2vector(InputStream is) {
		Vector<String> vector = new Vector<String>();
		CharArrayWriter cOut = new CharArrayWriter();
		BufferedReader input = null;
		try {
			int i = is.read();
			while (i != -1) {
				cOut.write(i);
				i = is.read();
			}
			is.close();
			input = null;
			// Tenemos todo en memoria. Ahora tenemos que pasarlo al vector.
			input = new BufferedReader(new CharArrayReader(
					cOut.toCharArray()), 8192);
			String lin = "";
			while ((lin = input.readLine()) != null) {
				vector.add(lin);
			}
		} catch (IOException e) {
			Soporte.err("Archivo.TXT2vector:" + e);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException io) {
				}
				input = null;
			}
		}
		cOut = null;
		input = null;
		return vector;
	}
	
	public static boolean string2TXT(String texto, String fileDestino,
                                     boolean append) {
		if(Soporte.isNullOrE(texto))
			return false;
		return string2TXT(texto.getBytes(), fileDestino, append);
	}
	public static boolean string2TXT(byte[] data, String fileDestino,
			boolean append) {
		// Soporte.dump("Archivo.TXT2string: Escribiendo = " + fileDestino);
		boolean ok = true;
		ByteArrayOutputStream bOut = null;
		FileOutputStream fos = null;
		if (data != null) {
			ok = crearArchivo(fileDestino, !append);
			if (ok) {
				// Se almacena la informacion del vector en memoria y luego
				// se vuelca al fileOutput.-
				bOut = new ByteArrayOutputStream();
				try {
					bOut.write(data);
					// / bOut.write(Archivo.RETORNO); HAY PROBLEMAS AL MOMENTO
					// DE HACER COMPARACIONES POR EL RETORNO DE CARRO
					fos = new FileOutputStream(fileDestino, append);
					bOut.writeTo(fos);
					// Common.esperar();
				} catch (IOException io) {
					Soporte.err("Archivo.string2TXT:" + io);
					ok = false;
				} finally {
					try {
						bOut.close();
						bOut = null;
						if (fos != null) {
							fos.close();
						}
						fos = null;
					} catch (IOException io) {
					}
				}
			}
		}
		bOut = null;
		fos = null;
		return ok;
	}

	public static String TXT2string(String rutnomTXT) {
		// Soporte.dump("Archivo.TXT2string: Leyendo de = " + rutnomTXT);
		StringBuffer buffer = new StringBuffer();
		File[] fi = getFiles(rutnomTXT);
		File fil;
		BufferedReader input = null;
		for (int u = 0; u < fi.length; u++) {
			fil = fi[u];
			try {
				InputStreamReader is = new InputStreamReader(
						new FileInputStream(fil), "ISO-8859-1");
				input = new BufferedReader(is, 8192);
				// Leer el archivo por bloques de 8K.-
				char[] rChars = new char[8192];
				int iCount = 0;
				while (true) {
					iCount = input.read(rChars);
					if (iCount == -1) {
						break;
					}
					buffer.append(rChars, 0, iCount);
				}
				rChars = null;
			} catch (IOException e) {
				Soporte.err("Archivo.TXT2string:" + e.getMessage());
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException io) {
					}
					input = null;
				}
			}
		}
		fi = null;
		fil = null;
		return buffer.toString();
	}
	public static byte[] getBytes(String nomfile) {
		FileInputStream fis = null;
		byte[] bitesF = new byte[0];
		try {
			File file = new File(nomfile);
			fis = new FileInputStream(file);
			bitesF = new byte[(int) file.length()];
			fis.read(bitesF);
			file = null;
		} catch (IOException io) {
			Soporte.err("Archivo.getBytes:" + io.getMessage());
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
				fis = null;
			} catch (IOException io) {
			}
		}
		return bitesF;
	}
	/* metodo cambiado por uno mas simple
	public static byte[] getBytes(String rutnomFILE) {
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		FileInputStream fi = getFileInput(rutnomFILE);
		if(fi!=null){
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(fi, "utf8"));
				char[] buffer = new char[8192];
				int len = 0;
				while (true) {
					len = br.read(buffer);
					if (len == -1) 
						break;
					Soporte.dump(new String(buffer, 0, len));
					bOut.write(Texto.StrToByteArray(new String(buffer, 0, len)));
				}
				buffer = null;
			} catch (IOException e) {
				Soporte.err("Archivo.getBytes:" + e.getMessage());
			} finally {
				if (fi != null) {
					try {
						fi.close();
					} catch (IOException io) {
					}
					fi = null;
				}
			}
		}
		return bOut.toByteArray();
	}*/
	
	public static boolean crearArchivo(String ruta, String nombre) {
		if (!ruta.endsWith(SEPARADOR)) {
			ruta += SEPARADOR;
		}
		return (crearArchivo(ruta + nombre));
	}

	public static boolean crearArchivo(String rutafile) {
		return (crearArchivo(rutafile, false));
	}

	public static boolean crearArchivo(String ruta, String nombre,
                                       boolean crearloSioSi) {
		if (!ruta.endsWith(SEPARADOR)) {
			ruta = ruta + SEPARADOR;
		}
		return (crearArchivo(ruta + nombre, crearloSioSi));
	}

	/*****
	 * crearArchivo : Si no existe el archivo lo crea, si el parametro
	 * crearloSioSi es verdadero, elimina el que existe -en el caso que exista-
	 * y lo vuelve a crear.
	 */
	public static boolean crearArchivo(String rutafile, boolean crearloSioSi) {
		File arch = new File(rutafile);
		crearCarpeta(arch.getParent());
		if (crearloSioSi) {
			if (arch.exists()) {
				arch.delete();
			}
		}
		if (!arch.exists()) {
			try {
				arch.createNewFile();
			} catch (IOException e) {
				Soporte.err("Archivo.crearArchivo:" + e.getMessage());
				return (false);
			}
		}
		return (true);
	}

	public static void crearCarpeta(String ruta) {
		if (ruta == null) {
			return;
		}
		File arch = new File(ruta);
		if (!arch.exists()) {
			try {
				Soporte.dump("Creando carpeta " + ruta);
				arch.mkdirs();
			} catch (SecurityException s) {
				Soporte.err("Archivo.crearCarpeta:" + s.getMessage());
			}
		}
	}

	public static void crearCarpetaDeArchivo(String rutafile) {
		File arch = new File(rutafile);
		crearCarpeta(arch.getParent());
		arch = null;
	}

	public static boolean existeFile(String ruta_arch) {
		if(Soporte.isNullOrE(ruta_arch))
			return false;
		File file = new File(ruta_arch);
		String sCarpeta = file.getParent();
		if (sCarpeta == null) {
			sCarpeta = Archivo.SEPARADOR;
		}
		String sNomArch = file.getName();
		if (file.isDirectory()) {
			return false;
		}
		File fCarpeta = new File(sCarpeta);
		String[] arqivos;
		boolean auxb = false;
		arqivos = fCarpeta.list(new ArchivoFilter(sNomArch));
		if (arqivos == null) {
			arqivos = new String[0];
		}
		auxb = arqivos.length > 0;
		file = null;
		sCarpeta = null;
		sNomArch = null;
		fCarpeta = null;
		arqivos = null;
		return auxb;
	}

	public static boolean existeFile(String ruta, String arch) {
		if (!ruta.endsWith(SEPARADOR)) {
			ruta += SEPARADOR;
		}
		return (existeFile(ruta + arch));
	}
	
	public static boolean escribirArchivo(String rutafile, byte[] buffer,
                                          boolean append) {
		boolean ok = true;
		FileOutputStream fout = null;
		try{
			fout = streamArchivo(rutafile, append);
			fout.write(buffer);
		}catch(Exception e){
			Soporte.dump("escribirArchivo:"+e.getMessage());
			ok = false;
		}finally{
			if(fout!=null){
				try{fout.close();}catch(Exception e){}
			}
		}
		return ok;
	}
	public static FileInputStream getFileInput(String rutafile,
                                               boolean crearSinoExiste) {
		FileInputStream fileInputStream = null;
		boolean existef = existeFile(rutafile);
		if (existef || (!existef && crearSinoExiste && crearArchivo(rutafile))) {
			try {
				fileInputStream = new FileInputStream(rutafile);
			} catch (IOException io) {
				System.err
						.println("Archivo.abrirArchivo(String,boolean) " + io);
			}
		}
		return (fileInputStream);
	}

	public static FileInputStream getFileInput(String ruta_arch) {
		return (getFileInput(ruta_arch, false));
	}

	public static FileInputStream getFileInput(String ruta, String arch) {
		return (getFileInput(ruta, arch, false));
	}

	public static FileInputStream getFileInput(String ruta, String arch,
                                               boolean crearSinoExiste) {
		if (!ruta.endsWith(SEPARADOR)) {
			ruta += SEPARADOR;
		}
		return (getFileInput(ruta + arch, crearSinoExiste));
	}

	public static FileOutputStream streamArchivo(String rutafile) {
		return (streamArchivo(rutafile, false));
	}

	public static FileOutputStream streamArchivo(String rutafile, boolean append) {
		FileOutputStream fileOutputStream = null;
		rutafile = assigNomenclatura(rutafile);
		if (crearArchivo(rutafile, !append)) {
			try {
				fileOutputStream = new FileOutputStream(rutafile, append);
			} catch (IOException io) {
				System.err.println("Archivo.streamArchivo(String) " + io);
			}
		}
		return (fileOutputStream);
	}

	public static boolean eliminarFiles(String ruta_arch) {
		File file = new File(ruta_arch);
		String sCarpeta = file.getParent();
		if (sCarpeta == null) {
			sCarpeta = Archivo.SEPARADOR;
		}
		String sNomArch = file.getName();
		File fCarpeta = new File(sCarpeta);
		String[] arqivos;
		boolean auxb = false;
		arqivos = fCarpeta.list(new ArchivoFilter(sNomArch));
		if (arqivos == null) {
			arqivos = new String[0];
		}
		for (int u = 0; u < arqivos.length; u++) {
			file = new File(sCarpeta, arqivos[u]);
			if (file.exists()) {
				try {
					file.delete();
					auxb = true;
				} catch (Exception e) {
					Soporte.err("Archivo.eliminarFiles:" + e.getMessage());
				}
			}
		}
		file = null;
		sCarpeta = null;
		sNomArch = null;
		fCarpeta = null;
		arqivos = null;
		return auxb;
	}

	public static boolean eliminarFile(String ruta_arch) {
		File file = new File(ruta_arch);
		boolean auxb = false;
		if (file.exists()) {
			try {
				file.delete();
				auxb = true;
			} catch (Exception e) {
				Soporte.err("Archivo.eliminarFile:" + e.getMessage());
			}
		}
		file = null;
		return auxb;
	}

	public static boolean eliminarFile(String ruta, String arch) {
		if (!ruta.endsWith(SEPARADOR)) {
			ruta += SEPARADOR;
		}
		return (eliminarFile(ruta + arch));
	}

	public static boolean eliminarFiles(String ruta, String arch) {
		if (!ruta.endsWith(SEPARADOR)) {
			ruta += SEPARADOR;
		}
		return (eliminarFiles(ruta + arch));
	}

	public static int copiarByteArraytoFile(ByteArrayOutputStream biteOut,
			String sDestino) {
		return copiarByteArraytoFile(biteOut, new File(sDestino), false);
	}

	/***
	 * Esta Funcion copiara la informacion contenida en el ByteArrayOtputStream
	 * en el archivo File destino solo cuando la informacion contenida en el
	 * archivo File difiera con el contenido del origen o en su defecto no
	 * exista el destino.
	 */
	public static int copiarByteArraytoFile(ByteArrayOutputStream biteOut,
                                            File fDestino, boolean bCopiarSioSi) {
		int iOK = 0; // -1:error, 0:no copiado, 1:copiado
		boolean bExiste = false;
		boolean bCopiar;

		// if(!fDestino.getParentFile().canWrite())
		// return -1;
		bExiste = fDestino.exists();
		bCopiar = false;
		if (!bExiste || bCopiarSioSi) {
			bCopiar = true;
		} else {
			bCopiar = fDestino.length() != biteOut.size(); // size
			if (!bCopiar) {
				bCopiar = !compararBytes(biteOut.toByteArray(), fDestino);
			}
		}
		if (bCopiar) {
			// El 2do.parametro 'true', elimina el archivo previo.
			Archivo.crearArchivo(fDestino.getPath(), true);
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(fDestino);
				biteOut.writeTo(fos);
				iOK = 1;
			} catch (IOException io) {
				Soporte.err("Archivo.copiarByteArraytoFile:" + io.getMessage());
				iOK = -1;
			} finally {
				try {
					if (fos != null) {
						fos.close();
					}
					fos = null;
				} catch (IOException io) {
				}
			}
		}
		return (iOK);
	}

	public static boolean copiarBytes(String rutarchSource, String rutarchTarget) {
		return (copiarBytes(rutarchSource, rutarchTarget, false, false));
	}

	public static boolean copiarBytes(String rutarchSource,
                                      String rutarchTarget, boolean copiarSolosiNoExiste, boolean bAppend) {
		boolean copiado = false;
		File inputFile = new File(rutarchSource);
		File outputFile = new File(rutarchTarget);
		FileInputStream in = null;
		FileOutputStream out = null;
		boolean noCopiar = false;
		boolean existeTarget = existeFile(rutarchTarget);
		crearCarpeta(outputFile.getParent());
		if (copiarSolosiNoExiste) {
			noCopiar = existeTarget;
		} else {
			if (existeTarget && !bAppend) {
				eliminarFile(rutarchTarget);
			}
		}
		if (!noCopiar) {
			try {
				in = new FileInputStream(inputFile);
				out = new FileOutputStream(outputFile, bAppend);
				byte[] bites = new byte[8000];
				int iRead;
				while ((iRead = in.read(bites)) != -1) {
					out.write(bites, 0, iRead);
				}
				copiado = true;
			} catch (IOException io) {
				Soporte.err("Archivo.copiarBytes:" + io.getMessage());
			} finally {
				try {
					if (in != null) {
						in.close();
					}
					if (out != null) {
						out.close();
					}
				} catch (IOException io) {
					Soporte.err("Archivo.copiarBytes:" + io.getMessage());
				}
			}
		}
		return (copiado);
	}

	public static boolean copiarBytesTo(String rutarchSource,
			String folderTarget) {
		return (copiarBytesTo(rutarchSource, folderTarget, false));
	}

	public static boolean copiarBytesTo(String rutarchSource,
                                        String folderTarget, boolean copiarSolosiNoExiste) {
		boolean copiado = false;
		File inputFile = new File(rutarchSource);
		File outputFile = null;
		FileInputStream in = null;
		FileOutputStream out = null;
		boolean noCopiar = false;
		boolean existeTarget = false;
		String rutarchTarget = null;
		crearCarpeta(folderTarget);
		rutarchTarget = folderTarget + Archivo.SEPARADOR + inputFile.getName();
		outputFile = new File(rutarchTarget);
		existeTarget = existeFile(rutarchTarget);
		if (copiarSolosiNoExiste) {
			noCopiar = existeTarget;
		} else {
			if (existeTarget) {
				eliminarFile(rutarchTarget);
			}
		}
		if (!noCopiar) {
			try {
				in = new FileInputStream(inputFile);
				out = new FileOutputStream(outputFile);
				byte[] bites = new byte[8000];
				int iRead;
				while ((iRead = in.read(bites)) != -1) {
					out.write(bites, 0, iRead);
				}
				copiado = true;
			} catch (IOException ioe) {
				Soporte.err("Archivo.copiarBytesTo:" + ioe.getMessage());
			} finally {
				try {
					if (in != null) {
						in.close();
					}
					if (out != null) {
						out.close();
					}
				} catch (IOException ioe) {
					Soporte.err("Archivo.copiarBytesTo:" + ioe.getMessage());
				}
			}
		}
		return (copiado);
	}

	public static void copia(String ficheroOriginal, String ficheroCopia) {
		try {
			// Se abre el fichero original para lectura
			FileInputStream fileInput = new FileInputStream(ficheroOriginal);
			BufferedInputStream bufferedInput = new BufferedInputStream(
					fileInput);

			// Se abre el fichero donde se harÃ¡ la copia
			FileOutputStream fileOutput = new FileOutputStream(ficheroCopia);
			BufferedOutputStream bufferedOutput = new BufferedOutputStream(
					fileOutput);

			// Bucle para leer de un fichero y escribir en el otro.
			byte[] array = new byte[1000];
			int leidos = bufferedInput.read(array);
			while (leidos > 0) {
				bufferedOutput.write(array, 0, leidos);
				leidos = bufferedInput.read(array);
			}

			// Cierre de los ficheros
			bufferedInput.close();
			bufferedOutput.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*****
	 * getFileImagen : Devuelve un objeto File del archivo indicado solo que con
	 * una carpeta de subnivel adicionado Utilizado para el proceso de
	 * generacion de archivos de imagenes de las sentencias SQL.
	 */
	public static File getFileImagen(File fArchSource, String sCarpeta) {
		File fImg = null;
		String sPath = fArchSource.getParent() + Archivo.SEPARADOR + sCarpeta
				+ Archivo.SEPARADOR;
		String sNomA = fArchSource.getName();
		fImg = new File(sPath + sNomA);
		return fImg;
	}

	public static boolean copiarBytesToHist(File fArchSource) {
		return copiarBytesToHist(fArchSource, null);
	}

	public static String getNewNameFile_AMMDDHHMMSSML(File file) {
		return getNewNameFileAMMDDHHMMSSML(file, null);
	}

	public static String getNewNameFileAMMDDHHMMSSML(File file, String sFolder) {
		String snewname;
		if (sFolder == null) {
			sFolder = "";
		}
		String sPath = file.getParent() + Archivo.SEPARADOR + sFolder
				+ Archivo.SEPARADOR;
		String sNomA = file.getName();
		String sNom8 = "";
		String sExte = "";
		StringTokenizer st = new StringTokenizer(sNomA, ".");
		if (st.countTokens() > 1) {
			sNom8 = st.nextToken();
			sExte = st.nextToken();
		} else {
			sNom8 = sNomA;
		}
		sNom8 += "_" + FechaHora.ammddhhmmssml();
		if (sExte.length() > 0) {
			sNom8 += "." + sExte;
		}
		snewname = sPath + sNom8;
		return snewname;
	}

	public static String nomenclaturaFile(String rutaUsr) {
		String fecha = FechaHora.ammdd();
		String hora = FechaHora.hhmmss();
		String idunico = FechaHora.ammddhhmmssml();

		rutaUsr = Texto.stuff(rutaUsr, FechaHora.NOMENCLATURA_FECHA, fecha);
		rutaUsr = Texto.stuff(rutaUsr, FechaHora.NOMENCLATURA_HORA, hora);
		rutaUsr = Texto.stuff(rutaUsr, FechaHora.NOMENCLATURA_FECHAHORA, fecha
				+ hora);
		rutaUsr = Texto.stuff(rutaUsr, FechaHora.NOMENCLATURA_IDUNICO,
				idunico);
		return rutaUsr;
	}

	public static boolean copiarBytesToHist(File file, String sCarpeta) {
		boolean copiado = true;
		if (sCarpeta == null
				|| (sCarpeta != null && sCarpeta.trim().equals(""))) {
			sCarpeta = "his";
		}
		String snewPath = getNewNameFileAMMDDHHMMSSML(file, sCarpeta);
		copiado = copiarBytes(file.getAbsolutePath(), snewPath);
		return copiado;
	}

	public static File[] getFiles(String ruta_arch) {
		File[] afiles = new File[0];
		File file = new File(ruta_arch);
		String sCarpeta = file.getParent();
		if (sCarpeta == null) {
			sCarpeta = Archivo.SEPARADOR;
		}
		String sNomArch = file.getName();
		File fCarpeta = new File(sCarpeta);
		File[] arqivos;
		// log.Visor.sistema("Archivo.getFiles: ruta_arch='"+ruta_arch+"'");
		arqivos = fCarpeta.listFiles(new ArchivoFilter(sNomArch));
		if (arqivos != null) {
			int cont = 0;
			for (int u = 0; u < arqivos.length; u++) {
				file = arqivos[u];
				if (file.isFile() && file.length() > 0) {
					cont++;
				}
			}
			// log.Visor.sistema("Archivo.getFiles: encontrados="+cont+" ruta_arch='"+ruta_arch+"'");
			afiles = new File[cont];
			cont = 0;
			for (int u = 0; u < arqivos.length; u++) {
				file = arqivos[u];
				if (file.isFile() && file.length() > 0) {
					// log.Visor.sistema("Archivo.getFiles: file='"+file.getAbsolutePath()+"' ruta_arch='"+ruta_arch+"'");
					afiles[cont++] = file;
				}
			}
		}
		file = null;
		sCarpeta = null;
		sNomArch = null;
		fCarpeta = null;
		arqivos = null;
		return afiles;
	}

	public static File[] getFolders(String ruta_arch) {
		File[] afiles = new File[0];
		File file;
		File fCarpeta = new File(ruta_arch);
		File[] arqivos;
		arqivos = fCarpeta.listFiles();
		if (arqivos != null) {
			int cont = 0;
			for (int u = 0; u < arqivos.length; u++) {
				file = arqivos[u];
				if (file.isDirectory()) {
					cont++;
				}
			}
			afiles = new File[cont];
			cont = 0;
			for (int u = 0; u < arqivos.length; u++) {
				file = arqivos[u];
				if (file.isDirectory()) {
					afiles[cont++] = file;
				}
			}
		}
		file = null;
		fCarpeta = null;
		arqivos = null;
		return afiles;
	}

	public static boolean compararBytes(byte[] uno, byte[] dos) {
		boolean iguales = true;
		if (uno.length != dos.length) {
			iguales = false;
		} else {
			for (int u = 0; u < uno.length; u++) {
				if (uno[u] != dos[u]) {
					iguales = false;
					break;
				}
			}
		}
		return iguales;
	}

	public static boolean compararBytes(byte[] bitesR, File file2) {
		boolean iguales = true;
		FileInputStream fis = null;
		byte[] bitesF = null;
		try {
			fis = new FileInputStream(file2);
			bitesF = new byte[(int) file2.length()];
			fis.read(bitesF);
			iguales = compararBytes(bitesR, bitesF);
		} catch (IOException io) {
			Soporte.err("Archivo.compararBytes:" + io.getMessage());
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
				fis = null;
			} catch (IOException io) {
			}
			bitesR = null;
			bitesF = null;
		}
		return iguales;
	}

	public static char[] getcharArchivo(String rutafile) {
		return getcharArchivo(rutafile, -1);
	}

	public static char[] getcharArchivo(String rutafile, long maxSize) {
		CharArrayWriter cOut = new CharArrayWriter();
		File name = new File(rutafile);
		if (name.exists()) {
			if (name.isFile()) {
				BufferedReader input = null;
				try {
					if (maxSize == -1) {
						maxSize = name.length();
					} else {
						if (name.length() < maxSize) {
							maxSize = name.length();
						}
					}
					if (maxSize > 0) {
						input = new BufferedReader(new FileReader(name), 8192);
						// Leer el archivo por bloques de 8K.-
						char[] rChars = new char[8192];
						int iCount = 0;
						while (true) {
							iCount = input.read(rChars);
							if (iCount == -1) {
								break;
							}
							cOut.write(rChars, 0, iCount);
						}
					}
				} catch (IOException io) {
					Soporte.err("Archivo.getcharArchivo:" + io.getMessage());
				} finally {
					if (input != null) {
						// Common.esperar();
						try {
							input.close();
							input = null;
						} catch (IOException e) {
						}
					}
				}
			}
		}
		return cOut.toCharArray();
	}

	/***********************************************************************
	 * FUNCION: xCopy DESCRIPCION: Se encarga de copiar carpetas
	 * 
	 * PARAMETROS: Mensaje
	 * 
	 * RETORNO: - REVISION: -
	 ***********************************************************************/
	protected boolean xCopy(String rootO, String rootD) {
		boolean devolver = true;
		File forigen = new File(rootO);
		File fdestino = new File(rootD);
		if (!rootO.endsWith(File.separator)) {
			rootO = rootO + File.separator;
		}
		if (!rootD.endsWith(File.separator)) {
			rootD = rootD + File.separator;

		}
		if (forigen.exists()) {
			if (!fdestino.exists()) {
				crearCarpeta(rootD);
			}
			File[] archivos = forigen.listFiles();
			for (int p = 0; p < archivos.length; p++) {
				File f = archivos[p];
				String nomDestino = rootD + f.getName();
				System.out.println("Copiando a:" + nomDestino);
				if (f.isDirectory()) {
					crearCarpeta(nomDestino);
					xCopy(f.getAbsolutePath(), nomDestino);
				} else {
					// Copia_File(f, new File(nomDestino), true);
				}
			}
		}
		return (devolver);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	public static boolean saveFile(String ruta_archivo, InputStream input) {
		boolean ok = true;
		Archivo.crearArchivo(ruta_archivo, true);
		File fi = new File(ruta_archivo);
		RandomAccessFile ran = null;
		byte[] buf = new byte[8000];
		try {
			ran = new RandomAccessFile(fi, "rw");
			int leidos;
			while (true) {
				leidos = input.read(buf);
				if (leidos <= 0) {
					break;
				}
				ran.write(buf, 0, leidos);
			}
		} catch (Exception e) {
			Soporte.err("Archivo.saveFile:" + e.getMessage());
			ok = false;
		} finally {
			if (ran != null) {
				try {
					ran.close();
				} catch (Exception io) {
				}
				ran = null;
			}
		}
		return ok;
	}

	public static String getCurrentFolder() {
		File directory = new File(".");
		String folder = "";
		try {
			folder = directory.getCanonicalPath();
		} catch (Exception e) {
			Soporte.err("Archivo.getCurrentFolder:" + e.getMessage());
		}
		return folder;
	}

	public static String assigNomenclatura(String ruta_salida) {
		String fecha = FechaHora.ammdd();
		String hora = FechaHora.hhmmss();
		ruta_salida = Texto.stuff(ruta_salida, FechaHora.NOMENCLATURA_FECHA,
				fecha);
		ruta_salida = Texto.stuff(ruta_salida, FechaHora.NOMENCLATURA_HORA,
				hora);
		ruta_salida = Texto.stuff(ruta_salida,
				FechaHora.NOMENCLATURA_FECHAHORA, fecha + hora);
		ruta_salida = Texto.stuff(ruta_salida, FechaHora.NOMENCLATURA_IDUNICO,
				FechaHora.ammddhhmmssml());
		return ruta_salida;
	}

    public static boolean moverFile(String origen, String destino){
    	boolean exito = copiarBytes(origen, destino);
    	if(exito){
    		eliminarFile(origen);
    	}
        return exito;
    }
    public static boolean moverFileTo(File origen, String carpeta){
    	String destino = carpeta + "/" + getNameSinExt(origen.getName()) + "_" +
    					 FechaHora.ammddhhmmss() + "." + getExtension(origen.getName()); 
    	boolean exito = copiarBytes(origen.getAbsolutePath(), destino);
    	if(exito)
    		eliminarFile(origen.getAbsolutePath());
        return exito;
    }
    public static String getNameSinExt(String nomfile){
    	String newname = null;
    	if(nomfile.indexOf(".")!=-1)
    		newname = nomfile.substring(0, nomfile.indexOf("."));
    	else
    		newname = nomfile;
    	return newname;
    }

    public static String getExtension(String nomfile){
    	String newname = null;
    	if(nomfile.indexOf(".")!=-1)
    		newname = nomfile.substring(nomfile.indexOf(".")+1);
    	else
    		newname = nomfile;
    	return newname;
    }
    
}
