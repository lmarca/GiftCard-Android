package peru.pmp.pe.giftcard.libreria;

/**
 * <p>Empresa: MOVILOGIC PERU</p>
 * @author Billy Vera Castellanos
 * @version 1.0
 * JAVA Port by Robert Neild November 1999
 * PC1 Cipher Algorithm ( Pukall Cipher 1 )
 * By Alexander PUKALL 1991
 * free code no restriction to use
 * please include the name of the Author in the final software
 * the Key is 128 bits
 * Only the K zone change in the two routines
 * You can create a single routine with the two parts in it
 */

import peru.pmp.pe.giftcard.utilitarios.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Cipher {
    char ax,bx,cx,dx,si,tmp,x1a2,res,i,inter,cfc,cfd,compte;
    byte cle[]  = new byte [17];               // holds key
    char x1a0[] = new char [8];
    short c;
    
    public Cipher() {
    }

    private void pkfin() {
	int j;
        for (j=0;j<=16;j++) {
            cle[j]=0;
        }
        for (j=0;j<8;j++) {
            x1a0[j]=0;
        }
        ax=0;
        bx=0;
        cx=0;
        dx=0;
        si=0;
        tmp=0;
        x1a2=0;
        res=0;
        i=0;
        inter=0;
        cfc=0;
        cfd=0;
        compte=0;
        c=0;
    }

    private void assemble(){
        
        x1a0[0]= (char) ( ( cle[0]*256 )+ cle[1]);
        code();
        inter=res;
        
        x1a0[1]= (char) (x1a0[0] ^ ( (cle[2]*256) + cle[3]));
        code();
        inter=(char) (inter^res);
        
        x1a0[2]= (char) (x1a0[1] ^ ( (cle[4]*256) + cle[5]));
        code();
        inter=(char) (inter^res);
        
        x1a0[3]= (char) (x1a0[2] ^ ( (cle[6]*256) + cle[7] ));
        code();
        inter=(char) (inter^res);
        
        x1a0[4]= (char) (x1a0[3] ^ ( (cle[8]*256) + cle[9] ));
        code();
        inter=(char) (inter^res);
        
        x1a0[5]= (char) (x1a0[4] ^ ( (cle[10]*256) + cle[11] ));
        code();
        inter=(char) (inter^res);
        
        x1a0[6]= (char) (x1a0[5] ^ ( (cle[12]*256) + cle[13] ));
        code();
        inter=(char) (inter^res);
        
        x1a0[7]= (char) (x1a0[6] ^ ( (cle[14]*256) + cle[15] ));
        code();
        inter=(char) (inter^res);
        
        i=0;
    }
    
    private void code(){
        
        dx=(char) (x1a2+i);
        ax=x1a0[i];
        cx=0x015a;
        bx=0x4e35;
        
        tmp=ax;
        ax=si;
        si=tmp;
        
        tmp=ax;
        ax=dx;
        dx=tmp;
        
        if (ax!=0) {
            ax=(char) (ax*bx);
        }
        
        tmp=ax;
        ax=cx;
        cx=tmp;
        
        if (ax!=0)  {
            ax=(char) (ax*si);
            cx=(char) (ax+cx);
        }
        
        tmp=ax;
        ax=si;
        si=tmp;
        ax=(char) (ax*bx);
        dx=(char) (cx+dx);
        
        ax=(char) (ax+1);
        
        x1a2=dx;
        x1a0[i]=ax;
        
        res=(char) (ax^dx);
        i=(char) (i+1);
    }
    
    public int cryp(int c){
        assemble();
        cfc=(char) (inter>>8);
        cfd=(char) (inter&255); /* cfc^cfd = random byte */
        /* K ZONE !!!!!!!!!!!!! */
        /* here the mix of c and cle[compte] is before the encryption of c */
        for (compte=0;compte<=15;compte++){
            /* we mix the plaintext byte with the key */
            cle[compte]=(byte) (cle[compte]^c);
        }
        c = c ^ (cfc^cfd);
        return (c);  /* we write the crypted byte */
    }
    
    public int decryp(int c){
        assemble();
        cfc=(char) (inter>>8);
        cfd=(char) (inter&255); /* cfc^cfd = random byte */
        /* K ZONE !!!!!!!!!!!!! */
        /* here the mix of c and cle[compte] is before the encryption of c */
        c = c ^ (cfc^cfd);
        for (compte=0;compte<=15;compte++) {
            /* we mix the plaintext byte with the key */
            cle[compte]=(byte) (cle[compte]^ c);
        }
        return (c);  /* we write the crypted byte */
    }
    
    public char[] cryp(char[] bits, String pwd){
        return cryp(bits, 0, bits.length, pwd, false);
    }

    public char[] cryp(char[] bits, String pwd, boolean bNoFlags){
        return cryp(bits, 0, bits.length, pwd, bNoFlags);
    }

    public char[] cryp(char[] bits, int iPosI, int iSize, String pwd){
        return cryp(bits, iPosI, iSize, pwd, false);
    }
    
    public char[] cryp(char[] bits, int iPosI, int iSize, String pwd, boolean bNoFlags){
	int itemporal;
        char pkd, pke;
        if(bits.length==0)
                return new char[0];
        // Para evitar un indexofBounds cuando la cant. sea mayor que el total de elementos
        // del arreglo 'bits'
        if(iPosI+iSize>bits.length)
            iSize = bits.length - iPosI;
        
        int iTamRec = 0;
        if(bNoFlags)
            iTamRec = iSize*2;
        else
            iTamRec = (iSize-2)*2+2;

        if(iTamRec == 0)
            return new char[0];
        
        char[] bites = new char[iTamRec];
        
        int count = 0, k = 0;

        pkfin();
        // Importante invocar el seteo del arreglo de la pwd despues de 'pkfin()'
        setKey(pwd);

	for (count=0;count<iSize;count++) {
            itemporal = (0x000000FF & ((int)bits[count]));
            if(count==30){
                count=30;
            }
            c = (short)(byte)itemporal;
            if(bNoFlags || count>=2){
                assemble();
                cfc=(char) (inter>>8);
                cfd=(char) (inter&255); /* cfc^cfd = random byte */
                /* K ZONE !!!!!!!!!!!!! */
                /* here the mix of c and cle[compte] is before the encryption of c */
                for (compte=0;compte<=15;compte++){
                    /* we mix the plaintext byte with the key */
                    cle[compte]=(byte) (cle[compte]^c);
                }
                c = (short)(c ^ (cfc^cfd));
                
                pkd =(char)(c >> 4);
                pke =(char)(c & 15);

                bites[k] = (char)(0x61+pkd); k++;
                bites[k] = (char)(0x61+pke); k++;

            }else{
                
                bites[k] = (char)c; k++;

            }
        }
        return bites;
    }
    
    public char[] decryp(char[] bits, String pwd){
        return decryp(bits, 0, bits.length, pwd);
    }

    public char[] decryp(char[] bits, String pwd, boolean bNoFlags){
        return decryp(bits, 0, bits.length, pwd, bNoFlags);
    }

    public char[] decryp(char[] bits, int iPosI, int iSize, String pwd){
        return decryp(bits, iPosI, iSize, pwd, false);
    }
    
    public char[] decryp(char[] bits, int iPosI, int iSize, String pwd, boolean bNoFlags){
	char pkd, pke; char[] nulo=new char[1];
        // Para evitar un indexofBounds cuando la can sea mayor que el total de elementos
        // del arreglo 'bits'
        //nulo[0]=' ';
        if(bits==null || bits.length==0)
             return (nulo);
        if(iPosI+iSize>bits.length)
            iSize = bits.length - iPosI;

        int iTamRec = 0;
        if(bNoFlags)
            iTamRec = iSize/2;
        else
            iTamRec = (iSize-2)/2+2;
        
        char[] bites = new char[iTamRec];

        int count = 0, k = 0;
        
        pkfin();
        // Importante invocar el seteo del arreglo de la pwd despues de 'pkfin()'
        setKey(pwd);

        for (count=0;count<iTamRec;count++) {

            if(bNoFlags || count>=2){
                pkd = (char)bits[k]; k++;
                pke = (char)bits[k]; k++;

                pkd=(char)(pkd-0x61);
                pkd=(char)(pkd<<4);

                pke=(char)(pke-0x61);
                c=(short)(pkd+pke);

                assemble();
                cfc=(char)(inter>>8);
                cfd=(char)(inter&255);

                c = (short)(c ^ (cfc^cfd));

                for (compte=0;compte<=15;compte++){
                    cle[compte]=(byte)(cle[compte]^c);
                }
                if(c<0){
                    c+=256;
                }
                bites[count] = (char)c;
                
            }else{
                c = (short)bits[count];
                bites[count] = (char)c; k++;
            }
        }
        return bites;
    }
    
    // Algoritmo por cambio de orden de Tabla ASCII
    public static int[] getValoresAscii(){
        String ARCHIVO_FISICO = "criptoAscii.properties";
        Cipher cipher = new Cipher();
        int[] newBite = new int[256];
        FileInputStream fin;
        int iRead=-1;
        try{
            fin = Archivo.getFileInput(Archivo.getCurrentFolder()+
                        Archivo.SEPARADOR + 
                        ARCHIVO_FISICO);
            for(int u=0; u<newBite.length; u++){
                iRead = fin.read();
                if(iRead==-1) break;
                newBite[u] = cipher.decryp(iRead);
            }
            fin.close();
        }catch(IOException e){
            Soporte.dump("Cipher.getValoresAscii", e);
        } finally{fin = null; cipher=null;}
        return newBite;
    }
    
    public static int[] BitesCriptAscii(int[] bites) {
        int[] bitesU = new int[256];
        int iCar;
        int   u;
        for(u=0; u<256; u++){
            iCar = bites[u];
            bitesU[iCar]=u;
        }
        return bitesU;
    }
    
    public void setKey(String sKey){
        // Importante invocar el seteo del arreglo de la pwd despues de 'pkfin()'
        if(sKey!=null && sKey.length()==16){
            System.arraycopy(sKey.getBytes(),0,cle,0, Math.min(16,sKey.length()));
        }
    }

    public static byte[] crypAscii(byte[] baits, int iPosX, int iPosY) {
        int[] iAscii = Cipher.getValoresAscii();
        int iRead, iCurX = 0, iCurY = 1;
        for (int u = 0; u < baits.length; u++) {
            iRead = baits[u];
            if (iRead >= 0 && iRead <= 255) {
                iCurX++;
                if (iCurX >= iPosX && iCurY >= iPosY) {
                    iRead = iAscii[iRead];
                }
                if (iRead == 10 || iRead == 13) {
                    iCurY++;
                    iCurX = 0;
                }
                baits[u] = (byte) iRead;
            }
        }
        return baits;
    }

    public static byte[] decrypAscii(byte[] baits, int iPosX, int iPosY) {
        int[] iAscii = Cipher.getValoresAscii();
        int[] iAsciiU = Cipher.BitesCriptAscii(iAscii);
        int iRead, iCurX = 0, iCurY = 1;
        for (int u = 0; u < baits.length; u++) {
            iRead = baits[u];
            if (iRead >= 0 && iRead <= 255) {
                iCurX++;
                if (iCurX >= iPosX && iCurY >= iPosY) {
                    iRead = iAsciiU[iRead];
                }
                baits[u] = (byte) iRead;
                if (iRead == 10 || iRead == 13) {
                    iCurY++;
                    iCurX = 0;
                }
            }
        }
        return baits;
    }

    public static byte[] crypPukall(byte[] baits, String sKey) {
    	return crypPukall(baits, sKey, false);
    }
    public static byte[] crypPukall(byte[] baits, String sKey, boolean NoFlags) {
        String auxStr;
        byte[] bites = {};
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedReader bread = new BufferedReader(
                new InputStreamReader(new ByteArrayInputStream(baits)));
        Cipher cipher = new Cipher();
        cipher.setKey(sKey);
        char[] cars;
        try {
            while (true) {
                auxStr = bread.readLine();
                if (auxStr == null || auxStr.length() == 0) {
                    break;
                }
                if (baos.size() > 0) {
                    baos.write(Archivo.RETORNO);
                }
                cars = cipher.cryp(auxStr.toCharArray(), sKey, NoFlags);
                auxStr = new String(cars);
                baos.write(auxStr.getBytes());
            }
        } catch (IOException e) {
            Soporte.dump("Cipher.crypPukall", e);
        } finally {
            cipher = null;
            if (baos != null) {
                // Se obtiene el resultado.-
                if (baos.size() > 0) {
                    // Enter final
                    try {
                        baos.write(Archivo.RETORNO);
                    } catch (Exception e) {
                    }
                }
                bites = baos.toByteArray();
                try {
                    baos.close();
                } catch (IOException io) {
                }
                baos = null;
            }
        }
        return bites;
    }

    public static byte[] decrypPukall(byte[] baits, String sKey) {
    	return decrypPukall(baits, sKey, false);
    }
    public static byte[] decrypPukall(byte[] baits, String sKey, boolean NoFlags) {
        String auxStr;
        byte[] bites = {};
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedReader bread = new BufferedReader(
                new InputStreamReader(new ByteArrayInputStream(baits)));
        Cipher cipher = new Cipher();
        cipher.setKey(sKey);
        char[] cars;
        try {
            while (true) {
                auxStr = bread.readLine();
                if (auxStr == null || auxStr.length() == 0) {
                    break;
                }
                if (baos.size() > 0) {
                    baos.write(Archivo.RETORNO);
                }
                cars = cipher.decryp(auxStr.toCharArray(), sKey, NoFlags);
                auxStr = new String(cars);
                baos.write(auxStr.getBytes());
            }
        } catch (IOException e) {
            Soporte.dump("Cipher.decrypPukall", e);
        } finally {
            cipher = null;
            if (baos != null) {
                // Se obtiene el resultado.-
                bites = baos.toByteArray();
                try {
                    baos.close();
                } catch (IOException io) {
                }
                baos = null;
            }
        }
        return bites;
    }
    
    /******
     *  encriptarArchivo
     *  Parametros : File
     *  Descripcion : Si se encripta, se encripta todo el archivo completo
     */
    public static boolean crypPukallArchivo(File fOrig, String sKey) {
        Cipher cipher = new Cipher();
        FileInputStream fInRaw = null;
        FileWriter fOutRaw = null;
        BufferedReader bufIN = null;
        PrintWriter bufOUT = null;
        String auxStr;
        ///int ilenR;
        File fTemp = null, fWork = null;
        boolean ok = false;
        try {
            if (fOrig.exists() && fOrig.length() > 0) {
                fTemp = new File(fOrig.getAbsolutePath() + "_");
                fWork = new File(fOrig.getAbsolutePath() + "_w");
                Archivo.crearArchivo(fWork.getAbsolutePath());
                //Visor.sistema("encriptarArchivo.Paso 1");
                // Se copian los bytes a un archivo temporal
                Archivo.copiarBytes(fOrig.getAbsolutePath(), fTemp.getAbsolutePath());
                fInRaw = new FileInputStream(fOrig);
                bufIN = new BufferedReader(new InputStreamReader(fInRaw));
                fOutRaw = new FileWriter(fWork);
                bufOUT = new PrintWriter(new BufferedWriter(fOutRaw));
                //Visor.sistema("encriptarArchivo.Paso 2");
                while (true) {
                    auxStr = bufIN.readLine();
                    if (auxStr == null || auxStr.length() == 0) {
                        break;
                    }
                    ///ilenR = auxStr.length();
                    bufOUT.println(cipher.cryp(auxStr.toCharArray(), sKey));
                }
                //Visor.sistema("encriptarArchivo.Paso 3");
                bufIN.close();
                bufIN = null;
                bufOUT.close();
                bufOUT = null;
                fInRaw.close();
                fInRaw = null;
                fOutRaw.close();
                fOutRaw = null;
                ok = true;
                //Visor.sistema("encriptarArchivo.Paso 4");
            }
        } catch (IOException e) {
            ok = false;
            Soporte.dump("Cipher.crypPukallArchivo", e);
            //Visor.sistema("encriptarArchivo.Paso ****1");
            //System.out.println("Error de acceso de archivo:" + e);
        } finally {
            try {
                if (bufIN != null) {
                    bufIN.close();
                    bufIN = null;
                }
                if (bufOUT != null) {
                    bufOUT.close();
                    bufOUT = null;
                }
                if (fInRaw != null) {
                    fInRaw.close();
                    fInRaw = null;
                }
                if (fOutRaw != null) {
                    fOutRaw.close();
                    fOutRaw = null;
                }
                //Visor.sistema("descomprimirArchivoRLE.Paso 5");
                if (ok) {
                    // Si es OK : Delete 'fOrig', Rename 'fWork' y Delete 'fTemp'
                    try {
                    	Archivo.copiarBytes(fWork.getAbsolutePath(), fOrig.getAbsolutePath());
                    } catch (Exception e) {
                        Soporte.dump("Cipher.crypPukallArchivo", e);
                    }
                }
                Archivo.eliminarFile(fWork.getAbsolutePath());
                Archivo.eliminarFile(fTemp.getAbsolutePath());
            } catch (Exception e) {
                ok = false;
                Soporte.dump("Cipher.crypPukallArchivo", e);
                //Visor.sistema("descomprimirArchivoRLE.Paso ****3");
            }
        }
        return ok;
    }

    /******
     *  desencriptarArchivo
     *  Parametros : File
     *  Descripcion : Si se encripta, se encripta todo el archivo completo
     */
    public static boolean decrypPukallArchivo(File fOrig, String sKey) {
        Cipher cipher = new Cipher();
        FileInputStream fInRaw = null;
        FileWriter fOutRaw = null;
        BufferedReader bufIN = null;
        PrintWriter bufOUT = null;
        String auxStr;
        ///int ilenR;
        File fTemp = null, fWork = null;
        boolean ok = false;
        try {
            if (fOrig.exists() && fOrig.length() > 0) {
                fTemp = new File(fOrig.getAbsolutePath() + "_");
                fWork = new File(fOrig.getAbsolutePath() + "_w");
                Archivo.crearArchivo(fWork.getAbsolutePath());
                //Visor.sistema("encriptarArchivo.Paso 1");
                // Se copian los bytes a un archivo temporal
                Archivo.copiarBytes(fOrig.getAbsolutePath(), fTemp.getAbsolutePath());
                fInRaw = new FileInputStream(fOrig);
                bufIN = new BufferedReader(new InputStreamReader(fInRaw));
                fOutRaw = new FileWriter(fWork);
                bufOUT = new PrintWriter(new BufferedWriter(fOutRaw));
                //Visor.sistema("encriptarArchivo.Paso 2");
                while (true) {
                    auxStr = bufIN.readLine();
                    if (auxStr == null || auxStr.length() == 0) {
                        break;
                    }
                    ///ilenR = auxStr.length();
                    bufOUT.println(cipher.decryp(auxStr.toCharArray(), sKey));
                }
                //Visor.sistema("encriptarArchivo.Paso 3");
                bufIN.close();
                bufIN = null;
                bufOUT.close();
                bufOUT = null;
                fInRaw.close();
                fInRaw = null;
                fOutRaw.close();
                fOutRaw = null;
                ok = true;
                //Visor.sistema("encriptarArchivo.Paso 4");
            }
        } catch (IOException e) {
            ok = false;
            Soporte.dump("Cipher.decrypPukallArchivo", e);
            //Visor.sistema("encriptarArchivo.Paso ****1");
            //System.out.println("Error de acceso de archivo:" + e);
        } finally {
            try {
                if (bufIN != null) {
                    bufIN.close();
                    bufIN = null;
                }
                if (bufOUT != null) {
                    bufOUT.close();
                    bufOUT = null;
                }
                if (fInRaw != null) {
                    fInRaw.close();
                    fInRaw = null;
                }
                if (fOutRaw != null) {
                    fOutRaw.close();
                    fOutRaw = null;
                }
                //Visor.sistema("descomprimirArchivoRLE.Paso 5");
                if (ok) {
                    // Si es OK : Delete 'fOrig', Rename 'fWork' y Delete 'fTemp'
                    try {
                    	Archivo.copiarBytes(fWork.getAbsolutePath(), fOrig.getAbsolutePath());
                    } catch (Exception e) {
                        Soporte.dump("Cipher.decrypPukallArchivo", e);
                    }
                }
                Archivo.eliminarFile(fWork.getAbsolutePath());
                Archivo.eliminarFile(fTemp.getAbsolutePath());
            } catch (Exception e) {
                ok = false;
                Soporte.dump("Cipher.decrypPukallArchivo", e);
                //Visor.sistema("descomprimirArchivoRLE.Paso ****3");
            }
        }
        return ok;
    }

    /******
     *  crypAsciiArchivo
     *  Parametros : File
     *  Descripcion : Si se encripta, se encripta todo el archivo completo
     */
    public static boolean crypAsciiArchivo(File fOrig, int iPosX, int iPosY) {
        int[] iAscii = Cipher.getValoresAscii();
        FileInputStream fInRaw = null;
        FileOutputStream fOutRaw = null;
        int iRead, iCurX = 0, iCurY = 1;
        File fTemp = null, fWork = null;
        boolean ok = false;
        try {
            if (fOrig.exists() && fOrig.length() > 0) {
                fTemp = new File(fOrig.getAbsolutePath() + "_");
                fWork = new File(fOrig.getAbsolutePath() + "_w");
                Archivo.crearArchivo(fWork.getAbsolutePath());
                //Visor.sistema("encriptarArchivo.Paso 1");
                // Se copian los bytes a un archivo temporal
                Archivo.copiarBytes(fOrig.getAbsolutePath(), fTemp.getAbsolutePath());
                fInRaw = new FileInputStream(fOrig);
                fOutRaw = new FileOutputStream(fWork);
                //Visor.sistema("encriptarArchivo.Paso 2");
                while (true) {
                    iRead = fInRaw.read();
                    if (iRead == -1) {
                        break;
                    }
                    if (iRead >= 0 && iRead <= 255) {
                        iCurX++;
                        if (iCurX >= iPosX && iCurY >= iPosY) {
                            iRead = iAscii[iRead];
                        }
                        if (iRead == 10 || iRead == 13) {
                            iCurY++;
                            iCurX = 0;
                        }
                        fOutRaw.write(iRead);
                    }
                }
                //Visor.sistema("encriptarArchivo.Paso 3");
                fInRaw.close();
                fInRaw = null;
                fOutRaw.close();
                fOutRaw = null;
                ok = true;
                //Visor.sistema("encriptarArchivo.Paso 4");
            }
        } catch (IOException e) {
            ok = false;
            Soporte.dump("Cipher.crypAsciiArchivo", e);
            //Visor.sistema("encriptarArchivo.Paso ****1");
            //System.out.println("Error de acceso de archivo:" + e);
        } finally {
            try {
                if (fInRaw != null) {
                    fInRaw.close();
                    fInRaw = null;
                }
                if (fOutRaw != null) {
                    fOutRaw.close();
                    fOutRaw = null;
                }
                //Visor.sistema("descomprimirArchivoRLE.Paso 5");
                if (ok) {
                    // Si es OK : Delete 'fOrig', Rename 'fWork' y Delete 'fTemp'
                    try {
                    	Archivo.copiarBytes(fWork.getAbsolutePath(), fOrig.getAbsolutePath());
                    } catch (Exception e) {
                        Soporte.dump("Cipher.crypAsciiArchivo", e);
                    }
                }
                Archivo.eliminarFile(fWork.getAbsolutePath());
                Archivo.eliminarFile(fTemp.getAbsolutePath());
            } catch (Exception e) {
                ok = false;
                Soporte.dump("Cipher.crypAsciiArchivo", e);
                //Visor.sistema("descomprimirArchivoRLE.Paso ****3");
            }
        }
        return ok;
    }

    /******
     *  decrypAsciiArchivo
     *  Parametros : File
     *  Descripcion : Si se encripta, se encripta todo el archivo completo
     */
    public static boolean decrypAsciiArchivo(File fOrig, int iPosX, int iPosY) {
        int[] iAscii = Cipher.getValoresAscii();
        int[] iAsciiU = Cipher.BitesCriptAscii(iAscii);
        FileInputStream fInRaw = null;
        FileOutputStream fOutRaw = null;
        int iRead, iCurX = 0, iCurY = 1;
        File fTemp = null, fWork = null;
        boolean ok = false;
        try {
            if (fOrig.exists() && fOrig.length() > 0) {
                fTemp = new File(fOrig.getAbsolutePath() + "_");
                fWork = new File(fOrig.getAbsolutePath() + "_w");
                Archivo.crearArchivo(fWork.getAbsolutePath());
                //Visor.sistema("desencriptarArchivo.Paso 1");
                // Se copian los bytes a un archivo temporal
                Archivo.copiarBytes(fOrig.getAbsolutePath(), fTemp.getAbsolutePath());
                fInRaw = new FileInputStream(fOrig);
                fOutRaw = new FileOutputStream(fWork);
                //Visor.sistema("desencriptarArchivo.Paso 2");
                while (true) {
                    iRead = fInRaw.read();
                    if (iRead == -1) {
                        break;
                    }
                    if (iRead >= 0 && iRead <= 255) {
                        iCurX++;
                        if (iCurX >= iPosX && iCurY >= iPosY) {
                            iRead = iAsciiU[iRead];
                        }
                        fOutRaw.write(iRead);
                        if (iRead == 10 || iRead == 13) {
                            iCurY++;
                            iCurX = 0;
                        }
                    }
                }
                //Visor.sistema("desencriptarArchivo.Paso 3");
                fInRaw.close();
                fInRaw = null;
                fOutRaw.close();
                fOutRaw = null;
                ok = true;
                //Visor.sistema("desencriptarArchivo.Paso 4");
            }
        } catch (IOException e) {
            ok = false;
            Soporte.dump("Cipher.decrypAsciiArchivo", e);
            //Visor.sistema("desencriptarArchivo.Paso ****1");
            //System.out.println("Error de acceso de archivo:" + e);
        } finally {
            try {
                if (fInRaw != null) {
                    fInRaw.close();
                    fInRaw = null;
                }
                if (fOutRaw != null) {
                    fOutRaw.close();
                    fOutRaw = null;
                }
                //Visor.sistema("descomprimirArchivoRLE.Paso 5");
                if (ok) {
                    // Si es OK : Delete 'fOrig', Rename 'fWork' y Delete 'fTemp'
                    try {
                    	Archivo.copiarBytes(fWork.getAbsolutePath(), fOrig.getAbsolutePath());
                    } catch (Exception e) {
                        Soporte.dump("Cipher.decrypAsciiArchivo", e);
                    }
                }
                Archivo.eliminarFile(fWork.getAbsolutePath());
                Archivo.eliminarFile(fTemp.getAbsolutePath());
            } catch (Exception e) {
                ok = false;
                Soporte.dump("Cipher.decrypAsciiArchivo", e);
                //Visor.sistema("descomprimirArchivoRLE.Paso ****3");
            }
        }
        return ok;
    }

}
