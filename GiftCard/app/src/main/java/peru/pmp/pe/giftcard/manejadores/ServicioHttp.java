package peru.pmp.pe.giftcard.manejadores;



import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import peru.pmp.pe.giftcard.baseclass.Constantes;
import peru.pmp.pe.giftcard.libreria.Archivo;
import peru.pmp.pe.giftcard.libreria.security.Certificado;
import peru.pmp.pe.giftcard.utilitarios.ByteBuffer;
import peru.pmp.pe.giftcard.utilitarios.Soporte;


public class ServicioHttp implements Constantes {

	public static ByteBuffer callGET(String direccionURL) {

		if (Soporte.isNullOrE(direccionURL))
			return null;

		ByteBuffer buffer = null;
		try {

			URL obj = new URL(direccionURL);
			Soporte.dump("HTTP GET : " + direccionURL);
			HttpURLConnection con = null;

			if (obj.getProtocol().toLowerCase().equals("https")) {

				trustAllHosts();
				HttpsURLConnection https = (HttpsURLConnection) obj
						.openConnection();
				// /https.setHostnameVerifier(new HostnameVerifier());
				con = https;
			} else
				con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", HTTP_USER_AGENT);

			int responseCode = con.getResponseCode();

			if (responseCode != 200)
				Soporte.dump("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream(), Charset.forName(ByteBuffer.UTF8)));
			String inputLine;
			buffer = new ByteBuffer();

			while ((inputLine = in.readLine()) != null) {
				buffer.write(inputLine);
				buffer.write(Archivo.RETORNO);
			}

			in.close();
			in = null;
			obj = null;

		} catch (Exception e) {
			System.err.println("Error callGET " + direccionURL + " " + " - "
					+ e.getMessage());
		} finally {

		}
		return buffer;
	}

	public static ByteBuffer callPOST(String direccionURL, String parametrosURL) {

		if (Soporte.isNullOrE(direccionURL))
			return null;

		ByteBuffer buffer = null;
		try {

			URL obj = new URL(direccionURL);
			Soporte.dump("HTTP POST : " + direccionURL);
			HttpURLConnection con = null;

			if (obj.getProtocol().toLowerCase().equals("https")) {

				trustAllHosts();
				HttpsURLConnection https = (HttpsURLConnection) obj
						.openConnection();
				// /https.setHostnameVerifier(new HostnameVerifier());
				con = https;
			} else
				con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", HTTP_USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(parametrosURL);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();

			if (responseCode == HttpsURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				buffer = new ByteBuffer();

				while ((inputLine = in.readLine()) != null)
					buffer.write(inputLine);

				in.close();
			} else
				Soporte.dump("Response Code : " + responseCode);

		} catch (Exception e) {
			System.err.println("Error callGET " + direccionURL + " " + " - "
					+ e.getMessage());
		} finally {

		}
		return buffer;
	}

	public static ByteBuffer callPOST(String urlString,
			Map<String, String> variables) {
		ByteBuffer buffer = null;
		try {
			URL url = new URL(urlString);
			HttpsURLConnection httpsConnection = (HttpsURLConnection) url
					.openConnection();
			/*
			 * if (mHostnameVerifier != null) {
			 * httpsConnection.setHostnameVerifier(mHostnameVerifier); }
			 */
			httpsConnection.setDoInput(true);
			httpsConnection.setDoOutput(true);
			httpsConnection.setUseCaches(false);
			httpsConnection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			String postData = "";
			for (String key : variables.keySet()) {
				postData += "&" + key + "=" + variables.get(key);
			}
			postData = postData.substring(1);
			DataOutputStream postOut = new DataOutputStream(
					httpsConnection.getOutputStream());
			postOut.writeBytes(postData);
			postOut.flush();
			postOut.close();
			int responseCode = httpsConnection.getResponseCode();
			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				buffer = new ByteBuffer();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						httpsConnection.getInputStream()));
				while ((line = br.readLine()) != null) {
					buffer.write(line);
				}
			} else {
				Soporte.dump("HTTPs request failed on: " + urlString
						+ " With error code: " + responseCode);
			}
		} catch (Exception e) {
			System.err.println("Error callGET " + urlString + " " + " - "
					+ e.getMessage());
		}
		return buffer;
	}

	public static ByteBuffer callService(String urlServicio, String metodo,
                                         String data, boolean tls) throws Exception {
		ByteBuffer buffer = null;
		Certificado certificado = null;
		String textoBase;
		try {
			textoBase = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					+ "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
					+ "  xmlns:ns1=\"http://tempuri.org/\">"
					+ " <SOAP-ENV:Body>" + "   <ns1:#METODO>"
					+ "     <ns1:XML>#CONTENIDO</ns1:XML>"
					+ "   </ns1:#METODO>" + " </SOAP-ENV:Body>"
					+ "</SOAP-ENV:Envelope>";

			textoBase = textoBase.replace("#METODO", metodo);
			textoBase = textoBase.replace("#CONTENIDO", data);

			byte[] dataBase = textoBase.getBytes("UTF-8");

			Soporte.dump(textoBase);
			/*
			if(tls){
				certificado = new Certificado();
				certificado.loadKeyStore();
				certificado.preloadSSLContext("TLS");
			}
            */
			//Memoria.getOperacion().sendBackPaymentHandler("Call service "+urlServicio+" Metodo:"+metodo);
			
			URL url = new URL(urlServicio);
			HttpURLConnection httpsConnection = (HttpURLConnection) url
					.openConnection();
			
			/// Se asigna el SocketFactory de acuerdo a la informacion almacenada en el keystore
			//if(certificado!=null && certificado.getSSLSocketFactory()!=null)
			//	httpsConnection.setSSLSocketFactory(certificado.getSSLSocketFactory());

			httpsConnection.setDoInput(true);
			httpsConnection.setDoOutput(true);
			httpsConnection.setUseCaches(false);
			httpsConnection.setReadTimeout(30000);
			httpsConnection.setRequestProperty("Content-Type",
					"text/xml; charset=utf-8");
			httpsConnection.setRequestProperty("Content-Length",
					dataBase.length + "");
			httpsConnection.setRequestProperty("SOAPAction",
					"http://tempuri.org/IService1/" + metodo);

			DataOutputStream postOut = new DataOutputStream(
					httpsConnection.getOutputStream());
			postOut.write(dataBase);
			postOut.flush();
			postOut.close();
			int responseCode = httpsConnection.getResponseCode();
			
			//Memoria.getOperacion().sendBackPaymentHandler("HTTP response code:"+responseCode);

			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				buffer = new ByteBuffer();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						httpsConnection.getInputStream()));
				while ((line = br.readLine()) != null) {
					buffer.write(line);
				}
			} else {
				///Soporte.dump("HTTPs request failed on: " + urlServicio
				///		+ " With error code: " + responseCode);
				throw new Exception("HTTP codigo retorno: "+ responseCode);
			}
		} catch (Exception e) {
			System.err.println("Error callGET " + urlServicio + " " + " - "
					+ e.getMessage());
			throw new Exception(e.getMessage());
		} finally {
			certificado = null;
			textoBase = null;
		}
		return buffer;
	}

	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(
					java.security.cert.X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

			@Override
			public void checkServerTrusted(
					java.security.cert.X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ByteBuffer callPOST() {
		String urlString = "";
		ByteBuffer buffer = null;
		try {
			// Load CAs from an InputStream
			// (could be from a resource or ByteArrayInputStream or ...)
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			// From
			// https://www.washington.edu/itconnect/security/ca/load-der.crt
			InputStream caInput = new BufferedInputStream(new FileInputStream(
					"load-der.crt"));
			Certificate ca;
			try {
				ca = cf.generateCertificate(caInput);
				System.out.println("ca="
						+ ((X509Certificate) ca).getSubjectDN());
			} finally {
				caInput.close();
			}

			// Create a KeyStore containing our trusted CAs
			String keyStoreType = KeyStore.getDefaultType();
			KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(null, null);
			keyStore.setCertificateEntry("ca", ca);

			// Create a TrustManager that trusts the CAs in our KeyStore
			String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			TrustManagerFactory tmf = TrustManagerFactory
					.getInstance(tmfAlgorithm);
			tmf.init(keyStore);

			// Create an SSLContext that uses our TrustManager
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, tmf.getTrustManagers(), null);

			// Tell the URLConnection to use a SocketFactory from our SSLContext
			URL url = new URL("https://certs.cac.washington.edu/CAtest/");
			HttpsURLConnection urlConnection = (HttpsURLConnection) url
					.openConnection();
			urlConnection.setSSLSocketFactory(context.getSocketFactory());
			InputStream in = urlConnection.getInputStream();
			Vector<String> vector = Archivo.TXT2vector(in);
			for (String texto : vector)
				Soporte.dump(texto);
		} catch (Exception e) {
			System.err.println("Error callGET " + urlString + " " + " - "
					+ e.getMessage());
		}
		return null;
	}
	
}
