package peru.pmp.pe.giftcard.entidades;

/**
 * Created by Admin on 29/11/17.
 */

public class TarjetaInfoE {

    private String card_alias;
    private String card_brand;
    private Integer card_color;
    private Integer card_favorite;
    private String card_fecha_vcto;
    private String card_full_name;
    private String card_mail;
    private String card_mask;
    private String card_token_id;
    private Integer card_validation;
    public String SaldoDispActual;
    public String LastMontoTxn;

    public TarjetaInfoE() {
        //super();

        card_alias = "";
        card_brand = "";
        card_color = 0;
        card_favorite = 0;
        card_fecha_vcto = "";
        card_full_name = "";
        card_mail = "";
        card_mask = "";
        card_token_id = "";
        card_validation = 0;
        SaldoDispActual = "";
        LastMontoTxn = "";
    }

    public TarjetaInfoE(String card_alias, String card_brand, Integer card_color, Integer card_favorite, String card_fecha_vcto, String card_full_name, String card_mail, String card_mask, String card_token_id, Integer card_validation) {
        this.card_alias = card_alias;
        this.card_brand = card_brand;
        this.card_color = card_color;
        this.card_favorite = card_favorite;
        this.card_fecha_vcto = card_fecha_vcto;
        this.card_full_name = card_full_name;
        this.card_mail = card_mail;
        this.card_mask = card_mask;
        this.card_token_id = card_token_id;
        this.card_validation = card_validation;
    }

    public String getCard_alias() {
        return card_alias;
    }

    public void setCard_alias(String card_alias) {
        this.card_alias = card_alias;
    }

    public String getCard_brand() {
        return card_brand;
    }

    public void setCard_brand(String card_brand) {
        this.card_brand = card_brand;
    }

    public Integer getCard_color() {
        return card_color;
    }

    public void setCard_color(Integer card_color) {
        this.card_color = card_color;
    }

    public Integer getCard_favorite() {
        return card_favorite;
    }

    public void setCard_favorite(Integer card_favorite) {
        this.card_favorite = card_favorite;
    }

    public String getCard_fecha_vcto() {
        return card_fecha_vcto;
    }

    public void setCard_fecha_vcto(String card_fecha_vcto) {
        this.card_fecha_vcto = card_fecha_vcto;
    }

    public String getCard_full_name() {
        return card_full_name;
    }

    public void setCard_full_name(String card_full_name) {
        this.card_full_name = card_full_name;
    }

    public String getCard_mail() {
        return card_mail;
    }

    public void setCard_mail(String card_mail) {
        this.card_mail = card_mail;
    }

    public String getCard_mask() {
        return card_mask;
    }

    public void setCard_mask(String card_mask) {
        this.card_mask = card_mask;
    }

    public String getCard_token_id() {
        return card_token_id;
    }

    public void setCard_token_id(String card_token_id) {
        this.card_token_id = card_token_id;
    }

    public Integer getCard_validation() {
        return card_validation;
    }

    public void setCard_validation(Integer card_validation) {
        this.card_validation = card_validation;
    }

    public String getSaldoDispActual() {
        return SaldoDispActual;
    }

    public void setSaldoDispActual(String SaldoDispActual) {
        this.SaldoDispActual = SaldoDispActual;
    }

    public String getLastMontoTxn() {
        return LastMontoTxn;
    }

    public void setLastMontoTxn(String LastMontoTxn) {
        this.LastMontoTxn = LastMontoTxn;
    }
}
