package peru.pmp.pe.giftcard.realm;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import io.realm.Realm;
import io.realm.RealmResults;
import peru.pmp.pe.giftcard.entidades.Tarjeta;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from Book.class
    public void clearAll() {

        realm.beginTransaction();
        realm.clear(Tarjeta.class);
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public RealmResults<Tarjeta> getBooks() {

        return realm.where(Tarjeta.class).findAll();
    }

    //query a single item with the given id
    public Tarjeta getBook(String card_mask, String card_alias) {

        return realm.where(Tarjeta.class).contains("card_mask", card_mask).contains("card_alias", card_alias).findFirst();
    }

    //check if Book.class is empty
    public boolean hasBooks() {

        return !realm.allObjects(Tarjeta.class).isEmpty();
    }

    //query example
    public RealmResults<Tarjeta> queryedBooks() {

        return realm.where(Tarjeta.class)
                //.contains("author", "Author 0")
                //.or()
                //.contains("title", "Realm")
                .findAll();
    }
}
