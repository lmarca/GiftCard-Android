package peru.pmp.pe.giftcard.baseclass;

import java.io.File;
import java.util.Locale;

/**
 * Clase interface utilizada para la definicion de constantes utilizadas en el
 * sistema
 * 
 * @author Billy Vera Castellanos
 */
public interface Constantes {

	// / Manejo de Base de Datos
	String FLAG1 = "flag1";
	String FLAG2 = "flag2";
	char FLG_EMPTY = ' ';
	char FLG_NOT_CHANGED = '0';
	char FLG_UPD_MOVIL = '1';
	char FLG_NEW_MOVIL = '2';
	char FLG_ELI_MOVIL = '3';
	char FLG_UPD_SERVER = '4';
	char FLG_NEW_SERVER = '5';
	char FLG_ELI_SERVER = '6';
	char FLG_TRANSMITIDO = '7';
	char FLG_CUSTOM = 'X';

	String AID_MC_CREDIT = "A0000000041010";
	String AID_AMEX = "A00000002501";
	String AID_DINERS = "A0000001523010";
	String AID_VISA_ELECTRON = "A0000000031012";
	String AID_VISA_CREDITO = "A0000000031011";
	String AID_MAESTRO = "A0000000043060";
	String AID_UNIONPAY = "A0000003330101";
	String AID_PRIVADAS = "A0000006110102";

	// / Caracteres
	String CARACTER_COMODIN = "*";
	String FIELD_DELIMITED = "|";
	String FIELD_CODE = "code";
	String FIELD_DESCRIP = "descrip";
	String FIELD_DEFAULT = "default";
	String PRIMARY_KEY = "primary_key";
	String ENTER = "\r\n";
	String SDCARD = "//sdcard//";
	String EQUIPO_BLUETOOH = "Wisepad,WP02840624140003"; // / Separado por ','
	String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	String PATTERN_TELEFONO = "^(?:0091|\\+91|0)[7-9][0-9]{9}$";

	int LEN_ENTER = 2;
	int LEN_MAX_NUMTRANSACTION = 7;
	int LEN_MAX_IMEI = 15;
	int KEY_RETORNO_CARRO = 13;
	int KEY_SALTO_LINEA = 10;
	String SEPARADOR = File.separator;

	// / Tipo de Sistema de Base de datos
	int DB_FILESYSTEM = 1;
	int DB_RECORDSTORE = 2;
	int DB_SQLITE = 3;

	// / Condiciones de Busqueda
	int COND_IN = 1;
	int COND_NOT_IN = 2;
	int COND_EQUALS = 3;
	int COND_NOT_EQUALS = 4;
	int COND_STARTWITH = 5;
	int COND_CONTAINS = 6;
	int COND_ENDWITH = 7;
	int COND_NONE = 8;
	int COND_REQUIRED = 9;
	int COND_STARTWITH_REQUIRED = 10;
	int COND_BETWEEN = 11;
	int COND_MAYOR = 12;
	int COND_MAYOR_IGUAL = 13;
	int COND_MENOR = 14;
	int COND_MENOR_IGUAL = 15;

	// / Match de Busqueda
	int MATCH_END_SEARCH = 101;
	int MATCH_EQUAL_CONDITION = 102;
	int MATCH_NOT_EQUAL_CONDITION = 103;
	int MAX_RECORDS_RESULT = 20;
	int MAX_RECORDS_PERMITED = 350;

	//String PRE_URL = "https://testwsmobile.punto-web.com/wcfecommerceJW/Service.svc/";



	//String PRE_URL = "https://wsgiftcard.punto-web.com/wcfEcommerceGC/Service.svc/";
    String PRE_URL = "https://testwsgiftcard.punto-web.com/wcfEcommerceGC/Service.svc/";

	//String URL_GESTION_TARJETAS = "https://wsgiftcard.punto-web.com/WCFTransformacionXMLJSON/Service.svc/";
	String URL_GESTION_TARJETAS = "https://testwsgiftcard.punto-web.com/WCFTransformacionXMLJSON/Service.svc/";

	String MERCHANT_CODE = "0000095";

	String COD_EMISOR = "095";
	String COD_USUARIO = "BC0001";
	String COMERCIO = "4000200";
	String MONEDA = "604";
	String NUM_TERMINAL = "11010101";
	String WS_USUARIO = "3607813531";
	String WS_CLAVE = "WRazu6ed7u7azuth";


	// / Informacion General
	String APP_NAME = "BotonPago";
	String APP_VERSION = "08Jun2016";
	String APP_DESCRIP = "Sistema Movil de Pagos con Tarjeta de Credito";
	String APP_CLIENTE = "Procesos de Medios de Pago S.A.";
	String APP_CLI_SHORTNAME = "PROCESOS MC";
	String APP_DATABASE = "botonProcesos.db";
	int APP_DATABASE_VERSION = 2;
	String APP_FOLDER = "botonpmp_root/";
	String APP_FOLDER_IMAGES = "images/";
	String APP_FOLDER_INCLUDE = "include/";
	String APP_FOLDER_LOGS = "log_files/";
	String APP_FOLDER_TEMP = "temp/";
	String APP_FOLDER_CHECKSUM = "checksum/";
	String ARCH_PROPERTIES = "properties.conf";
	String ARCH_LOG_ERROR = "error.log";
	String ARCH_LOG_SISTEMA = "system.log";
	String ARCH_LOG_BASEDATOS = "database.log";
	String ARCH_LOG_MENSAJERIA = "messaging.log";
	String ARCH_LOG_RED_TRAFICO = "network.log";
	String ARCH_LOG_TRACE = "trace.txt";
	int MAX_LOGSIZE = 3072;

	// / Clave 128bits
	String KEY_128BITS = "Epe7perez4in2013";

	// / Informacion HTTP
	String HEADER_PROPERTIES = "(2014) Author: Billy Vera";
	String HTTP_USER_AGENT = "User-Agent"; // "Profile/MIDP-2.0 Configuration/CLDC-1.0"
	String HTTP_CONTENT_LANGUAGE = "Content-Language"; // "sp-PE"
	String HTTP_CONTENT_LENGTH = "Content-Length"; // size bytes
	String ASCII = "US-ASCII";
	String UTF8 = "UTF-8";
	String ISO8859_1 = "ISO8859_1";
	String DEFAULT_CHAR_ENCODING = ISO8859_1;

	public enum ModuloOrProceso {
		NONE, CONSUMO, CONFIRMACION, ANULACION, EMAIL, SMS, REPORTE_TOTAL, REPORTE_DETALLADO, REPORTE_CONSOLIDADO, REPORTE_REIMPRESION, REPORTE_MOZO, TEBCA_APERTURA, TEBCA_RECARGA, DATOS_COMERCIO_GET, DATOS_COMERCIO_SET;
		public int value() {
			switch (this) {
			case CONSUMO:
				return 1;
			case CONFIRMACION:
				return 2;
			case ANULACION:
				return 3;
			case EMAIL:
				return 4;
			case REPORTE_TOTAL:
				return 5;
			case REPORTE_DETALLADO:
				return 6;
			case REPORTE_REIMPRESION:
				return 7;
			case REPORTE_MOZO:
				return 8;
			case SMS:
				return 9;
			case TEBCA_APERTURA:
				return 10;
			case TEBCA_RECARGA:
				return 11;
			case DATOS_COMERCIO_GET:
				return 12;
			case DATOS_COMERCIO_SET:
				return 13;
			case REPORTE_CONSOLIDADO:
				return 14;
			default:
				return 0;
			}
		}

		public boolean isTebca() {
			return this == TEBCA_APERTURA || this == TEBCA_RECARGA;
		}
	}

	public enum Cines {
		NONE, WHOIS, DATA_INICIAL, CARTELERA, SALA, PORC_VENTA, TARIFARIO, ASIENTOS, RESERVAR, LIBERAR, REG_VENTA, REG_NCRED, REIMPRESION, EMAIL, BUTACAS
	};

	public enum BateriaStatus {
		NONE, NORMAL, BAJA, CRITICA
	};

	public enum TipoOperacion {
		NONE, CONSUMO, CONSULTA, ANULACION, REIMPRESION, REPORTE_DETALLADO, REPORTE_TOTAL, REPORTE_MOZO, TEBCA;

		public String code() {
			if (this == CONSUMO)
				return "AT";
			else if (this == CONSULTA)
				return "CO";
			else if (this == ANULACION)
				return "AL";
			else
				return "AT";
		}

		public boolean isConsumo() {
			return this == CONSUMO;
		}

		public boolean isAnulacion() {
			return this == ANULACION;
		}

		public boolean isTebca() {
			return this == TEBCA;
		}

		public boolean isReporte() {
			return this == REPORTE_DETALLADO || this == REPORTE_TOTAL
					|| this == REIMPRESION || this == REPORTE_MOZO;
		}
	}

	public enum TipoLectura {
		NONE, BANDA, CHIP, FALLBACK;

	}

	public enum TipoTarjeta {
		GENERICO, MASTERDEBIT, DEBITO, CREDITO, UNIONPAY, NONE;

		public int value() {
			switch (this) {
			case GENERICO:
				return 1;
			case MASTERDEBIT:
				return 2;
			case DEBITO:
				return 3;
			case CREDITO:
				return 4;
			case UNIONPAY:
				return 5;
			default:
				return 0;
			}
		}

		public String toString() {
			String output = name();
			return output;
		}
	}

	public enum Moneda {
		SOLES, DOLARES;

		public static Moneda get(String name) {
			Moneda[] listado = Moneda.values();
			Moneda retorno = SOLES;
			if (name != null)
				for (Moneda current : listado) {
					if (current.toString().equalsIgnoreCase(name)) {
						retorno = current;
						break;
					}
				}
			return retorno;
		}

		public static Moneda getByCode(String code) {
			Moneda[] listado = Moneda.values();
			Moneda retorno = SOLES;
			if (code != null)
				for (Moneda current : listado) {
					if (current.currencyCode().equalsIgnoreCase(code)) {
						retorno = current;
						break;
					}
				}
			return retorno;
		}

		public String code() {
			switch (this) {
			case SOLES:
				return "PEN";
			case DOLARES:
				return "USD";
			default:
				return "PEN";
			}
		}

		public int value() {
			switch (this) {
			case SOLES:
				return 1;
			case DOLARES:
				return 2;
			default:
				return 0;
			}
		}

		public String simbolo() {
			switch (this) {
			case SOLES:
				return "S/";
			case DOLARES:
				return "U$";
			default:
				return "";
			}
		}

		public String currencyCode() {
			switch (this) {
			case SOLES:
				return "604";
			case DOLARES:
				return "840";
			default:
				return "";
			}
		}

		public String toString() {
			String output = name();
			return output;
		}
	}

	public enum TipoPago {
		CRED_SIN_CUOTAS, CRED_CON_CUOTAS, DEBITO;

		public static TipoPago get(String name) {
			TipoPago[] listado = TipoPago.values();
			TipoPago retorno = CRED_SIN_CUOTAS;
			if (name != null)
				for (TipoPago current : listado) {
					if (current.toString().equalsIgnoreCase(name)) {
						retorno = current;
						break;
					}
				}
			return retorno;
		}

		public String value() {
			switch (this) {
			case CRED_SIN_CUOTAS:
				return "30";
			case CRED_CON_CUOTAS:
				return "30";
			case DEBITO:
				return "00";
			default:
				return "";
			}
		}

		public String toString() {
			String output = name();
			return output;
		}
	}

	// / Status Dispositivo
	public enum Status {
		NONE, BUSY, SOLO_BANDA, LECTURA_BANDA, LECTURA_EMV, PROCESANDO, PROCESO_MCR, PROCESO_EMV, REQUEST_PIN, APP_NO_SOPORTADA, INSERT_CHIP, RESET_EQUIPO;

		public boolean isConsumo() {
			if (this == LECTURA_BANDA || this == LECTURA_EMV
					|| this == PROCESO_MCR || this == PROCESO_EMV)
				return true;
			else
				return false;
		}
	}

	public enum ModoLectura {
		NONE, LECTURA_MCR, LECTURA_EMV;

		public boolean isBanda() {
			return this == LECTURA_MCR;
		}

		public boolean isEMV() {
			return this == LECTURA_EMV;
		}
	}

	// / Tipos de Datos
	public enum Tipos {
		NONE, CURSOR, TEXT, CHAR, CHARACTER, VARCHAR, VARCHAR2, NVARCHAR, DATE, DATETIME, BIT, NUMBER, NUMERIC, DOUBLE, INT, INTEGER, BIGINT, SMALLINT, DECIMAL, REAL, MONEY, C, N, L, D;

		public boolean isCursor() {
			return (this == CURSOR);
		}

		public boolean isText() {
			if (this == TEXT || this == CHAR || this == CHARACTER
					|| this == VARCHAR || this == VARCHAR2 || this == NVARCHAR
					|| this == C)
				return true;
			else
				return false;
		}

		public boolean isNumber() {
			if (isDecimal() || isInteger())
				return true;
			else
				return false;
		}

		public boolean isDecimal() {
			if (this == NUMBER || this == NUMERIC || this == DOUBLE
					|| this == DECIMAL || this == REAL || this == N)
				return true;
			else
				return false;
		}

		public boolean isInteger() {
			if (this == INT || this == INTEGER || this == BIGINT
					|| this == SMALLINT)
				return true;
			else
				return false;
		}

		public boolean isDate() {
			if (this == DATE || this == DATETIME || this == D)
				return true;
			else
				return false;
		}

		public boolean isBit() {
			if (this == BIT || this == L)
				return true;
			else
				return false;
		}

		public static Tipos get(String name) {
			Tipos[] listado = Tipos.values();
			Tipos retorno = NONE;
			if (name != null)
				for (Tipos current : listado) {
					if (current.toString().equalsIgnoreCase(name)) {
						retorno = current;
						break;
					}
				}
			return retorno;
		}

		public int sqlType() {

			if (isCursor())
				return -10; // /OracleTypes.CURSOR;
			else if (isText())
				return java.sql.Types.VARCHAR;
			else if (isNumber())
				return java.sql.Types.INTEGER;
			else if (isDecimal())
				return java.sql.Types.DECIMAL;
			else if (isDate())
				return java.sql.Types.DATE;
			else if (isBit())
				return java.sql.Types.BOOLEAN;
			else
				return 0;
		}

		public String toString() {
			String output = name();
			return output;
		}
	}

	// / Nomenclaturas del Sistema
	public enum Nomenclatura {
		USER;

		public String value() {
			switch (this) {
			case USER:
				return "<USR>";
			default:
				return this.name();
			}
		}

		public String html() {
			switch (this) {
			case USER:
				return "&ltUSR>";
			default:
				return this.name();
			}
		}

		public static Nomenclatura get(String name) {
			Nomenclatura[] listado = Nomenclatura.values();
			Nomenclatura retorno = USER;
			for (Nomenclatura current : listado) {
				if (current.value().equalsIgnoreCase(name)) {
					retorno = current;
					break;
				}
			}
			return retorno;
		}
	}

	/**********
	 * NO_INICIADO = No se inicio el servicio INICIADO = Se inicio el servicio
	 * PAUSA = Se ha puesto el servicio en pausa manualmente DETENIDO = Se
	 * detuvo el servicio manualmente TIMER_IDLE = Servicio iniciado pero esta
	 * en sleep por la frecuencia CRITICO = Servicio se encuentra en ejecucion
	 * critica
	 */
	public enum EstadoProceso {
		NO_INICIADO, DETENIDO, PAUSA, INICIADO, TIMER_IDLE, CRITICO, NO_CALENDARIO, NONE;

		public static EstadoProceso get(String name) {
			EstadoProceso[] listado = EstadoProceso.values();
			EstadoProceso retorno = DETENIDO;
			for (EstadoProceso current : listado) {
				if (current.name().equalsIgnoreCase(name)) {
					retorno = current;
					break;
				}
			}
			return retorno;
		}

		public boolean isWorking() {
			if (this == INICIADO || this == TIMER_IDLE || this == CRITICO)
				return true;
			else
				return false;
		}

		public boolean isPause() {
			if (this == PAUSA)
				return true;
			else
				return false;
		}

		public boolean isDetenido() {
			if (this == DETENIDO)
				return true;
			else
				return false;
		}

		public boolean isReady() {
			if ((this == NO_INICIADO || this == DETENIDO || this == PAUSA)
					&& (this != NO_CALENDARIO && this != NONE))
				return true;
			else
				return false;
		}
	}

	public enum TblSYS {
		NONE, CURSOR, TEXT, CHAR, CHARACTER, VARCHAR, VARCHAR2, NVARCHAR, DATE, DATETIME, BIT, NUMBER, NUMERIC, DOUBLE, INT, INTEGER, BIGINT, SMALLINT, DECIMAL, REAL, MONEY, C, N, L, D;
		public String toString() {
			String output = name();
			return output;
		}
	}

	public enum TblJUN {
		NONE, CURSOR, TEXT, CHAR, CHARACTER, VARCHAR, VARCHAR2, NVARCHAR, DATE, DATETIME, BIT, NUMBER, NUMERIC, DOUBLE, INT, INTEGER, BIGINT, SMALLINT, DECIMAL, REAL, MONEY, C, N, L, D;

		public String toString() {
			String output = name();
			return output;
		}
	}

	public enum Cmd {
		WHOIS, LOGON, SESSION_OK, SESSION_ERR, SESSION_NEWPWD, SESSION_INACTIVA, FILE_REQUEST, FILE_RESPONSE, FILE_SEND, FILE_REGISTERED, SQL_SEND, SQL_REQUEST, SQL_RESPONSE, SQL_REGISTERED, SQL_RESULT, END_SESSION;

		public String toString() {
			String output = name();
			return output;
		}
	}

	public enum Properties {
		RATIO_BYTES;

		public String toString() {
			String output = name();
			return output;
		}
	}

	public enum LogonStatus {
		NO_INICIADO, REQUIRED, RECEIVED, TERMINAL_NOT_VALID, SUCCESS, NOT_RESPONSE;

		public int value() {
			switch (this) {
			case NO_INICIADO:
				return 1;
			case REQUIRED:
				return 2;
			case RECEIVED:
				return 3;
			case TERMINAL_NOT_VALID:
				return 4;
			case SUCCESS:
				return 5;
			default:
				return 0;
			}
		}

		public String text() {
			switch (this.value()) {
			case 1:
				return "INICIANDO SISTEMA";
			case 2:
				return "INFORMACION REQUERIDA";
			case 3:
				return "INFORMACION RECIBIDA";
			case 4:
				return "TERMINAL NO VALIDO";
			case 5:
				return "LOGON SATISFACTORIO";
			default:
				return "LA COMUNICACION CON EL SERVIDOR NO HA SIDO SATISFACTORIA";
			}
		}

	}

	public enum StatusGeneral {
		NO_INICIADO, TERMINAL_INVALIDO, EN_LINEA, REQUEST_HOST
	}

	public enum TerminalPOS {
		NONE, E105, WISEPAD, EMVSWIPE, E106;

		public static TerminalPOS get(int valor) {
			switch (valor) {
			case 1:
				return E105;
			case 2:
				return WISEPAD;
			case 3:
				return EMVSWIPE;
			case 4:
				return E106;
			}
			return NONE;
		}

		public int value() {
			switch (this) {
			case E105:
				return 1;
			case WISEPAD:
				return 2;
			case EMVSWIPE:
				return 3;
			case E106:
				return 4;
			default:
				return 0;
			}
		}

		public String toString() {
			switch (this) {
			case E105:
				return "Verifone E105";
			case WISEPAD:
				return "WisePad";
			case EMVSWIPE:
				return "EMVSwipe";
			case E106:
				return "E106";
			default:
				return "NINGUNO";
			}
		}

		public String descripcion() {
			switch (this) {
			case E105:
				return "Conectividad por Jack Audio";
			case WISEPAD:
				return "Conectividad por Bluetooth";
			case EMVSWIPE:
				return "Conectividad por Jack Audio";
			case E106:
				return "Conectividad por Bluetooth";
			default:
				return "NINGUNO";
			}
		}

		public boolean isAudioJack() {
			switch (this) {
			case E105:
				return true;
			case WISEPAD:
				return false;
			case EMVSWIPE:
				return true;
			case E106:
				return false;
			default:
				return false;
			}
		}

		public boolean isBluetooth() {
			switch (this) {
			case E105:
				return false;
			case WISEPAD:
				return true;
			case EMVSWIPE:
				return false;
			case E106:
				return true;
			default:
				return false;
			}
		}
	}

	public static enum EstadoAsiento {
		DISPONIBLE, OCUPADO, RESERVADO;

		public static EstadoAsiento get(int valor) {
			if (valor == 1)
				return OCUPADO;
			else if (valor == 4)
				return RESERVADO;
			else
				return DISPONIBLE;

		}
	};

	public static int ENABLE_BT = 1;
	public static int CONNECT_DEVICE = 2;

	public enum MarcaTarjeta {
		MASTERCARD, AMERICAN_EXPRESS, DINERS, OH, CENCOSUD, RIPLEY, CMR, DISCOVER, JCB, NONE;

		public String imagenOn() {
			return value().toLowerCase(Locale.getDefault()) + "_on.png";
		}

		public String imagenOff() {
			return value().toLowerCase(Locale.getDefault()) + "_off.png";
		}

		public String cvcImagen() {
			if (this == AMERICAN_EXPRESS)
				return value().toLowerCase(Locale.getDefault()) + "_cvc.png";
			else if (this == DINERS)
				return value().toLowerCase(Locale.getDefault()) + "_cvc.png";
			else
				return "mc_cvc.png";
		}

		public String text() {
			switch (this) {
			case MASTERCARD:
				return "MASTERCARD";
			case AMERICAN_EXPRESS:
				return "AMERICAN EXPRESS";
			case DINERS:
				return "DINERS CLUB";
			case OH:
				return "OH";
			case CENCOSUD:
				return "CENCOSUD";
			case RIPLEY:
				return "RIPLEY";
			case CMR:
				return "CMR";
			case DISCOVER:
				return "DISCOVER";
			case JCB:
				return "JCB";
			default:
				return "NUEVA";
			}
		}

		public String value() {
			switch (this) {
			case MASTERCARD:
				return "MC";
			case AMERICAN_EXPRESS:
				return "AE";
			case DINERS:
				return "DN";
			case OH:
				return "T0";
			case CENCOSUD:
				return "TM";
			case RIPLEY:
				return "RP";
			case CMR:
				return "BF";
			case DISCOVER:
				return "DV";
			case JCB:
				return "JC";
			default:
				return "new";
			}
		}

		public static MarcaTarjeta get(String valor) {
			for (MarcaTarjeta tipo : MarcaTarjeta.values()) {
				if (tipo.value().equalsIgnoreCase(valor)) {
					return tipo;
				}
			}
			return NONE;
		}
	}

	public enum TipoDocumento {
		DNI, CE, PASAPORTE, RUC, OTROS;

		public String text() {
			switch (this) {
			case DNI:
				return "DNI";
			case CE:
				return "C.EXTRANJERIA";
			case PASAPORTE:
				return "PASAPORTE";
			case RUC:
				return "RUC";
			default:
				return "OTRO";
			}
		}

		public static TipoDocumento get(String valor) {
			for (TipoDocumento tipo : TipoDocumento.values()) {
				if (tipo.value().equals(valor)) {
					return tipo;
				}
			}
			return OTROS;
		}

		public String value() {
			return name();
		}
	}

	public enum Lenguaje {
		ESPAÑOL, ENGLISH;

		public String text() {
			switch (this) {
			case ESPAÑOL:
				return "ESPAÑOL";
			case ENGLISH:
				return "ENGLISH";
			default:
				return "";
			}
		}

		public String value() {
			switch (this) {
			case ESPAÑOL:
				return "ES";
			case ENGLISH:
				return "US";
			default:
				return "";
			}
		}

		public static Lenguaje get(String valor) {
			for (Lenguaje tipo : Lenguaje.values()) {
				if (tipo.value().equals(valor)) {
					return tipo;
				}
			}
			return ESPAÑOL;
		}
	}

	public enum CodigoRetorno {
		OPERACION_CANCELADA, 
		NO_HAY_INTERNET, 
		NO_PARAMETROS, 
		FECHA_HORA_INVALIDOS, 
		ACCION_NO_PERMITIDA,
		NO_TARJETAS_VALIDAS,
		TIMEOUT_OPERACION,
		EXCEPCION;

		public String text() {
			switch (this) {
			case OPERACION_CANCELADA:
				return "Operación cancelada por el usuario";
			case NO_HAY_INTERNET:
				return "No se ha detectado acceso a Internet";
			case NO_PARAMETROS:
				return "No se recibieron parámetros";
			case FECHA_HORA_INVALIDOS:
				return "Parámetros de Fecha/Hora inválidos";
			case ACCION_NO_PERMITIDA:
				return "Acción no permitida";
			case NO_TARJETAS_VALIDAS:
				return "No se han recibido marcas de tarjetas válidas";
			case TIMEOUT_OPERACION:
				return "Se ha excedido el tiempo limite de espera";
			default:
				return "";
			}
		}

		public String value() {
			switch (this) {
			case OPERACION_CANCELADA:
				return "M1";
			case NO_HAY_INTERNET:
				return "M2";
			case NO_PARAMETROS:
				return "M3";
			case FECHA_HORA_INVALIDOS:
				return "M4";
			case ACCION_NO_PERMITIDA:
				return "M5";
			case NO_TARJETAS_VALIDAS:
				return "M6";
			case TIMEOUT_OPERACION:
				return "M7";
			default:
				return "MX";
			}
		}

		public static CodigoRetorno get(String valor) {
			for (CodigoRetorno tipo : CodigoRetorno.values()) {
				if (tipo.value().equals(valor)) {
					return tipo;
				}
			}
			return null;
		}

	}
}
