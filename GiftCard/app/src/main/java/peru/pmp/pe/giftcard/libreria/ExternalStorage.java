package peru.pmp.pe.giftcard.libreria;

import android.os.Environment;

public class ExternalStorage {

	public static boolean isAvailable(){
		
		boolean mExternalStorageAvailable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = true;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = false;
		}
		
		return mExternalStorageAvailable;
	}

	public static boolean isWriteable(){
		
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		mExternalStorageWriteable = 
		    Environment.MEDIA_MOUNTED.equals(state)
				&& !Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
		
		return mExternalStorageWriteable;
	}
}
