package peru.pmp.pe.giftcard.baseclass;

import peru.pmp.pe.giftcard.utilitarios.Soporte;

/**
 * Clase base utilizada para el manejo de campos de tablas
 * @author Billy Vera Castellanos
 */
public class Campo implements Constantes{

    private int liOrden;
    private String lsNombre;
    private String lsTipo;
    private int liSize;
    private int liDecimales;
    private boolean lbPk;
    private boolean lbIndice;
    // Informacion calculada
    private int liPosIni;
    private int liPosFin;
    // Informacion personalizada
    private String lsContenido;

    public Campo(int orden, String nombre, String tipo,
                 int size, int decimales,
                 boolean pk, boolean indice) {
        liOrden = orden;
        lsNombre = nombre;
        lsTipo = Soporte.isNullOrE(tipo) ? Tipos.TEXT.toString() : tipo;
        liSize = size;
        liDecimales = decimales;
        lbPk = pk;
        lbIndice = indice;
    }
    
    public Campo(int orden, String nombre, Tipos tipo,
                 int size, int decimales,
                 boolean pk, boolean indice) {
        liOrden = orden;
        lsNombre = nombre;
        lsTipo = tipo.name();
        liSize = size;
        liDecimales = decimales;
        lbPk = pk;
        lbIndice = indice;
    }
    /// PROPIEDADES
    public int getDecimales() {
        return liDecimales;
    }

    public String getNombre() {
        return lsNombre;
    }

    public int getOrden() {
        return liOrden;
    }

    public int getSize() {
        return liSize;
    }

    public String getTipo() {
        return lsTipo;
    }

    public boolean isIndice() {
        return lbIndice;
    }

    public boolean isPk() {
        return lbPk;
    }

    public void setDecimales(int _decimales) {
        this.liDecimales = _decimales;
    }

    public void setIndice(boolean _indice) {
        this.lbIndice = _indice;
    }

    public void setNombre(String _nombre) {
        this.lsNombre = _nombre;
    }

    public void setOrden(int _orden) {
        this.liOrden = _orden;
    }

    public void setPk(boolean _pk) {
        this.lbPk = _pk;
    }

    public void setSize(int _size) {
        this.liSize = _size;
    }

    public void setTipo(String _tipo) {
        this.lsTipo = _tipo;
    }

    public void setContenido(String _contenido) {
        this.lsContenido = (_contenido == null ? "" : _contenido.trim());
    }

    public String getContenido() {
        return lsContenido;
    }

    public void setPosIni(int _posIni) {
        this.liPosIni = _posIni;
    }

    public int getPosIni() {
        return liPosIni;
    }

    public void setPosFin(int _posFin) {
        this.liPosFin = _posFin;
    }

    public int getPosFin() {
        return liPosFin;
        
    }
    
}
