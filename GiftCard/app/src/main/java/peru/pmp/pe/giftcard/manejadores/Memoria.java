package peru.pmp.pe.giftcard.manejadores;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import peru.pmp.pe.giftcard.baseclass.Constantes;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.ConsultaSaldosE;
import peru.pmp.pe.giftcard.entidades.MovementsItem;
import peru.pmp.pe.giftcard.entidades.OperacionE;
import peru.pmp.pe.giftcard.entidades.TarjetaInfoE;
import peru.pmp.pe.giftcard.entidades.User;
import peru.pmp.pe.giftcard.libreria.Archivo;
import peru.pmp.pe.giftcard.libreria.CheckSum;
import peru.pmp.pe.giftcard.libreria.Cipher;
import peru.pmp.pe.giftcard.libreria.Texto;
import peru.pmp.pe.giftcard.utilitarios.BCD;
import peru.pmp.pe.giftcard.utilitarios.ByteBuffer;
import peru.pmp.pe.giftcard.utilitarios.Soporte;

/**
 * Clase que administra los objetos cabinet utilizados en el sistema movil.
 * Ademas de la lista de checksum de las tablas recibidas durante el proceso de sicronizacion.
 * @author Billy Vera Castellanos
 * @version 1.0
 */
public class Memoria implements Constantes {


    private static Context _context;
    private static Context _context_previus;

	private static STATUS_AUTH statusAuth;		/// -1:none  0:no autorizado  1:autorizado
	public static enum STATUS_AUTH {NONE, NO_AUTORIZADO, AUTORIZADO};
	
	//private static HashMap<String, BinE> hashBines;
	private static String merchantID, merchantName, transactionMerchantCode;
	private static String apiLogin, apiKey;
	private static String currencySimbol;
	private static String referencia;
	private static String resultado;
	private static String llaveEnc;
	private static Lenguaje idioma;
    private static boolean _credencialesLoaded;
    private static ConsultaSaldosE _consultaSaldosE;
	private static boolean _procesando;
    private static TripleDES tdes;
    private static OperacionE _operacionE;
	private static int merchantFlagCVC;
	private static int merchantFlagCardOnFile;
	private static int merchantFlagCVC_1eraCompra;
	private static double merchantAmountLimitCVC, montoValidactionTarjeta;
	private static int merchantFlagCuotas;
	private static int merchantBrandImage;
	private static LinkedHashMap<String, TarjetaInfoE> hashMisTarjetas;
	private static ArrayList<MovementsItem> hashMovements;
	
	/******** ATRIBUTOS DE VERSIONES ANTERIORES
	 * 
	 */
    private static Hashtable<String, String> hashChecksum;
	private static int _callbackApp;
	private static int _timeoutSeconds;
	private static Intent _callbackIntent;
	private static int _callbackResultado;
    
    private static boolean _recargaTebcaOn;
    private static boolean _bloquearCOMM;
    private static boolean _multiComercio;
    private static int _terminalCineTeatros;	/// (-1)Pendiente (0)No (1)Si
	private static boolean _procesandoCine;
	private static boolean _blockRefreshSocket;
	private static boolean _reservandoAsientos;
	private static boolean _showMarquesina;

    
	private static Semaphore semaphoro;

	static{
		_terminalCineTeatros = 0; ///-1;
		statusAuth = STATUS_AUTH.NONE;
		setIdioma(Lenguaje.ESPAÑOL);
		_timeoutSeconds = 60;
	}
	
	public static void turnOnSemaphoro(){
		semaphoro = new Semaphore(0);
		try{
			semaphoro.acquire();
		}catch(Exception e){
			Soporte.dump("NO SE PUEDE ACTIVAR EL SEMAFORO:"+e.getMessage());
		}
	}
	public static void turnOffSemaphoro(){
		if(semaphoro!=null){
			try{
				semaphoro.release();
			}catch(Exception e){
				Soporte.dump("NO SE PUEDE ACTIVAR EL SEMAFORO:"+e.getMessage());
			}
			finally{
				semaphoro = null;
			}
		}
	}


    public static void setContext(Context contexto){
        _context = contexto;
    }
    public static void setPreviusContext(Context contexto){
        _context_previus = contexto;
    }

    public static Context getContext(){
        return _context;
    }

    public static MyActivity getPreviusActivity(){
        return (MyActivity)_context_previus;
    }

    public static MyActivity getCurrentMyActivity(){
        return (MyActivity)_context;
    }

    public static ConsultaSaldosE getConsultaSaldos(){
        if(_consultaSaldosE == null)
            _consultaSaldosE = new ConsultaSaldosE();
        return _consultaSaldosE;
    }

	public static void initOperacion(){
		Soporte.dump("INICIANDO OPERACION");
		_consultaSaldosE = null;
	}

    public static OperacionE getOperacion(){
        if(_operacionE == null)
            _operacionE = new OperacionE();
        return _operacionE;
    }

	/*

	
	public static TarjetaInfoE getTarjetaInfo(){
		if(_tarjetaInfoE == null)
			_tarjetaInfoE = new  TarjetaInfoE();
		return _tarjetaInfoE;
	}

	public static void setTarjetaInfo(TarjetaInfoE tarj){
		_tarjetaInfoE = tarj;
	}
	*/

    public static boolean validaTramaConsultaSaldos(String xml) {
        String[] arreglo;

        boolean autorizado = false;
        // / CODIGO RESPUESTA
        arreglo = Texto
                .regexToArray("<CodRespuesta>(.+)</CodRespuesta>", xml);
        if (arreglo != null) {
            Memoria.getConsultaSaldos().setCodRespuesta(arreglo[0]);

            autorizado = Memoria.getConsultaSaldos().getCodRespuesta().equals("0000");
        }
        arreglo = Texto.regexToArray(
                "<DescRespuesta>(.+)</DescRespuesta>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setDescRespuesta(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<CodEmisor>(.+)</CodEmisor>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setCodEmisor(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<CodUsuario>(.+)</CodUsuario>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setCodUsuario(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<NumTerminal>(.+)</NumTerminal>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setNumTerminal(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<NumTarjetaMonedero>(.+)</NumTarjetaMonedero>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setNumTarjetaMonedero(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<SaldoDescBloqueo>(.+)</SaldoDescBloqueo>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setSaldoDescBloqueo(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<SaldoNombreTarjeta>(.+)</SaldoNombreTarjeta>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setSaldoNombreTarjeta(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<IdTransaccion>(.+)</IdTransaccion>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setIdTransaccion(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<CodAutorizacion>(.+)</CodAutorizacion>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setCodAutorizacion(arreglo[0]);

        arreglo = Texto.regexToArray(
                "<NumReferencia>(.+)</NumReferencia>", xml);
        if (arreglo != null)
            Memoria.getConsultaSaldos().setNumReferencia(arreglo[0]);

        if (autorizado) {

            //Memoria.getCurrentMyActivity().vibrar();

            Soporte.dump(Memoria.getConsultaSaldos().getDescRespuesta() + ENTER
                    + ENTER + "ID:" + Memoria.getConsultaSaldos().getIdTransaccion()
                    + ENTER + "COD.AUTORIZA:"
                    + Memoria.getConsultaSaldos().getCodAutorizacion() + ENTER
                    + "REFERENCIA:" + Memoria.getConsultaSaldos().getNumReferencia()
                    + ENTER);
            return true;

        } else {
            /// Si no hay mensaje y la rpta NO es autorizada
            if(Texto.isNullOrEmpty(Memoria.getConsultaSaldos().getDescRespuesta()))
                Memoria.getConsultaSaldos().setDescRespuesta("*");

            return false;
        }
    }

	public static void init(){
		setCredenciales(false);
		setCallbackApp(-1);
		setCallbackResultado(-1);
		setCallbackIntent(null);
		setProcesando(false);
		setStatusAuth(Memoria.STATUS_AUTH.NONE);

		initOperacion();

		merchantFlagCVC = 0 ;
		merchantFlagCVC_1eraCompra = 0;
		merchantAmountLimitCVC = 0;
		merchantFlagCuotas = 0;
		merchantBrandImage = 0;
		merchantFlagCardOnFile = 1;
		hashMisTarjetas = null;
	}
	public static boolean isCredenciales(){
    	return _credencialesLoaded;
    }

    public static void setCredenciales(boolean valor){
    	_credencialesLoaded = valor;
    }

    public static boolean isRecargaTebcaOn(){
    	return _recargaTebcaOn;
    }

    public static void setRecargaTebcaOn(boolean valor){
    	_recargaTebcaOn = valor;
    }

    public static String getCheckSum(String codetable) {
        String checksum = "";
        checksum = Texto.nvl((String) getHashChecksum().get(codetable),
                "00000000");
        return checksum;
    }

    public static void setCheckSum(String codetable, String newchecksum) {
        getHashChecksum().put(codetable, newchecksum);
    }

    public static void cleanCheckSum(){
        getHashChecksum().clear();
    }

    public static boolean saveCheckSum() {
    	Vector<String> vector = new Vector<String>();
        Enumeration<String> enumera = getHashChecksum().keys();
        String key, val;
        while (enumera.hasMoreElements()) {
            key = (String) enumera.nextElement();
            val = (String) hashChecksum.get(key);
            vector.addElement(key + "|" + val);
        }
        key = null;
        val = null;
        enumera = null;
        boolean ok = Archivo.vector2TXT(vector, _context.getFilesDir()
        		+ "/checksum_txt");
        return ok;
    }

    // / MANEJO DE CHECKSUM
    public static Hashtable<String, String> getHashChecksum() {
        if (hashChecksum == null) {
            hashChecksum = new Hashtable<String, String>();
            Vector<String> vector = Archivo.TXT2vector(_context.getFilesDir()
            		+ "/checksum_txt");
            String[] tokens;
            for (int u = 0; u < vector.size(); u++) {
                tokens = Texto.split((String) vector.elementAt(u), "|");
                if (tokens.length > 1) {
                    hashChecksum.put(tokens[0], tokens[1]);
                }
            }
            vector = null;
        }
        return hashChecksum;
    }

    public static boolean hayInternet(){
        boolean enabled = true;
        NetworkInfo info = null;

        try{ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
            info = connectivityManager.getActiveNetworkInfo();}
        catch(Exception e){}

        if ((info == null || !info.isConnected() || !info.isAvailable()))
            enabled = false;

        return enabled;
    }

	public static synchronized void setProcesando(boolean valor){
		_procesando = valor;
	}

	public static synchronized void setProcesandoCine(boolean valor){
		_procesandoCine = valor;
	}

	public static int getTimeoutSeconds(){
		return _timeoutSeconds;
	}

	public static void setTimeoutSeconds(int valor){
		_timeoutSeconds = valor;
	}

	public static synchronized boolean isProcesando(){
		return _procesando;
	}

	public static synchronized boolean isProcesandoCine(){
		return _procesandoCine;
	}

	public static void sendEmail(String to, String subject, String body){
	    //es necesario un intent que levante la actividad deseada
	    Intent itSend = new Intent(android.content.Intent.ACTION_SEND);

	    //vamos a enviar texto plano a menos que el checkbox esté marcado
	    itSend.setType("plain/text");

	    //colocamos los datos para el envio
	    itSend.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ to});
	    itSend.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
	    itSend.putExtra(android.content.Intent.EXTRA_TEXT, body);

	    //revisamos si el checkbox está marcado enviamos el ícono de la aplicación como adjunto
	    /*if (chkAttachment.isChecked()) {
	        //colocamos el adjunto en el stream
	        itSend.putExtra(Intent.EXTRA_STREAM, Uri.parse("android.resource://" + getPackageName() + "/" + R.drawable.icon));

	        //indicamos el tipo de dato
	        itSend.setType("image/png");
	    }*/

	    //iniciamos la actividad
	    _context.startActivity(itSend);	
	}

    public static String getMerchantId(){
    	return merchantID;
    }
    
    public static String getMerchantName(){
    	return merchantName;
    }
    
    public static void setMerchanId(String valor){
    	merchantID = valor;
    }
    
    public static void setMerchanName(String valor){
    	merchantName = valor;
    }

    public static String crypTDES(String data){
        if(tdes == null)
            tdes = new TripleDES(Memoria.getLlaveEnc());
        return tdes.Encripta(data);
    }

    public static String decrypTDES(String data){
        if(tdes == null)
            tdes = new TripleDES(Memoria.getLlaveEnc());
        return tdes.Desencripta(data);
    }

    public static void setUser(User user) {

        SharedPreferences.Editor prefsEditor = getPreferences().edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.commit();
    }

    public static User getUser() {

        User user = new User();

        try {
            Gson gson = new Gson();
            String json = getPreferences().getString("user", "");
            if (json != null && json.length() != 0)
                user = gson.fromJson(json, User.class);

        }catch (Exception e) {
            Soporte.dump(e.getLocalizedMessage());
        }

        return user;
    }

    public static void deleteUser() {
        getPreferences().edit().remove("user").commit();
    }

    public static String getEmailUser()
    {
        String defaultValue = "";
        String name = getPreferences().getString("email_user", defaultValue);
        return name;
    }
    public static  void setEmailUser(String email)
    {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString("email_user", email);
        editor.commit();
    }

	public static String getPasswordUser()
	{
		String defaultValue = "";
		String name = getPreferences().getString("password_user", defaultValue);
		return name;
	}
	public static  void setPasswordUser(String password)
	{
		SharedPreferences.Editor editor = getPreferences().edit();
		editor.putString("password_user", password);
		editor.commit();
	}

	private static SharedPreferences prefs;

	private static SharedPreferences getPreferences(){
    	if(prefs == null)
    		prefs = _context.getSharedPreferences("mpos", Context.MODE_PRIVATE);/// PreferenceManager.getDefaultSharedPreferences(_context);
    	return prefs;
    }
	public static void preferenceSave(String Key, ByteBuffer buffer){
		preferenceSave(2, Key, buffer, true);
	}
	public static void preferenceSave(String Key, ByteBuffer buffer, boolean cifrado){
		preferenceSave(2, Key, buffer, cifrado);
	}
	public static void preferenceSave(int modo, String Key, ByteBuffer buffer){
		preferenceSave(modo, Key, buffer, true);
	}
	public static void preferenceSave(int modo, String Key, ByteBuffer buffer, boolean cifrado){
		try{
			/// Modo 1: Utilizando Preferences
			if(modo == 1){
				/// ALMACENAMOS STRINGHEX
				getPreferences().edit().putString(Key, buffer.toHexString()).commit();
			}else{
			/// Modo 2: Utilizando Filesystem + cifrado
				byte[] bites;
				if(cifrado)
					bites = Cipher.crypPukall(buffer.toByteArray(), KEY_128BITS);
				else
					bites = buffer.toByteArray();
				
				/*********** ALMACENAMIENTO CAMBIADO **********/
				//Memoria.getDataLocal().setData(Key, bites);
				
				/*String rutaFile = Archivo.getRootDefault() + "/procesos/"+Key+".mpos";
				Soporte.dump("savefile("+buffer.size()+" bytes): " + rutaFile);
				Archivo.eliminarFile(rutaFile);
				Soporte.sleep(100);
				Archivo.escribirArchivo(rutaFile, bites, false);
				Soporte.sleep(100);
				rutaFile = null;*/
		    	bites = null;
			}
		}catch(Exception e){
			Soporte.dump("preferenceSave: "+e.getMessage());
		}
    }
    
    public static boolean preferenceExists(String Key){
    	return preferenceExists(2, Key);
    }
    public static boolean preferenceExists(int modo, String Key){
		/// Modo 1: Utilizando Preferences
    	if(modo==1){
    		return getPreferences().contains(Key);
    	}else{
		/// Modo 2: Utilizando Filesystem + cifrado
	    	return Archivo.existeFile(Archivo.getRootDefault() + "/procesos/"+Key+".mpos");
    	}
    }
    public static int preferenceSize(String Key){
	/// Modo 2: Utilizando Filesystem + cifrado
    	ByteBuffer buff = preferenceGet(Key);
    	int size = buff.size();
    	buff = null;
    	return size;
    }
    
    public static void preferenceDelete(String Key){
    	preferenceDelete(2, Key);
    }
    
    public static void preferenceDelete(int modo, String Key){
		/// Modo 1: Utilizando Preferences
    	if(modo == 1){
    		getPreferences().edit().remove(Key).commit();
    	}else{
		/// Modo 2: Utilizando Filesystem + cifrado
    		Archivo.eliminarFile(Archivo.getRootDefault() + "/procesos/"+Key+".mpos");
    	}
    }
    
    public static ByteBuffer preferenceGet(String Key){
    	return preferenceGet(2, Key, true);
    }
    
    public static ByteBuffer preferenceGet(int modo, String Key){
    	return preferenceGet(modo, Key, true);
    }

    public static ByteBuffer preferenceGet(String Key, boolean cifrado){
    	return preferenceGet(2, Key, cifrado);
    }

    public static ByteBuffer preferenceGet(int modo, String Key, boolean cifrado){
    	ByteBuffer buffer = new ByteBuffer();
    	if(modo == 1){
        	/// Modo 1: Utilizando Preferences
    		String texto = getPreferences().getString(Key, "");
    		buffer.write(BCD.stringHexToByteArray(texto));
    	}else{
    		/// Modo 2: Utilizando Filesystem + cifrado0
    		/*******  LECTURA CAMBIADA *******/
    		ByteBuffer buff = null;//Memoria.getDataLocal().getData(Key);
    		if(buff!=null){
    	    	if(cifrado)
    	    		buffer.write(Cipher.decrypPukall(buff.toByteArray(), KEY_128BITS));
    	    	else
    	    		buffer.write(buff.toByteArray());
    			buff = null;
    		}
    		
    		/*String textFile = Archivo.TXT2string(Archivo.getRootDefault() + "/procesos/"+Key+".mpos");
    		byte[] bites = textFile.getBytes(); /// Archivo.getBytes(Archivo.getRootDefault() + "/procesos/"+Key+".mpos");
	    	if(cifrado)
	    		buffer.write(Cipher.decrypPukall(bites, KEY_128BITS));
	    	else
	    		buffer.write(bites);
	    	bites = null;
	    	textFile = null;
	    	*/
    	}
    	return buffer;
    }
    
    public static String getAppFolder(){
    	return Archivo.getRootDefault() + "/procesos";
    }
        
	public static boolean iscBloquearCOMM() {
		return _bloquearCOMM;
	}
	public static void setBloquearCOMM(boolean _bloquearCOMM) {
		Memoria._bloquearCOMM = _bloquearCOMM;
	}
	
	public static boolean isTerminalCineTeatros() {
		return _terminalCineTeatros == 1;
	}
	
	public static int getTerminalCineTeatros() {
		return _terminalCineTeatros;
	}

	public static void setTerminalCineTeatros(int _terminalCineTeatros) {
		Memoria._terminalCineTeatros = 1; ///_terminalCineTeatros;
	}
	
	public static boolean isBlockRefreshBySocket(){
		return _blockRefreshSocket;
	}
	
	public static void setBlockRefreshBySocket(boolean v){
		_blockRefreshSocket = v;
	}
	public static Boolean isReservandoAsientos() {
		return _reservandoAsientos;
	}
	public static void setReservandoAsientos(Boolean _reservandoAsientos) {
		Memoria._reservandoAsientos = _reservandoAsientos;
	}
	public static boolean isShowMarquesina() {
		return _showMarquesina;
	}
	public static void setShowMarquesina(boolean _showMarquesina) {
		Memoria._showMarquesina = _showMarquesina;
	}
	public static void setCallbackApp(int x){
		_callbackApp = x;
	}
	public static int getCallbackApp(){
		return _callbackApp;
	}


	public static boolean isMultiComercio() {
		return _multiComercio;
	}
	
	public static void setMultiComercio(boolean _multiComercio) {
		Memoria._multiComercio = _multiComercio;
	}


	public static Intent getCallbackIntent() {
		return _callbackIntent;
	}
	public static void setCallbackIntent(Intent _callbackIntent) {
		Memoria._callbackIntent = _callbackIntent;
	}
	public static int getCallbackResultado() {
		return _callbackResultado;
	}
	public static void setCallbackResultado(int _callbackResultado) {
		Memoria._callbackResultado = _callbackResultado;
	}

	public static String getReferencia() {
		return referencia;
	}
	public static void setReferencia(String referencia) {
		Memoria.referencia = referencia;
	}
	public static String getResultado() {
		return resultado;
	}
	public static void setResultado(String resultado) {
		Memoria.resultado = resultado;
	}
	public static String getLlaveEnc() {
		return llaveEnc;
	}
	public static void setLlaveEnc(String llaveEnc) {
		Memoria.llaveEnc = llaveEnc;
	}

	/// Key = CardTokenId
	public static LinkedHashMap<String, TarjetaInfoE> getHashMisTarjetas() {
		if(hashMisTarjetas==null)
			hashMisTarjetas = new LinkedHashMap<String, TarjetaInfoE>();
		return hashMisTarjetas;
	}

	public static ArrayList<MovementsItem> getHashMovements() {
		if(hashMovements==null)
			hashMovements = new ArrayList<>();
		return hashMovements;
	}

	public static STATUS_AUTH getStatusAuth() {
		return statusAuth;
	}
	public static void setStatusAuth(STATUS_AUTH statusAuth) {
		Memoria.statusAuth = statusAuth;
	}


	public static void setMovements(JSONObject obj) {

		Memoria.getHashMovements().clear();

		try {

			Integer MoviUltMovimientos = Texto.toInt(obj.getString("MoviUltMovimientos"));

			for (int i = 0; i < MoviUltMovimientos; ++i)
			{

				MovementsItem movement = new MovementsItem();

				movement.setMovFechaTxn(obj.getString("Mov"+String.valueOf(i + 1)+"FechaTxn"));
				movement.setMovDesTxn(obj.getString("Mov"+String.valueOf(i + 1)+"DesTxn"));
				movement.setMovMonOriginalTxn("S/ "+obj.getString("Mov"+String.valueOf(i + 1)+"MonOriginalTxn"));
				movement.setMovMontoTxn("S/ "+obj.getString("Mov"+String.valueOf(i + 1)+"MontoTxn"));
				movement.setMovSigMontoTxn("S/ "+obj.getString("Mov"+String.valueOf(i + 1)+"SigMontoTxn"));
				movement.setMovTipoTarjeta(obj.getString("Mov"+String.valueOf(i + 1)+"TipoTarjeta"));
				movement.setMovFiller(obj.getString("Mov"+String.valueOf(i + 1)+"Filler"));

				Memoria.getHashMovements().add(i,movement);

				movement = null;
			}

		}catch (Exception e){
		}

	}

	public static void setRESULT_AUTENTICA(ByteBuffer buffer){
		String xml = buffer.toHtmlEscape();
		String[] arreglo;

		Memoria.setMerchanName("");

		/**** MERCHANT FLAGS ****/
		arreglo = Texto.regexToArray("<MERCHANT_FLAG_CVC>(\\d+)</MERCHANT_FLAG_CVC>", xml);
		if(arreglo!=null)
			Memoria.setMerchantFlagCVC(Texto.toInt(arreglo[0]));
		
		arreglo = Texto.regexToArray("<merchant_cvc_1ra_compra>(\\d+)</merchant_cvc_1ra_compra>", xml);
		if(arreglo!=null)
			Memoria.setMerchantFlagCVC_1eraCompra(Texto.toInt(arreglo[0]));
		
		arreglo = Texto.regexToArray("<merchant_flag_CardOnFile>(\\d+)</merchant_flag_CardOnFile>", xml);
		if(arreglo!=null)
			Memoria.setMerchantFlagCardOnFile(Texto.toInt(arreglo[0]));
		
		arreglo = Texto.regexToArray("<merchant_amount_limit_cvc>(.+)</merchant_amount_limit_cvc>", xml);
		///arreglo = Texto.regexToArray("<merchant_amount_limit_cvc>(^\\d+(\\.\\d{1,2})?$)</merchant_amount_limit_cvc>", xml);
		if(arreglo!=null)
			Memoria.setMerchantAmountLimitCVC(Texto.toDouble(arreglo[0]));
		
		arreglo = Texto.regexToArray("<MERCHANT_FLAG_CUOTAS>(\\d+)</MERCHANT_FLAG_CUOTAS>", xml);
		if(arreglo!=null)
			Memoria.setMerchantFlagCuotas(Texto.toInt(arreglo[0]));
		
		arreglo = Texto.regexToArray("<MERCHANT_BRAND_IMAGE>(\\d+)</MERCHANT_BRAND_IMAGE>", xml);
		if(arreglo!=null)
			Memoria.setMerchantBrandImage(Texto.toInt(arreglo[0]));
		
		arreglo = Texto.regexToArray("<transaction_currency_symbol>(.+)</transaction_currency_symbol>", xml);
		if(arreglo!=null)
			Memoria.setCurrencySimbol(arreglo[0]);
		
		arreglo = Texto.regexToArray("<merchant_monto_validacion_tarjeta>(.+)</merchant_monto_validacion_tarjeta>", xml);
		if(arreglo!=null)
			Memoria.setMontoValidactionTarjeta(Texto.toDouble(arreglo[0]));

		arreglo = Texto.regexToArray("<app_timeout>(\\d+)</app_timeout>", xml);
		if(arreglo!=null)
			Memoria.setTimeoutSeconds(Texto.toInt(arreglo[0]));


		/**** MIS TARJETAS ****/

		/**** DOCUMENTOS ****/

		/**** RESULTADO ****/

		arreglo = Texto.regexToArray("<Resultado>(.+)</Resultado>", xml);
		if(arreglo!=null)
			Memoria.setResultado(arreglo[0]);

		/**** LLAVE ****/
		
		/// Debemos mostrar solo las marcas asignadas remotamente
		//Memoria.getCurrentMyActivity().finishNotify();

		arreglo = null;		
		xml = null;
	}


	public static String getAPILogin() {
		return apiLogin;
	}
	public static void setAPILogin(String apiLogin) {
		Memoria.apiLogin = apiLogin;
	}
	public static String getAPIKey() {
		return apiKey;
	}
	public static void setAPIKey(String apiKey) {
		Memoria.apiKey = apiKey;
	}
	public static Lenguaje getIdioma() {
		return idioma;
	}
	public static void setIdioma(Lenguaje idioma) {
		Memoria.idioma = idioma;
	}

	public static String getSignature(){
    	StringBuilder builder = new StringBuilder();
    	builder.append(getMerchantId());
    	builder.append(getReferencia());
    	//builder.append(getOperacion().getMoneda().code());
    	//builder.append(getOperacion().getTotalText());
    	builder.append(getAPIKey());
    	String signature = CheckSum.getSHA1Checksum(builder.toString(), getAPIKey());
    	builder = null;
    	return signature;
	}
	public static int getMerchantFlagCVC() {
		return merchantFlagCVC;
	}
	public static void setMerchantFlagCVC(int merchantFlagCVC) {
		Memoria.merchantFlagCVC = merchantFlagCVC;
	}
	public static double getMerchantAmountLimitCVC() {
		return merchantAmountLimitCVC;
	}
	public static void setMerchantAmountLimitCVC(double merchantAmountLimitCVC) {
		Memoria.merchantAmountLimitCVC = merchantAmountLimitCVC;
	}
	public static int getMerchantFlagCuotas() {
		return merchantFlagCuotas;
	}
	public static void setMerchantFlagCuotas(int merchantFlagCuotas) {
		Memoria.merchantFlagCuotas = merchantFlagCuotas;
	}
	public static int getMerchantBrandImage() {
		return merchantBrandImage;
	}
	public static void setMerchantBrandImage(int merchantBrandImage) {
		Memoria.merchantBrandImage = merchantBrandImage;
	}

	public static String getIMEI(){

		WifiManager wifiManager = (WifiManager)_context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		String stringValue = wInfo.getMacAddress();
		Soporte.dump("WiFi MAC:" + stringValue);
		stringValue = CheckSum.getMD5Checksum(BCD.stringHexToByteArray(stringValue));
		stringValue = CheckSum.getSubToken(stringValue);
		wInfo = null;
		wifiManager = null;
		return stringValue;
	}

	public static boolean isProduccion(){
    	return true;
    }
	public static int getMerchantFlagCVC_1eraCompra() {
		return merchantFlagCVC_1eraCompra;
	}
	public static void setMerchantFlagCVC_1eraCompra(
			int merchantFlagCVC_1eraCompra) {
		Memoria.merchantFlagCVC_1eraCompra = merchantFlagCVC_1eraCompra;
	}
	public static String getTransactionMerchantCode() {
		return transactionMerchantCode;
	}
	public static void setTransactionMerchantCode(
			String transactionMerchantCode) {
		Memoria.transactionMerchantCode = transactionMerchantCode;
	}
	public static String getCurrencySimbol() {
		if(currencySimbol==null)
			currencySimbol = "S/";
		return currencySimbol;
	}
	public static void setCurrencySimbol(String currencySimbol) {
		Memoria.currencySimbol = currencySimbol;
	}
	public static double getMontoValidactionTarjeta() {
		return montoValidactionTarjeta;
	}
	public static void setMontoValidactionTarjeta(double montoValidactionTarjeta) {
		Memoria.montoValidactionTarjeta = montoValidactionTarjeta;
	}
	
	public static int getMerchantFlagCardOnFile(){
		return merchantFlagCardOnFile;
	}
	
	public static void setMerchantFlagCardOnFile(int valor){
		merchantFlagCardOnFile = valor;
	}
	
}
 