package peru.pmp.pe.giftcard.libreria;

import android.content.Context;
import android.net.wifi.WifiManager;

import peru.pmp.pe.giftcard.manejadores.Memoria;

public class WiFi {

	public static boolean isEnabled(){
    	WifiManager wifiManager = (WifiManager) Memoria.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        boolean isEnabled = wifiManager.isWifiEnabled();
        return isEnabled;
	}
	
    public static boolean setWiFi(boolean enable)
    {
    	WifiManager wifiManager = (WifiManager) Memoria.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        boolean isEnabled = wifiManager.isWifiEnabled();
        if (enable && !isEnabled)
        {
        	return wifiManager.setWifiEnabled(true);
        }
        else if (!enable && isEnabled)
        {
        	return wifiManager.setWifiEnabled(false);
        }
        // No need to change bluetooth state
        return true;
    }
}
