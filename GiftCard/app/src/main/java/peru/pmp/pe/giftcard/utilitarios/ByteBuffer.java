package peru.pmp.pe.giftcard.utilitarios;

import java.io.ByteArrayOutputStream;
import peru.pmp.pe.giftcard.libreria.CheckSum;
import peru.pmp.pe.giftcard.libreria.Texto;

/**
 * Clase utilitaria que permite manejar buffer de memoria en el sistema movil
 * @author Billy Vera Castellanos
 * @version 1.0
 */
public class ByteBuffer {

    ByteArrayOutputStream baos;

    public static final String ASCII = "US-ASCII";
	public static final String UTF8 = "UTF-8";
	public static final String ISO8859_1 = "ISO-8859-1";
	public static final String DEFAULT_CHAR_ENCODING = ISO8859_1;
    
    public ByteBuffer(){
         
        baos = new ByteArrayOutputStream(6144);
    }
    
    public ByteBuffer(String texto){
    	this();
    	write(texto);
    }
    public void write(String data){
    	if(data == null)
    		data = "";
        try{
            baos.write(data.getBytes());
        }catch(Exception e){}
    }
    
    public void write(byte bite){
        try{
            baos.write(bite);
        }catch(Exception e){}
    }

    public void write(byte[] data){
        try{
            baos.write(data);
        }catch(Exception e){}
    }

    public void writeISO(int codigo, String data){
    	if(data == null)
    		data = "";
    	writeISO(codigo, data.getBytes());
    }

    public void writeISO(int codigo, byte[] data){
        try{
        	baos.write(Utiles.calculoCabecera(data.length));
        	baos.write(Utiles.calculoCabecera(codigo));
            baos.write(data);
        }catch(Exception e){}
    }

    public void write(byte[] data, int start, int len){
        try{
        	if((start+len) > data.length)
        		len = data.length - start;
            baos.write(data, start, len);
        }catch(Exception e){
        	Soporte.dump("ByteBuffer.write:"+e.getMessage());
        }
    }
    
    public byte[] toByteArray(){
        return baos.toByteArray();
    }

    public String toHexString(){
    	return BCD.byteArrayToStringHex(toByteArray());
    }
    
    public String toStringISO8859()
    {
        return Soporte.ISO8859_1Decode(toByteArray(), 0, size());
    }
    
    public String toString()
    {
        return Soporte.UTF8Decode(toByteArray(), 0, size());
    }
    
    public String toHtmlEscape()
    {
    	try{
    		return Texto.unescapeHtml3(toString());
    	}catch(Exception e){
    		return null;
    	}
    }
    
    public java.nio.ByteBuffer toNioBuffer(){
    	java.nio.ByteBuffer buf = java.nio.ByteBuffer.allocate(size());
    	byte[] bytes_data = toByteArray(); 
    	for(byte bite : bytes_data)
    		buf.put(bite);
    	buf.position(size()-1);
        return buf;
    }

    public int size(){
        return baos.size();
    }
    
    public void reset(){
        baos.reset();
    }
    
    public void printBytes(){
    	StringBuffer st = new StringBuffer();
    	byte[] bites = toByteArray();
    	for(int pos=0; pos<size(); pos++){
    		st.append(bites[pos]+", ");
    	}
    	Soporte.dump(st.toString());
    	st = null;
    	bites = null;
    }
    
    public String checksum(){
    	String data = CheckSum.getMD5Checksum(baos.toByteArray());
    	return data;
    }
}
