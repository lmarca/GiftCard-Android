/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package peru.pmp.pe.giftcard.entidades;

import peru.pmp.pe.giftcard.baseclass.Entidad;


/**
 *
 * @author Administrador
 */
public class EventoLogE extends Entidad{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -1856872375707370560L;
	public static int iEVENTO_RED_TRAFICO      = 1;
    public static int iEVENTO_RED_CONTENT      = 2;
    public static int iEVENTO_SISTEMA          = 3;
    public static int iEVENTO_MENSAJERIA        = 4;
    public static int iEVENTO_BASE_DATOS       = 5;
    public static int iEVENTO_PRINTSTACKERROR  = 6;
    public static int iEVENTO_TRACE            = 7;
    
    /// Propiedades
    private int iTipo;
    private String sMessage;

    public EventoLogE(){}

    public EventoLogE(int iTipo, String sMessage) {
        this.iTipo = iTipo;
        this.sMessage = sMessage;
    }

    public void setITipo(int iTipo) {
        this.iTipo = iTipo;
    }

    public void setSMessage(String sMessage) {
        this.sMessage = sMessage;
    }

    public int getITipo() {
        return iTipo;
    }

    public String getSMessage() {
        return sMessage;
    }

}
