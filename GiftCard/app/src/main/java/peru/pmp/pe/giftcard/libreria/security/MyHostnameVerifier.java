package peru.pmp.pe.giftcard.libreria.security;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class MyHostnameVerifier implements HostnameVerifier {
	  public boolean verify(String urlHostname, SSLSession sslSession) {
		  
		  boolean equals = urlHostname.equals(sslSession.getPeerHost()); 
		  return equals;
		  
	  }
	}
