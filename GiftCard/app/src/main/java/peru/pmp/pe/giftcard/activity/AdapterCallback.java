package peru.pmp.pe.giftcard.activity;

import peru.pmp.pe.giftcard.entidades.TarjetaInfoE;

/**
 * Created by apple on 8/12/17.
 */

public interface AdapterCallback {

    public void deleteTarj(TarjetaInfoE tarj, int position);
    public void selectTarj(TarjetaInfoE tarj);

}
