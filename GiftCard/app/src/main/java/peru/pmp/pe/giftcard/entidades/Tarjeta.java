package peru.pmp.pe.giftcard.entidades;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Admin on 15/02/18.
 */

public class Tarjeta extends RealmObject {

    @PrimaryKey
    private String card_mask;

    private String card_alias;

    private String card_mask_encrypted;

    private String card_fecha_vcto_encrypted;

    public String getCard_mask() {
        return card_mask;
    }

    public void setCard_mask(String card_mask) {
        this.card_mask = card_mask;
    }

    public String getCard_alias() {
        return card_alias;
    }

    public void setCard_alias(String card_alias) {
        this.card_alias = card_alias;
    }

    public String getCard_mask_encrypted() {
        return card_mask_encrypted;
    }

    public void setCard_mask_encrypted(String card_mask_encrypted) {
        this.card_mask_encrypted = card_mask_encrypted;
    }

    public String getCard_fecha_vcto_encrypted() {
        return card_fecha_vcto_encrypted;
    }

    public void setCard_fecha_vcto_encrypted(String card_fecha_vcto_encrypted) {
        this.card_fecha_vcto_encrypted = card_fecha_vcto_encrypted;
    }
}
