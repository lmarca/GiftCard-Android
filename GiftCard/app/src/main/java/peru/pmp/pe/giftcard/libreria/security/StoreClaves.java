package peru.pmp.pe.giftcard.libreria.security;

import android.content.Context;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

/**
 * Crea un entorno SEGURO para almacenar contraseñas.
 * Utiliza el par de llaves publica y privada de un certificado registrado en el 
 * keystore. El cifrado se realiza de acuerdo al alias del certificado.
 * Si no lo encuentra en el keystore, genera un nuevo certificado con el alias especificado.
 */
public class StoreClaves {
	private final Cipher mCipher;
	private final KeyPair mPair;

	public StoreClaves(Context context, String alias)
	        throws GeneralSecurityException, IOException {
	    mCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
	    
	    final KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
	    keyStore.load(null);
	    if (!keyStore.containsAlias(alias)) 
	        Certificado.generateKeyPair(context, alias);
	    
	    // Even if we just generated the key, always read it back to ensure we
	    // can read it successfully.
	    final KeyStore.PrivateKeyEntry entry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(
	            alias, null);
	    mPair = new KeyPair(entry.getCertificate().getPublicKey(), entry.getPrivateKey());
	}

	

	/**
	 * Wrap a {@link javax.crypto.SecretKey} using the public key assigned to this wrapper.
	 * Use {@link #unwrap(byte[])} to later recover the original
	 * {@link javax.crypto.SecretKey}.
	 *
	 * @return a wrapped version of the given {@link javax.crypto.SecretKey} that can be
	 *         safely stored on untrusted storage.
	 */
	public byte[] wrap(SecretKey key) throws GeneralSecurityException {
	    mCipher.init(Cipher.WRAP_MODE, mPair.getPublic());
	    return mCipher.wrap(key);
	}

	/**
	 * Unwrap a {@link javax.crypto.SecretKey} using the private key assigned to this
	 * wrapper.
	 *
	 * @param blob a wrapped {@link javax.crypto.SecretKey} as previously returned by
	 *            {@link #wrap(javax.crypto.SecretKey)}.
	 */
	public SecretKey unwrap(byte[] blob) throws GeneralSecurityException {
	    mCipher.init(Cipher.UNWRAP_MODE, mPair.getPrivate());
	    return (SecretKey) mCipher.unwrap(blob, "AES", Cipher.SECRET_KEY);
	}
}
