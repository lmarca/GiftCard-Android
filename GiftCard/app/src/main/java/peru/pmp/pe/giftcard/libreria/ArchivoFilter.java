package peru.pmp.pe.giftcard.libreria;

/**
 * <p>Empresa: MOVILOGIC PERU</p>
 * @author Billy Vera Castellanos
 * @version 1.0
 */

import java.io.File;
import java.io.FilenameFilter;

public class ArchivoFilter implements FilenameFilter {

  String nomarch;

  public ArchivoFilter(String narch) {
    nomarch = narch;
  }

  public boolean accept(File dir, String nombre) {
    if (nomarch.equals(".") && nombre.length() == 0)
      return (true);
    if (nomarch.equals("*.*") && nombre.length() > 0)
      return (true);
      
    boolean nomOK = true;
    int iLen = nomarch.length();
    boolean bComodinChar = false, bComodinGlobal=false;
    char sChar;
    for (int i = 0; i < iLen; i++) {
        if(i==nombre.length()){
            if(!bComodinGlobal)
                nomOK=false;
            break;
        }
                                    // Si el tamaÃ±o del archivo de comparacion
        bComodinChar = false;       // es menor que la matriz, y se continua
        sChar = nomarch.charAt(i);   // comparando quiere decir que el archivo es OK.-
        
        //if(sChar=='.')  bComodinGlobal=false;
        if(sChar=='?')  bComodinChar=true;
        if(sChar=='*')  bComodinGlobal=true;
        
        if(bComodinGlobal)  continue;
        if(bComodinChar)    continue;
        if (sChar != nombre.charAt(i)) {
            nomOK = false;
            break;
        }
    }
    return (nomOK);
  }
}
