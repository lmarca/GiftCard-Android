package peru.pmp.pe.giftcard.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.activity.AdapterCallback;
import peru.pmp.pe.giftcard.activity.ListMovementsActivity;
import peru.pmp.pe.giftcard.entidades.TarjetaInfoE;

/**
 * Created by Admin on 29/11/17.
 */

public class TarjsAdapter extends RecyclerView.Adapter<TarjsAdapter.TarjetaHolder> {


    private AdapterCallback mAdapterCallback;
    private ArrayList<TarjetaInfoE> lista;
    private Activity activity;

    public TarjsAdapter(AdapterCallback callback) {
        this.mAdapterCallback = callback;
    }

    public TarjsAdapter(ArrayList<TarjetaInfoE> lista, Activity activity, AdapterCallback callback) {
        this.lista = lista;
        this.activity = activity;
        this.mAdapterCallback = callback;
    }

    @Override
    public TarjsAdapter.TarjetaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tarjeta, parent, false);
        return new TarjsAdapter.TarjetaHolder(view);
    }

    @Override
    public void onBindViewHolder(TarjsAdapter.TarjetaHolder holder, final int position) {

        //holder.image_logo.setImageResource(R.drawable.ic_wallet);

        holder.text_pan.setText(lista.get(position).getCard_mask());
        holder.text_alias.setText(lista.get(position).getCard_alias());

        holder.linear_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.selectTarj(lista.get(position));
            }
        });

        holder.linear_container.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                try {
                    mAdapterCallback.deleteTarj(lista.get(position), position);
                } catch (ClassCastException exception) {
                    // do something
                }
                return false;
            }
        });
    }

    public void removeAt(int position) {
        lista.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, lista.size());
    }


    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class TarjetaHolder extends RecyclerView.ViewHolder {

        RelativeLayout linear_container;
        ImageView image_logo;
        TextView text_pan;
        TextView text_alias;

        public TarjetaHolder(View itemView) {
            super(itemView);

            linear_container = (RelativeLayout) itemView.findViewById(R.id.linear_container_tarj);
            image_logo = (ImageView) itemView.findViewById(R.id.image_logo);
            text_pan = (TextView) itemView.findViewById(R.id.text_pan);
            text_alias = (TextView) itemView.findViewById(R.id.text_alias);
        }
    }
}
