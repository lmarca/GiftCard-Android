package peru.pmp.pe.giftcard.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;
import io.realm.Realm;
import peru.pmp.pe.giftcard.R;
import peru.pmp.pe.giftcard.baseclass.MyActivity;
import peru.pmp.pe.giftcard.entidades.Tarjeta;
import peru.pmp.pe.giftcard.entidades.TramaE;
import peru.pmp.pe.giftcard.libreria.Texto;
import peru.pmp.pe.giftcard.manejadores.Memoria;
import peru.pmp.pe.giftcard.networking.GoogleApi;
import peru.pmp.pe.giftcard.realm.RealmController;
import peru.pmp.pe.giftcard.utilitarios.DesignUtil;
import peru.pmp.pe.giftcard.utilitarios.ReportUtil;
import peru.pmp.pe.giftcard.utilitarios.Soporte;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by apple on 9/11/17.
 */

public class CardActivity extends MyActivity {

    TramaE tramaE;
    private Realm realm;
    public int pos=0;

    @Bind(R.id.btnAlias)
    Button btnAlias;

    @Bind(R.id.edit_expirydate)
    EditText edit_expirydate;

    @Bind(R.id.edit_card)
    EditText edit_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_card);
        DesignUtil.customStatusBarColor(getWindow(), this, R.color.colorPrimaryDark);

        //ButterKnife.inject(this);
        ButterKnife.bind(this);
        Memoria.setLlaveEnc(Memoria.getUser().getApi_key());

        //get realm instance
        this.realm = RealmController.with(this).getRealm();


        /*** INICIALIZACION DE VARIABLES ***/


        edit_card.addTextChangedListener(new TextWatcher() {

            private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
            private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
            private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
            private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
            private static final char DIVIDER = '-';

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // noop
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // noop
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
                    s.replace(0, s.length(), buildCorrecntString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
                }
            }

            private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
                boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
                for (int i = 0; i < s.length(); i++) { // chech that every element is right
                    if (i > 0 && (i + 1) % dividerModulo == 0) {
                        isCorrect &= divider == s.charAt(i);
                    } else {
                        isCorrect &= Character.isDigit(s.charAt(i));
                    }
                }
                return isCorrect;
            }

            private String buildCorrecntString(char[] digits, int dividerPosition, char divider) {
                final StringBuilder formatted = new StringBuilder();

                for (int i = 0; i < digits.length; i++) {
                    if (digits[i] != 0) {
                        formatted.append(digits[i]);
                        if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                            formatted.append(divider);
                        }
                    }
                }

                return formatted.toString();
            }

            private char[] getDigitArray(final Editable s, final int size) {
                char[] digits = new char[size];
                int index = 0;
                for (int i = 0; i < s.length() && index < size; i++) {
                    char current = s.charAt(i);
                    if (Character.isDigit(current)) {
                        digits[index] = current;
                        index++;
                    }
                }
                return digits;
            }
        });

        edit_expirydate.addTextChangedListener(new TextWatcher() {

            boolean ignorarChanged = false;

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                if (!ignorarChanged) {

                    String vence = s.toString();
                    vence = vence.replace("/", "").trim();

                    // / Validación de tamaño
                    if (vence.length() > 4)
                        vence = vence.substring(0, 4);

                    // / Validación de mes/año
                    if (vence.length() < 2) {

                    } else {
                        String valorMes = vence.substring(0, 2);
                        if (Texto.toInt(valorMes) > 12)
                            valorMes = "12";
                        // / Enmascarar
                        if (vence.length() > 2)
                            vence = valorMes + "/" + vence.substring(2);
                        else
                            vence = valorMes;
                    }
                    ignorarChanged = true;
                    edit_expirydate.setText(vence);
                    edit_expirydate.setSelection(edit_expirydate.getText()
                            .length());
                    ignorarChanged = false;
                }
            }
        });

        if (Memoria.getOperacion().getNumberTarj().length() != 0) {
            edit_card.setText("980195" + Memoria.getOperacion().getNumberTarj());
        }

    }

    @OnClick(R.id.btnHecho)
    public void getRegistra() {

        if (!Memoria.hayInternet()) {
            toast(ReportUtil.MENSAJE_CONEXION_INTERNET);
        }else {

            if (btnAlias.getText().toString().equals("+ Ingrese Nombre")) {
                Memoria.getOperacion().setAliasTarj("GiftCard");
            }

            if (edit_card.getText().toString().length() != 19) {
                toast("Ingrese un número de tarjeta válido");
                return;
            }

            if (edit_expirydate.getText().toString().length() != 5) {
                toast("Ingrese una fecha válida");
                return;
            }

            Memoria.getOperacion().setAliasTarj(btnAlias.getText().toString());
            Memoria.getOperacion().setNumberTarj(edit_card.getText().toString().replace("-",""));
            Memoria.getOperacion().setDateTarj(edit_expirydate.getText().toString().replace("/",""));

            apiJson();
        }
    }

    @OnClick(R.id.btnAlias)
    public void getAlertAlias() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ingrese Alias");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        //input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        if (!btnAlias.getText().toString().equals("+ Ingrese Nombre")) {
            input.setText(btnAlias.getText().toString());
        }
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //m_Text = input.getText().toString();
                btnAlias.setText(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void apiJson() {

        List<Tarjeta> tarjetaList = RealmController.with(this).getBooks();

        for (int i = 0; i < tarjetaList.size(); i++) {

            if (tarjetaList.get(i).getCard_mask_encrypted().equals(Memoria.crypTDES(Memoria.getOperacion().getNumberTarj()))) {
                toast("La tarjeta ya se encuentra registrada");
                return;
            }
        }

        showProgressBar("Procesando");

        GoogleApi.RestApi restApi = GoogleApi.RestApi.RETROFIT.create(GoogleApi.RestApi.class);

        tramaE = new TramaE();

        restApi.wsGetRegistraCard(tramaE.buildTramaRegistraTarjeta()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                tramaE = null;
                hideProgressBar();

                if (response.isSuccessful()) {

                    try {

                        String user = response.body().get("Registra_Card").toString();

                        Soporte.dump(user);

                        JSONObject obj = new JSONObject(user);

                        if (obj.getString("response_code").equals("00")){

                            show(HomeCardActivity.class);

                            Tarjeta tarj = new Tarjeta();

                            tarj.setCard_mask_encrypted(Memoria.crypTDES(Memoria.getOperacion().getNumberTarj()));
                            tarj.setCard_fecha_vcto_encrypted(Memoria.crypTDES(Memoria.getOperacion().getDateTarj()));
                            tarj.setCard_mask(obj.getString("transaction_card"));
                            tarj.setCard_alias(btnAlias.getText().toString());

                            // Persist your data easily
                            realm.beginTransaction();
                            realm.copyToRealm(tarj);
                            realm.commitTransaction();

                        }else {
                            toast(obj.getString("response_message") + obj.getString("response_detalle"));
                        }

                    } catch (Throwable tx) {}
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                tramaE = null;
                hideProgressBar();
            }
        });

    }

}
