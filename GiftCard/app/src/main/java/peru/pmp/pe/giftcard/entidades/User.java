package peru.pmp.pe.giftcard.entidades;


/**
 * Created by Admin on 14/11/17.
 */

public class User {

    private String afiliacion_mensaje;
    private String api_key;
    private String api_login;
    private String buyer_doc_number;
    private String buyer_doc_type;
    private String buyer_email;
    private String buyer_first_name;
    private String buyer_last_name;
    private String buyer_merchant_id;
    private String buyer_mobile_id;
    private String buyer_mobile_token_id;
    private String buyer_token_id;
    private int flag_mobile;
    private String merchant_code;
    private String response_code;
    private String response_message;
    private String tarjeta_promocion;
    private String transaction_currency;

    public User() {
        //super();

        afiliacion_mensaje = "";
        api_key = "";
        api_login = "";
        buyer_doc_number = "";
        buyer_doc_type = "";
        buyer_email = "";
        buyer_first_name = "";
        buyer_last_name = "";
        buyer_merchant_id = "";
        buyer_mobile_id = "";
        buyer_mobile_token_id = "";
        buyer_token_id = "";
        setFlag_mobile(0);
        merchant_code = "";
        response_code = "";
        response_message = "";
        tarjeta_promocion = "";
        transaction_currency = "";
    }

    public User(String afiliacion_mensaje, String api_key, String api_login, String buyer_doc_number, String buyer_doc_type, String buyer_email, String buyer_first_name, String buyer_last_name, String buyer_merchant_id, String buyer_mobile_id, String buyer_mobile_token_id, String buyer_token_id, int flag_mobile, String merchant_code, String response_code, String response_message, String tarjeta_promocion, String transaction_currency) {
        this.afiliacion_mensaje = afiliacion_mensaje;
        this.api_key = api_key;
        this.api_login = api_login;
        this.buyer_doc_number = buyer_doc_number;
        this.buyer_doc_type = buyer_doc_type;
        this.buyer_email = buyer_email;
        this.buyer_first_name = buyer_first_name;
        this.buyer_last_name = buyer_last_name;
        this.buyer_merchant_id = buyer_merchant_id;
        this.buyer_mobile_id = buyer_mobile_id;
        this.buyer_mobile_token_id = buyer_mobile_token_id;
        this.buyer_token_id = buyer_token_id;
        this.flag_mobile = flag_mobile;
        this.merchant_code = merchant_code;
        this.response_code = response_code;
        this.response_message = response_message;
        this.tarjeta_promocion = tarjeta_promocion;
        this.transaction_currency = transaction_currency;
    }

    public String getAfiliacion_mensaje() {
        return afiliacion_mensaje;
    }

    public void setAfiliacion_mensaje(String afiliacion_mensaje) {
        this.afiliacion_mensaje = afiliacion_mensaje;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getApi_login() {
        return api_login;
    }

    public void setApi_login(String api_login) {
        this.api_login = api_login;
    }

    public String getBuyer_doc_number() {
        return buyer_doc_number;
    }

    public void setBuyer_doc_number(String buyer_doc_number) {
        this.buyer_doc_number = buyer_doc_number;
    }

    public String getBuyer_doc_type() {
        return buyer_doc_type;
    }

    public void setBuyer_doc_type(String buyer_doc_type) {
        this.buyer_doc_type = buyer_doc_type;
    }

    public String getBuyer_email() {
        return buyer_email;
    }

    public void setBuyer_email(String buyer_email) {
        this.buyer_email = buyer_email;
    }

    public String getBuyer_first_name() {
        return buyer_first_name;
    }

    public void setBuyer_first_name(String buyer_first_name) {
        this.buyer_first_name = buyer_first_name;
    }

    public String getBuyer_last_name() {
        return buyer_last_name;
    }

    public void setBuyer_last_name(String buyer_last_name) {
        this.buyer_last_name = buyer_last_name;
    }

    public String getBuyer_merchant_id() {
        return buyer_merchant_id;
    }

    public void setBuyer_merchant_id(String buyer_merchant_id) {
        this.buyer_merchant_id = buyer_merchant_id;
    }

    public String getBuyer_mobile_id() {
        return buyer_mobile_id;
    }

    public void setBuyer_mobile_id(String buyer_mobile_id) {
        this.buyer_mobile_id = buyer_mobile_id;
    }

    public String getBuyer_mobile_token_id() {
        return buyer_mobile_token_id;
    }

    public void setBuyer_mobile_token_id(String buyer_mobile_token_id) {
        this.buyer_mobile_token_id = buyer_mobile_token_id;
    }

    public String getBuyer_token_id() {
        return buyer_token_id;
    }

    public void setBuyer_token_id(String buyer_token_id) {
        this.buyer_token_id = buyer_token_id;
    }

    public int getFlag_mobile() {
        return flag_mobile;
    }

    public void setFlag_mobile(int flag_mobile) {
        this.flag_mobile = flag_mobile;
    }

    public String getMerchant_code() {
        return merchant_code;
    }

    public void setMerchant_code(String merchant_code) {
        this.merchant_code = merchant_code;
    }

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }

    public String getTarjeta_promocion() {
        return tarjeta_promocion;
    }

    public void setTarjeta_promocion(String tarjeta_promocion) {
        this.tarjeta_promocion = tarjeta_promocion;
    }

    public String getTransaction_currency() {
        return transaction_currency;
    }

    public void setTransaction_currency(String transaction_currency) {
        this.transaction_currency = transaction_currency;
    }
}
